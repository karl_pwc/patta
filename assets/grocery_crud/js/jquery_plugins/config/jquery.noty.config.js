function success_message(success_message)
{
    Metronic.alert({
        container: '',
        place: 'append',
        type: 'success',
        message: success_message,
        close: true,
        reset: true,
        focus: true,
        closeInSeconds: 0,
        icon: 'check'
    });
}

function error_message(error_message)
{
    Metronic.alert({
        container: '',
        place: 'append',
        type: 'danger',
        message: error_message,
        close: true,
        reset: true,
        focus: true,
        closeInSeconds: 0,
        icon: 'warning'
    });
}

function form_success_message(success_message)
{
    success_message(success_message);
}

function form_error_message(error_message)
{
    error_message(error_message);
}