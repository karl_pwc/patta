function success_message(success_message)
{
    Metronic.alert({
        container: '',
        place: 'append',
        type: 'success',
        message: success_message,
        close: true,
        reset: true,
        focus: true,
        closeInSeconds: 0,
        icon: ''
    });
}

function error_message(error_message)
{
    Metronic.alert({
        container: '',
        place: 'append',
        type: 'danger',
        message: error_message,
        close: true,
        reset: true,
        focus: true,
        closeInSeconds: 0,
        icon: ''
    });
}

function form_success_message(message)
{
    success_message(message);
}

function form_error_message(message)
{
    error_message(message);
}

function toastrSuccess(str) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr.success(str);
}