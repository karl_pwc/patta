$(function () {
    var save_and_close = false;

    $('#save-and-go-back-button').click(function () {
        save_and_close = true;
        $('#crudForm').trigger('submit');
    });

    $('#crudForm').submit(function () {
        $(this).ajaxSubmit({
            url: validation_url,
            dataType: 'json',
            cache: 'false',
            beforeSend: function () {
                Metronic.blockUI();
            },
            success: function (data) {
                Metronic.unblockUI();
                if (data.success) {
                    $('#crudForm').ajaxSubmit({
                        dataType: 'text',
                        cache: 'false',
                        beforeSend: function () {
                            Metronic.blockUI();
                        },
                        success: function (result) {
                            Metronic.unblockUI();
                            data = $.parseJSON(result);
                            if (data.success) {
                                if (save_and_close) {
                                    window.location = data.success_list_url;
                                    return true;
                                }
                                $('.has-error').each(function () {
                                    $(this).removeClass('has-error');
                                });
                                form_success_message(data.success_message);
                            }
                            else {
                                alert(message_insert_error);
                            }
                        },
                        error: function () {
                            alert(message_insert_error);
                            Metronic.unblockUI();
                        }
                    });
                }
                else {
                    $('.has-error').removeClass('has-error');
                    form_error_message(data.error_message);
                    $.each(data.error_fields, function (index, value) {
                        $('[name=' + index + ']').closest('.form-group').addClass('has-error');
                    });
                }
            },
            error: function (data) {
                console.log(data);
                error_message(message_insert_error);
                Metronic.unblockUI();
            }
        });
        return false;
    });

    $('#cancel-button').click(function () {
        if (confirm(message_alert_edit_form)) {
            window.location = list_url;
        }
        return false;
    });
});

