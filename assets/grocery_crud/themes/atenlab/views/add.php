<?php
    $this->set_js_lib($this->default_theme_path.'/atenlab/js/jquery.form.js');
    $this->set_js_config($this->default_theme_path.'/atenlab/js/add.js');
?>
<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo $subject?>
        </h3>
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="/department"><?php echo $subject?></a>
                <i class="fa fa-angle-right"></i>
            </li>
            <li>
                <a href="#">新增</a>
            </li>
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>
<!-- END PAGE HEADER-->
<div class="portlet box blue ">
    <div class="portlet-title">
        <div class="caption">
            新增 <?php echo $subject?>
        </div>
    </div>
    <div class="portlet-body form" data-unique-hash="<?php echo $unique_hash; ?>">
        <!-- BEGIN FORM-->
        <?php echo form_open( $insert_url, 'method="post" id="crudForm" class="form-horizontal form-bordered form-row-stripped"'); ?>
        <?php foreach($fields as $field) { ?>
            <div class="form-body">
                <div class="form-group" id="<?php echo $field->field_name; ?>_field_box">
                    <label class="control-label col-md-3">
                        <?php echo $input_fields[$field->field_name]->display_as; ?>
                        <?php echo ($input_fields[$field->field_name]->required)? '<span class="required" aria-required="true">* </span>' : ""; ?>
                    </label>
                    <div class="col-md-9" id="<?php echo $field->field_name; ?>_input_box">
                        <?php echo $input_fields[$field->field_name]->input?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!-- Start of hidden inputs -->
        <?php
        foreach($hidden_fields as $hidden_field){
            echo $hidden_field->input;
        }
        ?>
        <!-- End of hidden inputs -->
        <?php if ($is_ajax) { ?><input type="hidden" name="is_ajax" value="true" /><?php }?>

        <div id='report-error' class='report-div error'></div>
        <div id='report-success' class='report-div success'></div>
            <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-offset-3 col-md-9">
                            <?php if(!$this->unset_back_to_list) { ?>
                                <button id="save-and-go-back-button" type="button" class="btn green">
                                    <i class="fa fa-check"></i>
                                    <?php echo $this->l('form_save_and_go_back'); ?></button>
                                <button id="cancel-button" type="button" class="btn default">
                                    <?php echo $this->l('form_cancel'); ?></button>
                            <?php } ?>
                            <button type="submit" class="btn green"><i class="fa fa-check"></i>
                                <?php echo $this->l('form_save'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        <?php echo form_close(); ?>
        <!-- END FORM-->
    </div>
</div>
<script>
    var validation_url = '<?php echo $validation_url?>';
    var list_url = '<?php echo $list_url?>';

    var message_alert_add_form = "<?php echo $this->l('alert_add_form')?>";
    var message_insert_error = "<?php echo $this->l('insert_error')?>";
</script>