<?php $column_width = (int)(80 / count($columns)); ?>
        <div <?php if(!empty($GLOBALS['table_width'])) echo 'style="width:'.$GLOBALS['table_width'].';"';?>>
            <table class="table table-striped table-bordered table-hover" id="sample_1">
                <thead>
                <?php if (!empty($GLOBALS['search_template'])) {
                    echo $GLOBALS['search_template'];
                } ?>
                <tr>
                    <?php if (!empty($GLOBALS['columns_width'])) { ?>
                        <?php foreach ($columns as $key => $column) { ?>
                            <th style='width:<?= !empty($GLOBALS['columns_width'][$key]) ?
                                $GLOBALS['columns_width'][$key] . '%;' : '' ?>'>
                                <?php echo $column->display_as ?>
                            </th>
                        <?php } ?>
                    <?php } else { ?>
                        <?php foreach ($columns as $column) { ?>
                            <th width='<?php echo $column_width ?>%'>
                                <?php echo $column->display_as ?>
                            </th>
                        <?php } ?>
                    <?php } ?>
                    <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)) { ?>
                        <th width='15%'>
                            <?php echo $this->l('list_actions'); ?>
                        </th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($list as $num_row => $row) { ?>
                    <tr>
                        <?php foreach ($columns as $column) { ?>
                            <td>
                                <?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} :
                                    '&nbsp;'; ?>
                            </td>
                        <?php } ?>
                        <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)) { ?>
                            <td>
                                <div class='tools'>
                                    <?php if (!$unset_edit) { ?>
                                        <a href='<?php echo $row->edit_url ?>'
                                           title='<?php echo $this->l('list_edit') ?> <?php echo $subject ?>'
                                           class="edit_button"><i class='fa fa-pencil'></i>編輯</a>
                                    <?php } ?>
                                    <?php if (false && !$unset_read) { ?>
                                        <a href='<?php echo $row->read_url ?>'
                                           title='<?php echo $this->l('list_view') ?> <?php echo $subject ?>'
                                           class="edit_button"><span class='read-icon'></span>編輯</a>
                                    <?php } ?>
                                    <?php
                                    if (!empty($row->action_urls)) {
                                        foreach ($row->action_urls as $action_unique_id => $action_url) {
                                            $action = $actions[$action_unique_id];
                                            ?>
                                            <a href="<?php echo $action_url; ?>"
                                               class="<?php echo $action->css_class; ?> crud-action"
                                               title="<?php echo $action->label ?>"><?php
                                                if (!empty($action->image_url)) {
                                                    ?><img src="<?php echo $action->image_url; ?>"
                                                           alt="<?php echo $action->label ?>" /><?php
                                                }
                                                ?></a>
                                        <?php }
                                    }
                                    ?>
                                    <div class='clear'></div>
                                </div>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
    </div>
<?php if (empty($GLOBALS['js_template'])) { ?>
    <script>
        <?php
            $columns_def = array();
            foreach ($columns as $column) {
                $columns_def[] = array('orderable' => true);
            }
            if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions))
                $columns_def[] = array('orderable' => false);
        ?>
        var TableManaged = function () {
            var initTable = function () {
                var table = $('#sample_1');
                // begin first table
                table.dataTable({
                    "columns": <?php echo json_encode($columns_def) ?>,
                    "lengthMenu": [
                        [5, 15, 20, -1],
                        [5, 15, 20, "All"] // change per page values here
                    ],
                    // set the initial value
                    "pageLength": 5,
                    "pagingType": "bootstrap_full_number",
                    "language": {
                        "lengthMenu": "  _MENU_ records",
                        "paginate": {
                            "previous": "Prev",
                            "next": "Next",
                            "last": "Last",
                            "first": "First"
                        }
                    },
                    "columnDefs": [{  // set default column settings
                        'orderable': true,
                        'targets': [0]
                    }, {
                        "searchable": false,
                        "targets": [0]
                    }],
                    "order": [
                        [1, "asc"]
                    ] // set first column as a default sort by asc
                });

                var tableWrapper = jQuery('#sample_1_wrapper');

                table.find('.group-checkable').change(function () {
                    var set = jQuery(this).attr("data-set");
                    var checked = jQuery(this).is(":checked");
                    jQuery(set).each(function () {
                        if (checked) {
                            $(this).attr("checked", true);
                            $(this).parents('tr').addClass("active");
                        } else {
                            $(this).attr("checked", false);
                            $(this).parents('tr').removeClass("active");
                        }
                    });
                    jQuery.uniform.update(set);
                });

                table.on('change', 'tbody tr .checkboxes', function () {
                    $(this).parents('tr').toggleClass("active");
                });
                tableWrapper.find('.dataTables_length select').addClass("form-control input-xsmall input-inline"); // modify table per page dropdown
            }

            return {
                init: function () {
                    if (!jQuery().dataTable) {
                        return;
                    }
                    initTable();
                }
            };
        }();
    </script>
<?php } else {
    echo $GLOBALS['js_template'];
} ?>