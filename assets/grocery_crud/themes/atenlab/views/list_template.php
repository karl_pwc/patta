<script type='text/javascript'>
    var base_url = '<?php echo base_url();?>';
    var subject = '<?php echo addslashes($subject); ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
</script>

<!-- BEGIN PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
        <h3 class="page-title">
            <?php echo $subject ?>
        </h3>
        <ul style="display: none;" class="page-breadcrumb breadcrumb">
        </ul>
        <!-- END PAGE TITLE & BREADCRUMB-->
    </div>
</div>

<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box grey-cascade">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-globe"></i><?php echo $subject ?>
                </div>
            </div>
            <div class="portlet-body">
                <div id="table-toolbar" class="table-toolbar">
                    <?php if(!empty($GLOBALS['add'])){
                        echo $GLOBALS['add'];
                    } ?>
                    <?php if (!$unset_add) { ?>
                    <div class="btn-group">
                        <a href="<?php echo $add_url ?>">
                            <button class="btn green">
                                Add New <i class="fa fa-plus"></i>
                            </button>
                        </a>
                    </div>
                    <?php } ?>
                    <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)) { ?>
                    <div class="btn-group pull-right">

                    </div>
                    <?php } ?>
                </div>
                <?php if(!empty($GLOBALS['sql_select'])){
                    echo $GLOBALS['sql_select'];
                } ?>
                <form id="crud_form">
                    <div class="scrollit">
                        <?php echo $list_view ?>
                    </div>
                </form>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<script>
    <?php if($success_message !== null){ ?>
        $(function(){
            form_success_message("<?php echo $success_message; ?>");
        });
    <?php } ?>
</script>