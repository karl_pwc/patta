SELECT
  CASE WHEN packing.db = 'SAP_KP' THEN N'鋐昇實業股份有限公司'
  WHEN packing.db = 'SAP_PA' THEN N'煒鈞實業股份有限公司'
  WHEN packing.db = 'SAP_JP' THEN N'昆山敬鹏建筑五金有限公司'
  WHEN packing.db = 'SAP_BO' THEN N'保信國際有限公司'
  ELSE N'' END AS company_name,
  detail.*,
  packing.*,
  packing_detail.*,
  rdr1.*,
  ordr.*,
  oslp.SlpName
FROM PATTA_PN.dbo.packing_detail packing_detail INNER JOIN PATTA_PN.dbo.packing packing
    ON packing.packing_id = packing_detail.packing_id
  INNER JOIN PATTA_PN.dbo.detail detail
    ON detail.sap_id = packing_detail.rdr1_id AND detail.sap_line = packing_detail.rdr1_line_num AND
       detail.packing_id = packing.packing_id
  INNER JOIN SAP_KP.dbo.RDR1 rdr1 ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
  INNER JOIN SAP_KP.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry
  INNER JOIN SAP_KP.dbo.OSLP oslp ON ordr.SlpCode = OSLP.SlpCode
  LEFT JOIN SAP_KP.dbo.OITM oitm on otim.ItemCode = rdr1.ItemCode
WHERE packing.packing_id = 27
ORDER BY rdr1.DocEntry, rdr1.LineNum

SELECT
  CASE WHEN packing_detail.id IS NULL THEN 0
  ELSE 1 END AS packed,
  CASE WHEN packing.db = 'SAP_KP' THEN N'鋐昇實業股份有限公司'
  WHEN packing.db = 'SAP_PA' THEN N'煒鈞實業股份有限公司'
  WHEN packing.db = 'SAP_JP' THEN N'昆山敬鹏建筑五金有限公司'
  WHEN packing.db = 'SAP_BO' THEN N'保信國際有限公司'
  ELSE N'' END     AS company_name,
  detail.*,
  packing.*,
  packing_detail.*,
  rdr1.*,
  ordr.*,
  oslp.SlpName
FROM PATTA_PN.dbo.detail detail INNER JOIN PATTA_PN.dbo.packing packing
    ON packing.packing_id = detail.packing_id
  LEFT JOIN PATTA_PN.dbo.packing_detail packing_detail
    ON detail.sap_id = packing_detail.rdr1_id AND detail.sap_line = packing_detail.rdr1_line_num AND
       packing_detail.packing_id = packing.packing_id
  INNER JOIN SAP_KP.dbo.RDR1 rdr1 ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
  INNER JOIN SAP_KP.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry
  INNER JOIN SAP_KP.dbo.OSLP oslp ON ordr.SlpCode = OSLP.SlpCode
WHERE packing.packing_id = 27
ORDER BY rdr1.DocEntry, rdr1.LineNum


SELECT
  CASE WHEN packing.db = 'SAP_KP' THEN N'鋐昇實業股份有限公司'
  WHEN packing.db = 'SAP_PA' THEN N'煒鈞實業股份有限公司'
  WHEN packing.db = 'SAP_JP' THEN N'昆山敬鹏建筑五金有限公司'
  WHEN packing.db = 'SAP_BO' THEN N'保信國際有限公司'
  ELSE N'' END AS company_name,
  packing.*,
  packing_detail.*,
  rdr1.*,
  ordr.*
FROM PATTA_PN.dbo.packing_detail packing_detail
  INNER JOIN PATTA_PN.dbo.packing packing ON packing.packing_id = packing_detail.packing_id
  INNER JOIN PATTA_PN.dbo.detail detail
    ON detail.sap_id = packing_detail.rdr1_id AND detail.sap_line = packing_detail.rdr1_line_num
  INNER JOIN SAP_PA.dbo.RDR1 rdr1
    ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
  INNER JOIN SAP_PA.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry;

-- ceo report 檢視表
CREATE VIEW ceo AS (SELECT
                      PATTA.packing_id,
                      N'鋐昇實業股份有限公司'                                           AS company_name,
                      PATTA.date,
                      PATTA.db,
                      PATTA.amount,
                      PATTA.note,
                      PATTA.sap_packing_id,
                      CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_BoxCtn) /
                                                         rdr1.U_MPcsPlt)
                      ELSE 0 END                                              AS total_ctn,
                      CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price
                      ELSE detail.planned_amount * rdr1.Price * rdr1.Rate END AS total_Quantity,
                      cast((datepart(WK, PATTA.date) - datepart(WK, convert(VARCHAR(7), PATTA.date, 120) + '-01') + 1)
                           AS VARCHAR(2))                                     AS wk,
                      RTRIM(DATEPART(WEEKDAY, PATTA.date) -
                            1)                                                AS day,
                      detail.planned_amount,
                      detail.state,
                      ordr.U_PINo,
                      ordr.NumAtCard,
                      ordr.DocDate,
                      ordr.DocDueDate,
                      ordr.U_DocType,
                      CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00002' THEN N'PA-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00004' THEN N'DW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00006' THEN N'XW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00008' THEN N'TD-' + ocrd.CardName
                      ELSE ordr.CardName END                                  AS CardName,
                      ordr.DocCur,
                      rdr1.DocEntry,
                      rdr1.LineNum,
                      rdr1.U_PlannedQty,
                      rdr1.Quantity,
                      rdr1.OpenQty,
                      rdr1.U_BoxCtn,
                      rdr1.U_CtnPLT,
                      rdr1.U_PcsBox,
                      rdr1.U_PLTQty,
                      CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'B' THEN N'拉釘'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'D' THEN N'八大緊固件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'F' THEN N'配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'H' THEN N'螺帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'L' THEN N'螺栓'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'N' THEN N'拉帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'P' THEN N'工具零配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'T' THEN N'工具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'W' THEN N'零配件'
                      ELSE N'' END                                            AS Type,
                      CASE WHEN port.Name IS NULL THEN N''
                      ELSE
                        port.Name + N',' + port.U_Country END                 AS PortName
                    FROM PATTA_PN.dbo.packing PATTA
                      INNER JOIN PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id
                      INNER JOIN SAP_KP.dbo.RDR1 rdr1
                        ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
                      INNER JOIN SAP_KP.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry
                      LEFT JOIN SAP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort
                      LEFT JOIN SAP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode
                      INNER JOIN SAP_KP.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
                    WHERE PATTA.db = 'SAP_KP'

                    UNION ALL

                    SELECT
                      PATTA.packing_id,
                      N'煒鈞實業股份有限公司'                                           AS company_name,
                      PATTA.date,
                      PATTA.db,
                      PATTA.amount,
                      PATTA.note,
                      PATTA.sap_packing_id,
                      CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_BoxCtn) /
                                                         rdr1.U_MPcsPlt)
                      ELSE 0 END                                              AS total_ctn,
                      CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price
                      ELSE detail.planned_amount * rdr1.Price * rdr1.Rate END AS total_Quantity,
                      cast((datepart(WK, PATTA.date) - datepart(WK, convert(VARCHAR(7), PATTA.date, 120) + '-01') + 1)
                           AS VARCHAR(2))                                     AS wk,
                      RTRIM(DATEPART(WEEKDAY, PATTA.date) -
                            1)                                                AS day,
                      detail.planned_amount,
                      detail.state,
                      ordr.U_PINo,
                      ordr.NumAtCard,
                      ordr.DocDate,
                      ordr.DocDueDate,
                      ordr.U_DocType,
                      CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00002' THEN N'PA-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00004' THEN N'DW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00006' THEN N'XW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00008' THEN N'TD-' + ocrd.CardName
                      ELSE ordr.CardName END                                  AS CardName,
                      ordr.DocCur,
                      rdr1.DocEntry,
                      rdr1.LineNum,
                      rdr1.U_PlannedQty,
                      rdr1.Quantity,
                      rdr1.OpenQty,
                      rdr1.U_BoxCtn,
                      rdr1.U_CtnPLT,
                      rdr1.U_PcsBox,
                      rdr1.U_PLTQty,
                      CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'B' THEN N'拉釘'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'D' THEN N'八大緊固件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'F' THEN N'配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'H' THEN N'螺帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'L' THEN N'螺栓'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'N' THEN N'拉帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'P' THEN N'工具零配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'T' THEN N'工具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'W' THEN N'零配件'
                      ELSE N'' END                                            AS Type,
                      CASE WHEN port.Name IS NULL THEN N''
                      ELSE
                        port.Name + N',' + port.U_Country END                 AS PortName
                    FROM PATTA_PN.dbo.packing PATTA
                      INNER JOIN PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id
                      INNER JOIN SAP_PA.dbo.RDR1 rdr1
                        ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
                      INNER JOIN SAP_PA.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry
                      LEFT JOIN SAP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort
                      LEFT JOIN SAP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode
                      INNER JOIN SAP_PA.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
                    WHERE PATTA.db = 'SAP_PA'


                    UNION ALL

                    SELECT
                      PATTA.packing_id,
                      N'昆山敬鹏建筑五金有限公司'                                         AS company_name,
                      PATTA.date,
                      PATTA.db,
                      PATTA.amount,
                      PATTA.note,
                      PATTA.sap_packing_id,
                      CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_BoxCtn) /
                                                         rdr1.U_MPcsPlt)
                      ELSE 0 END                                              AS total_ctn,
                      CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price
                      ELSE detail.planned_amount * rdr1.Price * rdr1.Rate END AS total_Quantity,
                      cast((datepart(WK, PATTA.date) - datepart(WK, convert(VARCHAR(7), PATTA.date, 120) + '-01') + 1)
                           AS VARCHAR(2))                                     AS wk,
                      RTRIM(DATEPART(WEEKDAY, PATTA.date) -
                            1)                                                AS day,
                      detail.planned_amount,
                      detail.state,
                      ordr.U_PINo,
                      ordr.NumAtCard,
                      ordr.DocDate,
                      ordr.DocDueDate,
                      ordr.U_DocType,
                      CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00002' THEN N'PA-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00004' THEN N'DW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00006' THEN N'XW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00008' THEN N'TD-' + ocrd.CardName
                      ELSE ordr.CardName END                                  AS CardName,
                      ordr.DocCur,
                      rdr1.DocEntry,
                      rdr1.LineNum,
                      rdr1.U_PlannedQty,
                      rdr1.Quantity,
                      rdr1.OpenQty,
                      rdr1.U_BoxCtn,
                      rdr1.U_CtnPLT,
                      rdr1.U_PcsBox,
                      rdr1.U_PLTQty,
                      CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'B' THEN N'拉釘'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'D' THEN N'八大緊固件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'F' THEN N'配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'H' THEN N'螺帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'L' THEN N'螺栓'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'N' THEN N'拉帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'P' THEN N'工具零配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'T' THEN N'工具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'W' THEN N'零配件'
                      ELSE N'' END                                            AS Type,
                      CASE WHEN port.Name IS NULL THEN N''
                      ELSE
                        port.Name + N',' + port.U_Country END                 AS PortName
                    FROM PATTA_PN.dbo.packing PATTA
                      INNER JOIN PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id
                      INNER JOIN SAP_JP.dbo.RDR1 rdr1
                        ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
                      INNER JOIN SAP_JP.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry
                      LEFT JOIN SAP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort
                      LEFT JOIN SAP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode
                      INNER JOIN SAP_JP.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
                    WHERE PATTA.db = 'SAP_JP'


                    UNION ALL

                    SELECT
                      PATTA.packing_id,
                      N'保信國際有限公司'                                             AS company_name,
                      PATTA.date,
                      PATTA.db,
                      PATTA.amount,
                      PATTA.note,
                      PATTA.sap_packing_id,
                      CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_CtnPLT) /
                                                         rdr1.U_MPcsPlt)
                      ELSE 0 END                                              AS total_ctn,
                      CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price
                      ELSE detail.planned_amount * rdr1.Price * rdr1.Rate END AS total_Quantity,
                      cast((datepart(WK, PATTA.date) - datepart(WK, convert(VARCHAR(7), PATTA.date, 120) + '-01') + 1)
                           AS VARCHAR(2))                                     AS wk,
                      RTRIM(DATEPART(WEEKDAY, PATTA.date) -
                            1)                                                AS day,
                      detail.planned_amount,
                      detail.state,
                      ordr.U_PINo,
                      ordr.NumAtCard,
                      ordr.DocDate,
                      ordr.DocDueDate,
                      ordr.U_DocType,
                      CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00002' THEN N'PA-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00004' THEN N'DW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00006' THEN N'XW-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName
                      WHEN ordr.CardCode = 'A00008' THEN N'TD-' + ocrd.CardName
                      ELSE ordr.CardName END                                  AS CardName,
                      ordr.DocCur,
                      rdr1.DocEntry,
                      rdr1.LineNum,
                      rdr1.U_PlannedQty,
                      rdr1.Quantity,
                      rdr1.OpenQty,
                      rdr1.U_BoxCtn,
                      rdr1.U_CtnPLT,
                      rdr1.U_PcsBox,
                      rdr1.U_PLTQty,
                      CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'B' THEN N'拉釘'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'D' THEN N'八大緊固件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'F' THEN N'配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'H' THEN N'螺帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'L' THEN N'螺栓'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'N' THEN N'拉帽'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'P' THEN N'工具零配件'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'T' THEN N'工具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具'
                      WHEN Substring(rdr1.[ItemCode], 1, 1) = 'W' THEN N'零配件'
                      ELSE N'' END                                            AS Type,
                      CASE WHEN port.Name IS NULL THEN N''
                      ELSE
                        port.Name + N',' + port.U_Country END                 AS PortName
                    FROM PATTA_PN.dbo.packing PATTA
                      INNER JOIN PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id
                      INNER JOIN SAP_BO.dbo.RDR1 rdr1
                        ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
                      INNER JOIN SAP_BO.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry
                      LEFT JOIN SAP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort
                      LEFT JOIN SAP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode
                      INNER JOIN SAP_BO.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
                    WHERE PATTA.db = 'SAP_BO');




--
-- SELECT          PATTA.packing_id, N'鋐昇實業股份有限公司' AS company_name, PATTA.date, PATTA.db, PATTA.amount, PATTA.note,
--   PATTA.sap_packing_id,
--   CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_BoxCtn) / rdr1.U_MPcsPlt)
--   ELSE 0 END AS total_ctn,
--   CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price ELSE detail.planned_amount * rdr1.Price *
--                                                                              rdr1.Rate END AS total_Quantity, cast((datepart(WK, PATTA.date) - datepart(WK, CONVERT(VARCHAR(7), PATTA.date,
--                                                                                                                                                                     120) + '-01') + 1) AS VARCHAR(2)) AS wk, RTRIM(DATEPART(WEEKDAY, PATTA.date) - 1) AS day,
--   detail.planned_amount, detail.state, ordr.U_PINo, ordr.NumAtCard, ordr.DocDate, ordr.DocDueDate, ordr.U_DocType,
--   CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName WHEN ordr.CardCode = 'A00002' THEN N'PA-' +
--                                                                                                     ocrd.CardName WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName WHEN ordr.CardCode = 'A00004' THEN
--     N'DW-' + ocrd.CardName WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName WHEN ordr.CardCode = 'A00006'
--   THEN N'XW-' + ocrd.CardName WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName WHEN ordr.CardCode
--                                                                                              = 'A00008' THEN N'TD-' + ocrd.CardName ELSE ordr.CardName END AS CardName, ordr.DocCur, rdr1.DocEntry,
--   rdr1.LineNum, rdr1.U_PlannedQty, rdr1.Quantity, rdr1.OpenQty, rdr1.U_BoxCtn, rdr1.U_CtnPLT, rdr1.U_PcsBox,
--   rdr1.U_PLTQty, CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金' WHEN Substring(rdr1.[ItemCode],
--                                                                                               1, 1) = 'B' THEN N'拉釘' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                            1, 1) = 'D' THEN N'八大緊固件' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                           = 'F' THEN N'配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                        = 'H' THEN N'螺帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                     = 'L' THEN N'螺栓' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                  = 'N' THEN N'拉帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               = 'P' THEN N'工具零配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         1, 1) = 'T' THEN N'工具' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      1, 1) = 'W' THEN N'零配件' ELSE N'' END AS Type, CASE WHEN port.Name IS NULL
-- THEN N'' ELSE port.Name + N',' + port.U_Country END AS PortName
-- FROM              PATTA_PN.dbo.packing PATTA INNER JOIN
--   PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id INNER JOIN
--   ERP_KP.dbo.RDR1 rdr1 ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line INNER JOIN
--   ERP_KP.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry LEFT JOIN
--   ERP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort LEFT JOIN
--   ERP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode INNER JOIN
--   ERP_KP.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
-- WHERE          PATTA.db = 'ERP_KP'
-- UNION ALL
-- SELECT          PATTA.packing_id, N'煒鈞實業股份有限公司' AS company_name, PATTA.date, PATTA.db, PATTA.amount, PATTA.note,
--   PATTA.sap_packing_id,
--   CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_BoxCtn) / rdr1.U_MPcsPlt)
--   ELSE 0 END AS total_ctn,
--   CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price ELSE detail.planned_amount * rdr1.Price *
--                                                                              rdr1.Rate END AS total_Quantity, cast((datepart(WK, PATTA.date) - datepart(WK, CONVERT(VARCHAR(7), PATTA.date,
--                                                                                                                                                                     120) + '-01') + 1) AS VARCHAR(2)) AS wk, RTRIM(DATEPART(WEEKDAY, PATTA.date) - 1) AS day,
--   detail.planned_amount, detail.state, ordr.U_PINo, ordr.NumAtCard, ordr.DocDate, ordr.DocDueDate, ordr.U_DocType,
--   CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName WHEN ordr.CardCode = 'A00002' THEN N'PA-' +
--                                                                                                     ocrd.CardName WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName WHEN ordr.CardCode = 'A00004' THEN
--     N'DW-' + ocrd.CardName WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName WHEN ordr.CardCode = 'A00006'
--   THEN N'XW-' + ocrd.CardName WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName WHEN ordr.CardCode
--                                                                                              = 'A00008' THEN N'TD-' + ocrd.CardName ELSE ordr.CardName END AS CardName, ordr.DocCur, rdr1.DocEntry,
--   rdr1.LineNum, rdr1.U_PlannedQty, rdr1.Quantity, rdr1.OpenQty, rdr1.U_BoxCtn, rdr1.U_CtnPLT, rdr1.U_PcsBox,
--   rdr1.U_PLTQty, CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金' WHEN Substring(rdr1.[ItemCode],
--                                                                                               1, 1) = 'B' THEN N'拉釘' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                            1, 1) = 'D' THEN N'八大緊固件' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                           = 'F' THEN N'配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                        = 'H' THEN N'螺帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                     = 'L' THEN N'螺栓' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                  = 'N' THEN N'拉帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               = 'P' THEN N'工具零配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         1, 1) = 'T' THEN N'工具' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      1, 1) = 'W' THEN N'零配件' ELSE N'' END AS Type, CASE WHEN port.Name IS NULL
-- THEN N'' ELSE port.Name + N',' + port.U_Country END AS PortName
-- FROM              PATTA_PN.dbo.packing PATTA INNER JOIN
--   PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id INNER JOIN
--   ERP_PA.dbo.RDR1 rdr1 ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line INNER JOIN
--   ERP_PA.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry LEFT JOIN
--   ERP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort LEFT JOIN
--   ERP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode INNER JOIN
--   ERP_PA.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
-- WHERE          PATTA.db = 'ERP_PA'
-- UNION ALL
-- SELECT          PATTA.packing_id, N'昆山敬鹏建筑五金有限公司' AS company_name, PATTA.date, PATTA.db, PATTA.amount,
--   PATTA.note, PATTA.sap_packing_id,
--   CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_BoxCtn) / rdr1.U_MPcsPlt)
--   ELSE 0 END AS total_ctn,
--   CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price ELSE detail.planned_amount * rdr1.Price *
--                                                                              rdr1.Rate END AS total_Quantity, cast((datepart(WK, PATTA.date) - datepart(WK, CONVERT(VARCHAR(7), PATTA.date,
--                                                                                                                                                                     120) + '-01') + 1) AS VARCHAR(2)) AS wk, RTRIM(DATEPART(WEEKDAY, PATTA.date) - 1) AS day,
--   detail.planned_amount, detail.state, ordr.U_PINo, ordr.NumAtCard, ordr.DocDate, ordr.DocDueDate, ordr.U_DocType,
--   CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName WHEN ordr.CardCode = 'A00002' THEN N'PA-' +
--                                                                                                     ocrd.CardName WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName WHEN ordr.CardCode = 'A00004' THEN
--     N'DW-' + ocrd.CardName WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName WHEN ordr.CardCode = 'A00006'
--   THEN N'XW-' + ocrd.CardName WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName WHEN ordr.CardCode
--                                                                                              = 'A00008' THEN N'TD-' + ocrd.CardName ELSE ordr.CardName END AS CardName, ordr.DocCur, rdr1.DocEntry,
--   rdr1.LineNum, rdr1.U_PlannedQty, rdr1.Quantity, rdr1.OpenQty, rdr1.U_BoxCtn, rdr1.U_CtnPLT, rdr1.U_PcsBox,
--   rdr1.U_PLTQty, CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金' WHEN Substring(rdr1.[ItemCode],
--                                                                                               1, 1) = 'B' THEN N'拉釘' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                            1, 1) = 'D' THEN N'八大緊固件' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                           = 'F' THEN N'配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                        = 'H' THEN N'螺帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                     = 'L' THEN N'螺栓' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                  = 'N' THEN N'拉帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               = 'P' THEN N'工具零配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         1, 1) = 'T' THEN N'工具' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      1, 1) = 'W' THEN N'零配件' ELSE N'' END AS Type, CASE WHEN port.Name IS NULL
-- THEN N'' ELSE port.Name + N',' + port.U_Country END AS PortName
-- FROM              PATTA_PN.dbo.packing PATTA INNER JOIN
--   PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id INNER JOIN
--   ERP_JP.dbo.RDR1 rdr1 ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line INNER JOIN
--   ERP_JP.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry LEFT JOIN
--   ERP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort LEFT JOIN
--   ERP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode INNER JOIN
--   ERP_JP.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
-- WHERE          PATTA.db = 'ERP_JP'
-- UNION ALL
-- SELECT          PATTA.packing_id, N'保信國際有限公司' AS company_name, PATTA.date, PATTA.db, PATTA.amount, PATTA.note,
--   PATTA.sap_packing_id,
--   CASE WHEN rdr1.U_MPcsPlt > 0 THEN ((detail.planned_amount * rdr1.U_CtnPLT * rdr1.U_CtnPLT) / rdr1.U_MPcsPlt)
--   ELSE 0 END AS total_ctn,
--   CASE WHEN ordr.DocCur = 'NTD' THEN detail.planned_amount * rdr1.Price ELSE detail.planned_amount * rdr1.Price *
--                                                                              rdr1.Rate END AS total_Quantity, cast((datepart(WK, PATTA.date) - datepart(WK, CONVERT(VARCHAR(7), PATTA.date,
--                                                                                                                                                                     120) + '-01') + 1) AS VARCHAR(2)) AS wk, RTRIM(DATEPART(WEEKDAY, PATTA.date) - 1) AS day,
--   detail.planned_amount, detail.state, ordr.U_PINo, ordr.NumAtCard, ordr.DocDate, ordr.DocDueDate, ordr.U_DocType,
--   CASE WHEN ordr.CardCode = 'A00001' THEN N'KP-' + ocrd.CardName WHEN ordr.CardCode = 'A00002' THEN N'PA-' +
--                                                                                                     ocrd.CardName WHEN ordr.CardCode = 'A00003' THEN N'KF-' + ocrd.CardName WHEN ordr.CardCode = 'A00004' THEN
--     N'DW-' + ocrd.CardName WHEN ordr.CardCode = 'A00005' THEN N'BO-' + ocrd.CardName WHEN ordr.CardCode = 'A00006'
--   THEN N'XW-' + ocrd.CardName WHEN ordr.CardCode = 'A00007' THEN N'JP-' + ocrd.CardName WHEN ordr.CardCode
--                                                                                              = 'A00008' THEN N'TD-' + ocrd.CardName ELSE ordr.CardName END AS CardName, ordr.DocCur, rdr1.DocEntry,
--   rdr1.LineNum, rdr1.U_PlannedQty, rdr1.Quantity, rdr1.OpenQty, rdr1.U_BoxCtn, rdr1.U_CtnPLT, rdr1.U_PcsBox,
--   rdr1.U_PLTQty, CASE WHEN Substring(rdr1.[ItemCode], 1, 1) = 'A' THEN N'建築五金' WHEN Substring(rdr1.[ItemCode],
--                                                                                               1, 1) = 'B' THEN N'拉釘' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'C' THEN N'釘類' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                            1, 1) = 'D' THEN N'八大緊固件' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                           = 'F' THEN N'配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'G' THEN N'銲材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                        = 'H' THEN N'螺帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'J' THEN N'鑽頭' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                     = 'L' THEN N'螺栓' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'M' THEN N'原料' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                  = 'N' THEN N'拉帽' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'O' THEN N'包材' WHEN Substring(rdr1.[ItemCode], 1, 1)
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               = 'P' THEN N'工具零配件' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'S' THEN N'螺絲' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         1, 1) = 'T' THEN N'工具' WHEN Substring(rdr1.[ItemCode], 1, 1) = 'U' THEN N'模具' WHEN Substring(rdr1.[ItemCode],
--                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      1, 1) = 'W' THEN N'零配件' ELSE N'' END AS Type, CASE WHEN port.Name IS NULL
-- THEN N'' ELSE port.Name + N',' + port.U_Country END AS PortName
-- FROM              PATTA_PN.dbo.packing PATTA INNER JOIN
--   PATTA_PN.dbo.detail detail ON detail.packing_id = PATTA.packing_id INNER JOIN
--   ERP_BO.dbo.RDR1 rdr1 ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line INNER JOIN
--   ERP_BO.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry LEFT JOIN
--   ERP_KP.[dbo].[@PORTLIST] port ON port.Code = ordr.U_ArrivalPort LEFT JOIN
--   ERP_KP.dbo.OCRD ocrd ON ordr.U_CTX_CMNT = ocrd.CardCode INNER JOIN
--   ERP_BO.dbo.OITM oitm ON rdr1.ItemCode = oitm.ItemCode
