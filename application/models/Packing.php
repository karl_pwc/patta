<?php

class Packing extends CI_Model
{

    public function get_owor_list($db_name)
    {
        $this->db->from("$db_name.dbo.OWOR");
        $this->db->join("$db_name.dbo.OITM", "$db_name.dbo.OITM.ItemCode = $db_name.dbo.OWOR.ItemCode");
        $this->db->select("OWOR.DocEntry, OWOR.ItemCode, OWOR.PlannedQty, OWOR.DueDate, OITM.U_SpecM,OITM.U_SpecE, OITM.ItemName");
        return $this->db->get()->result();
    }


    public function transform($packing_id, $doc_num, $line, $target, $source_packing_id)
    {
        $this->db->where('packing_id', $packing_id);
        $db_name = $this->db->get('packing')->row()->db;

        $doc_num = intval($doc_num);
        $line = intval($line);
        $this->db->where('sap_id', $doc_num);
        $this->db->where('sap_line', $line);
        $this->db->where('packing_id', $source_packing_id);
        $this->db->update('detail', array(
            'packing_id' => $packing_id,
            'state' => $target
        ));

        $this->db->where('packing_id', $source_packing_id);
        $this->db->where('rdr1_id', $doc_num);
        $this->db->where('rdr1_line_num', $line);
        $this->db->update('packing_detail', array('packing_id' => $packing_id));
        $this->update_planned_qty($packing_id, $db_name, $doc_num, $line);
    }

    public function update_planned_qty($packing_id, $db_name, $doc_entry, $line_num)
    {
        $this->db->where('db', $db_name);
        $packing_ids = [];
        foreach ($this->db->get('packing')->result() as $row) {
            $packing_ids = $row->packing_id;
        }
        $this->db->select_sum('planned_amount');
        $this->db->where_in('packing_id', $packing_ids);
        $this->db->where('sap_id', $doc_entry);
        $this->db->where('sap_line', $line_num);
        $total_planned_amount = $this->db->get('detail')->row()->planned_amount;
        $data = array(
            'U_PlannedQty' => floatval($total_planned_amount)
        );
        $this->db->where('DocEntry', $doc_entry);
        $this->db->where('LineNum', $line_num);
        $this->db->update("$db_name.dbo.RDR1", $data);
    }

    public function cancel_packing($db_name, $id)
    {
        $rows = $this->db->select("$db_name.dbo.RDR1.DocEntry, $db_name.dbo.RDR1.LineNum")->from("PATTA_PN.dbo.packing")
            ->join("PATTA_PN.dbo.detail", "PATTA_PN.dbo.detail.packing_id = PATTA_PN.dbo.packing.packing_id", 'inner', false)
            ->join("$db_name.dbo.RDR1",
                "$db_name.dbo.RDR1.DocEntry = PATTA_PN.dbo.detail.sap_id and $db_name.dbo.RDR1.LineNum = PATTA_PN.dbo.detail.sap_line",
                'inner', false)->where("PATTA_PN.dbo.packing.db", $db_name)
            ->order_by("PATTA_PN.dbo.detail.packing_id asc, PATTA_PN.dbo.detail.sap_line asc")
            ->where("PATTA_PN.dbo.packing.packing_id", $id)->get()->result();

        foreach ($rows as $row) {
            $this->cancel_detail($id, $db_name, $row->DocEntry, $row->LineNum);
        }
        $this->db->delete('packing_detail', array('packing_id' => $id));
        $this->db->delete('packing', array('packing_id' => $id));
        $this->db->delete('detail', array('packing_id' => $id));
    }

    public function cancel_detail($packing_id, $db_name, $doc_entry, $line_num)
    {
        $this->db->delete('packing_detail', array(
            'packing_id' => $packing_id,
            'rdr1_id' => $doc_entry,
            'rdr1_line_num' => $line_num
        ));
        $this->db->delete('detail', array(
            'packing_id' => $packing_id,
            'sap_id' => $doc_entry,
            'sap_line' => $line_num
        ));
        $this->update_planned_qty($packing_id, $db_name, $doc_entry, $line_num);
    }

    public function get_db_name($id)
    {
        $this->db->where("packing_id", $id);
        return $this->db->get('packing')->row()->db;
    }

    public function get_list_for_view($db_name = DEFAULT_DB)
    {
        $rows = $this->_get_packing_data($db_name)->get()->result();
        $data = [];
        foreach ($rows as $row) {
            $row->detail_total = floatval($this->get_detail_total($row->packing_id, $row->DocEntry, $row->LineNum));
            $data[$row->packing_id][] = $row;
        }
        return $data;
    }

    public function get_list_for_delivery($db_name = DEFAULT_DB)
    {
        $rows = $this->_get_packing_data($db_name)->where("PATTA_PN.dbo.detail.state", "D")->get()->result();
        $data = [];
        foreach ($rows as $row) {
            $row->detail_total = floatval($this->get_detail_total($row->packing_id, $row->DocEntry, $row->LineNum));
            $data[$row->packing_id][] = $row;
        }
        return $data;
    }

    public function get_list_for_shipping($db_name = DEFAULT_DB)
    {
        $rows = $this->_get_packing_data($db_name)->where("PATTA_PN.dbo.detail.state", "C")->get()->result();
        $data = [];
        foreach ($rows as $row) {
            $row->detail_total = $this->get_detail_total($row->packing_id, $row->DocEntry, $row->LineNum);
            $data[$row->packing_id][] = $row;
        }
        foreach ($data as $key => $rows) {
            $data[$key][0]->total_mpcs = $this->get_total($data[$key][0]->packing_id);
        }

        return $data;
    }

    public function get_detail_total($packing_id, $rdr1_id, $rdr1_line_num)
    {
        $this->db->where('packing_id', $packing_id);
        $this->db->where('rdr1_id', $rdr1_id);
        $this->db->where('rdr1_line_num', $rdr1_line_num);
        return $this->db->select_sum('mpcs')->get('packing_detail')->row()->mpcs;
    }

    public function get_total($packing_id)
    {
        $this->db->where('packing_id', $packing_id);
        return $this->db->select_sum('mpcs')->get('packing_detail')->row()->mpcs;
    }

    public function get_edit_for_factory($id, $db_name = DEFAULT_DB)
    {
        $rows = $this->_get_packing_data($db_name)
            ->where("(PATTA_PN.dbo.detail.state = 'B' or PATTA_PN.dbo.detail.state = 'C' or PATTA_PN.dbo.detail.state = 'D')")
            ->where("PATTA_PN.dbo.packing.packing_id", $id)->get()->result();
        return $rows;
    }

    public function get_list_for_factory($db_name = DEFAULT_DB)
    {
        $rows = $this->_get_packing_data($db_name)
            ->where("(PATTA_PN.dbo.detail.state = 'B' or PATTA_PN.dbo.detail.state = 'C' or PATTA_PN.dbo.detail.state = 'D')")->get()
            ->result();
        $data = [];
        foreach ($rows as $row) {
            $row->detail_total = floatval($this->get_detail_total($row->packing_id, $row->DocEntry, $row->LineNum));
            $data[$row->packing_id][] = $row;
        }
        return $data;
    }

    public function get_list_for_business($db_name = DEFAULT_DB)
    {
        $rows = $this->_get_packing_data($db_name)->where("PATTA_PN.dbo.detail.state", "A")->get()->result();
        $data = [];
        foreach ($rows as $row) {
            $data[$row->packing_id][] = $row;
        }
        return $data;
    }

    /**
     * 列出所有出貨計畫表 跟ORDR RDR1 join後的資料
     * @param $id
     * @param string $db_name
     */
    public function _get_packing_data($db_name = DEFAULT_DB)
    {
        return $this->db->select("PATTA_PN.dbo.packing.*, PATTA_PN.dbo.detail.*, $db_name.dbo.RDR1.U_PlannedQty, $db_name.dbo.RDR1.U_PltQty,$db_name.dbo.ORDR.DocEntry,$db_name.dbo.OSLP.SlpName,$db_name.dbo.ORDR.DocCur, $db_name.dbo.ORDR.U_PINO, $db_name.dbo.ORDR.CardCode, $db_name.dbo.ORDR.Footer, $db_name.dbo.ORDR.CardName, $db_name.dbo.ORDR.DocDueDate, $db_name.dbo.ORDR.U_DocType, $db_name.dbo.ORDR.U_ShipPort, $db_name.dbo.ORDR.SlpCode, $db_name.dbo.RDR1.U_SpecM, $db_name.dbo.RDR1.U_SpecE, $db_name.dbo.RDR1.Quantity, $db_name.dbo.RDR1.OpenQty, $db_name.dbo.RDR1.unitMsr, $db_name.dbo.RDR1.WhsCode, $db_name.dbo.RDR1.LineNum, $db_name.dbo.RDR1.ItemCode, $db_name.dbo.OITM.ItemName, $db_name.dbo.OITW.OnHand, $db_name.dbo.RDR1.U_PcsBox, $db_name.dbo.RDR1.U_BoxCtn, $db_name.dbo.RDR1.U_CtnPLT, $db_name.dbo.RDR1.U_MPcsPLT, $db_name.dbo.@EXPORTPORT.Name")
            ->from("PATTA_PN.dbo.packing")
            ->join("PATTA_PN.dbo.detail", "PATTA_PN.dbo.detail.packing_id = PATTA_PN.dbo.packing.packing_id", 'inner', false)
            ->join("$db_name.dbo.RDR1",
                "$db_name.dbo.RDR1.DocEntry = PATTA_PN.dbo.detail.sap_id and $db_name.dbo.RDR1.LineNum = PATTA_PN.dbo.detail.sap_line",
                'inner', false)->where("PATTA_PN.dbo.packing.db", $db_name)
            ->join("$db_name.dbo.ORDR", "$db_name.dbo.ORDR.DocEntry = $db_name.dbo.RDR1.DocEntry")
            ->join("$db_name.dbo.OSLP", "$db_name.dbo.OSLP.SlpCode = $db_name.dbo.ORDR.SlpCode")
            ->join("$db_name.dbo.OITM", "$db_name.dbo.RDR1.ItemCode = $db_name.dbo.OITM.ItemCode", 'left')
            ->join("$db_name.dbo.OITW",
                "$db_name.dbo.OITW.ItemCode = $db_name.dbo.OITM.ItemCode and $db_name.dbo.RDR1.WhsCode = $db_name.dbo.OITW.WhsCode",
                'left')
            ->join("$db_name.dbo.[@EXPORTPORT]", "$db_name.dbo.[@EXPORTPORT].Code = $db_name.dbo.ORDR.U_ShipPort",
                'left', false)->order_by("PATTA_PN.dbo.detail.sap_id asc, PATTA_PN.dbo.detail.sap_line asc")
            ->where("left($db_name.dbo.RDR1.ItemCode, 1) != 'Z'");
    }

    /**
     * 取得裝箱明細
     * @param $id
     * @return array
     */
    public function get_detail($id)
    {
        $this->db->where("PATTA_PN.dbo.packing_detail.packing_id", $id);
        $result = $this->db->get('PATTA_PN.dbo.packing_detail')->result();
        $data = [];
        foreach ((array)$result as $row) {
            $data[$row->rdr1_id][$row->rdr1_line_num][] = $row;
        }
        return $data;
    }

    /**
     * 是否超過庫存
     * @param $_item_code
     * @param $_whs_code
     * @param $amount
     * @param $db_name
     * @return bool
     */
    public function out_of_stock($_item_code, $amount, $db_name)
    {
        if ($db_name == 'SAP_KP' || $db_name == 'SAP_PA' || $db_name == 'SAP_KF') {
            $this->db->where("SAP_KP.dbo.OITM.ItemCode", $_item_code); // itemCode
            $this->db->where("SAP_KP.dbo.OITM.OnHand >=", $amount); // 庫存是否大於數量
            return $this->db->get("SAP_KP.dbo.OITM")->num_rows() == 0;
        }

        if ($db_name == 'BO') {

        }
    }

    /**
     * 取得庫存
     */
    public function get_on_hand($item_code, $db_name)
    {
        if ($db_name == 'SAP_KP' || $db_name == 'SAP_PA' || $db_name == 'SAP_KF') {
            $this->db->select("$db_name.dbo.OITM.OnHand")->from("$db_name.dbo.OITM");
            $this->db->where("ItemCode", $item_code);
            return $this->db->get()->row()->OnHand;
        } else {

        }
    }

    /**
     * 取得 "新增出貨計畫表" 所需的資料
     * @param $start
     * @param $length
     * @param $search
     * @param string $db_name
     * @return array
     */
    public function get_list_for_add($start, $length, $search, $db_name = DEFAULT_DB)
    {
        $select =
            "$db_name.dbo.ORDR.DocEntry, max($db_name.dbo.RDR1.U_PlannedQty) as U_PlannedQty, max($db_name.dbo.RDR1.Quantity) as Quantity,max($db_name.dbo.OSLP.SlpName) as SlpName,max($db_name.dbo.ORDR.DocCur) as DocCur, max($db_name.dbo.ORDR.U_PINO) as U_PINO, max($db_name.dbo.ORDR.CardCode) as CardCode, max($db_name.dbo.ORDR.CardName) as CardName, max($db_name.dbo.ORDR.DocDueDate) as DocDueDate, max($db_name.dbo.ORDR.U_DocType) as U_DocType, max($db_name.dbo.ORDR.U_ShipPort) as U_ShipPort, max($db_name.dbo.ORDR.SlpCode) as SlpCode";
        $return = [];

        $db = $this->_get_master_data($db_name);
        $db = $db->select($select)->limit($length, $start)->order_by("max($db_name.dbo.ORDR.DocDueDate)");


        if ($search['DocEntry'] != '0,0') {
            $DocEntry = explode(',', $search['DocEntry']);
            if (isset($DocEntry[0]) && intval($DocEntry[0]) > 0) {
                $db->where("$db_name.dbo.ORDR.DocEntry >=", intval($DocEntry[0]));
            }
            if (isset($DocEntry[1]) && intval($DocEntry[1]) > 0) {
                $db->where("$db_name.dbo.ORDR.DocEntry <=", intval($DocEntry[1]));
            }
        }
        if ($search['U_PINO'] != ',') {
            $U_PINO = explode(',', $search['U_PINO']);
            if (isset($U_PINO[0]) && $U_PINO[0] != '') {
                $db->where("$db_name.dbo.ORDR.U_PINO >= ", $U_PINO[0]);
            }
            if (isset($U_PINO[1]) && $U_PINO[1] != '') {
                $db->where("$db_name.dbo.ORDR.U_PINO <= ", $U_PINO[1]);
            }
        }
        if ($search['CardName']) {
            $db->like("$db_name.dbo.ORDR.CardName", $search['CardName']);
        }
        if ($search['SlpName']) {
            $db->like("$db_name.dbo.OSLP.SlpName", $search['SlpName']);
        }
        if ($search['DocCur']) {
            $db->like("$db_name.dbo.ORDR.DocCur", $search['DocCur']);
        }
        if ($search['DocDueDate'] != ',') {
            $DocDueDate = explode(',', $search['DocDueDate']);
            if (isset($DocDueDate[0]) && $DocDueDate[0] != '') {
                $db->where("$db_name.dbo.ORDR.DocDueDate >= ", $DocDueDate[0]);
            }
            if (isset($DocDueDate[1]) && $DocDueDate[1] != '') {
                $db->where("$db_name.dbo.ORDR.DocDueDate <= ", $DocDueDate[1]);
            }
        }
        if ($search['DocType']) {
            $db->like("$db_name.dbo.ORDR.U_DocType", $search['DocType']);
        }
        if ($search['U_Ship']) {
            $db->like("$db_name.dbo.@EXPORTPORT.Name", $search['U_Ship']);
        }
        $return['data'] = $db->get()->result();

        $db = $this->_get_master_data($db_name);
        $db = $db->select($select);

        if ($search['DocEntry'] != '0,0') {
            $DocEntry = explode(',', $search['DocEntry']);
            if (isset($DocEntry[0]) && intval($DocEntry[0]) > 0) {
                $db->where("$db_name.dbo.ORDR.DocEntry >=", intval($DocEntry[0]));
            }
            if (isset($DocEntry[1]) && intval($DocEntry[1]) > 0) {
                $db->where("$db_name.dbo.ORDR.DocEntry <=", intval($DocEntry[1]));
            }
        }
        if ($search['U_PINO'] != ',') {
            $U_PINO = explode(',', $search['U_PINO']);
            if (isset($U_PINO[0]) && $U_PINO[0] != '') {
                $db->where("$db_name.dbo.ORDR.U_PINO >= ", $U_PINO[0]);
            }
            if (isset($U_PINO[1]) && $U_PINO[1] != '') {
                $db->where("$db_name.dbo.ORDR.U_PINO <= ", $U_PINO[1]);
            }
        }
        if ($search['CardName']) {
            $db->like("$db_name.dbo.ORDR.CardName", $search['CardName']);
        }
        if ($search['SlpName']) {
            $db->like("$db_name.dbo.OSLP.SlpName", $search['SlpName']);
        }
        if ($search['DocCur']) {
            $db->like("$db_name.dbo.ORDR.DocCur", $search['DocCur']);
        }
        if ($search['DocDueDate'] != ',') {
            $DocDueDate = explode(',', $search['DocDueDate']);
            if (isset($DocDueDate[0]) && $DocDueDate[0] != '') {
                $db->where("$db_name.dbo.ORDR.DocDueDate >= ", $DocDueDate[0]);
            }
            if (isset($DocDueDate[1]) && $DocDueDate[1] != '') {
                $db->where("$db_name.dbo.ORDR.DocDueDate <= ", $DocDueDate[1]);
            }
        }
        if ($search['DocType']) {
            $db->like("$db_name.dbo.ORDR.U_DocType", $search['DocType']);
        }
        if ($search['U_Ship']) {
            $db->like("$db_name.dbo.@EXPORTPORT.Name", $search['U_Ship']);
        }
        $return['count'] = $db->get()->num_rows();
        return $return;
    }

    public function get_ship_name($code, $db_name = DEFAULT_DB)
    {
        $query = $this->db->where('Code', $code)->get("$db_name.dbo.@EXPORTPORT");
        if ($query->num_rows() > 0) {
            return $query->row()->Name;
        }
        return '';
    }

    /**
     * 根據 訂單DocEntry,狀態 取得RDR1資料
     * @param $id
     * @param string $db_name
     * @param string $state
     * @return mixed
     */
    public function get_line_data_by_id($id, $db_name = DEFAULT_DB)
    {
        return $this->db->from("$db_name.dbo.RDR1")
            ->select("$db_name.dbo.RDR1.*, $db_name.dbo.OITM.ItemName, $db_name.dbo.OITW.OnHand")
            ->join("$db_name.dbo.OITM", "$db_name.dbo.RDR1.ItemCode = $db_name.dbo.OITM.ItemCode", 'left')
            ->join("$db_name.dbo.OITW",
                "$db_name.dbo.OITW.ItemCode = $db_name.dbo.OITM.ItemCode and $db_name.dbo.RDR1.WhsCode = $db_name.dbo.OITW.WhsCode",
                'left')->where("$db_name.dbo.RDR1.DocEntry", $id);
    }

    /**
     * 更新 ORDR 的狀態. 以一個出貨計畫表(packing_id) 為單位更新
     * @param $id
     * @param $state_from
     * @param $state_to
     * @param string $db_name
     */
    public function update_state($id, $state_from, $state_to)
    {
        $this->db->where('packing_id', $id);
        if (!empty($state_from)) {
            $this->db->where('state', $state_from);
        }
        $this->db->update('detail', [
            'state' => $state_to
        ]);
    }


    /**
     * 根據狀態 取得ORDR資料
     * @param string $db_name
     * @param string $state
     * @return mixed
     */
    protected function _get_master_data($db_name = DEFAULT_DB)
    {
        return $this->db->from("$db_name.dbo.ORDR")->join("$db_name.dbo.RDR1",
            "$db_name.dbo.RDR1.DocEntry = $db_name.dbo.ORDR.DocEntry and $db_name.dbo.RDR1.LineStatus != 'C' and ($db_name.dbo.RDR1.U_PlannedQty < $db_name.dbo.RDR1.Quantity OR $db_name.dbo.RDR1.U_PlannedQty IS NULL)",
            'inner', false)->join("$db_name.dbo.OSLP", "$db_name.dbo.OSLP.SlpCode = $db_name.dbo.ORDR.SlpCode")
            ->join("$db_name.dbo.[@EXPORTPORT]", "$db_name.dbo.[@EXPORTPORT].Code = $db_name.dbo.ORDR.U_ShipPort",
                'left', false)->where("left($db_name.dbo.RDR1.ItemCode, 1) != 'Z'")
            ->group_by("$db_name.dbo.ORDR.DocEntry");
    }

    /**
     * 取得尚未排入出貨計畫表的 ORDR 資料總筆數 作為分頁用
     * @param string $db_name
     * @param string $state
     * @return mixed
     */
    public function get_master_count($db_name = DEFAULT_DB)
    {
        return $this->db->select("$db_name.dbo.ORDR.DocEntry")->from("$db_name.dbo.ORDR")->join("$db_name.dbo.RDR1",
            "$db_name.dbo.RDR1.DocEntry = $db_name.dbo.ORDR.DocEntry and $db_name.dbo.RDR1.LineStatus != 'C' and ($db_name.dbo.RDR1.U_PlannedQty < $db_name.dbo.RDR1.Quantity OR $db_name.dbo.RDR1.U_PlannedQty IS NULL)",
            'inner', false)->join("$db_name.dbo.OSLP", "$db_name.dbo.OSLP.SlpCode = $db_name.dbo.ORDR.SlpCode")
            ->where("left($db_name.dbo.RDR1.ItemCode, 1) != 'Z'")->group_by("$db_name.dbo.ORDR.DocEntry")->get()
            ->num_rows();
    }

    public function get_total_for_print($id, $db_name)
    {
        $sql_str = sprintf("SELECT
sum(PATTA_PN.dbo.packing_detail.mpcs) as mpcs,
sum(PATTA_PN.dbo.packing_detail.cartons) as cartons,
sum(PATTA_PN.dbo.packing_detail.total_nw) as total_nw,
sum(PATTA_PN.dbo.packing_detail.total_gw) as total_gw
FROM PATTA_PN.dbo.packing_detail
INNER JOIN PATTA_PN.dbo.packing on PATTA_PN.dbo.packing_detail.packing_id = PATTA_PN.dbo.packing.packing_id
INNER JOIN $db_name.dbo.RDR1 on $db_name.dbo.RDR1.DocEntry = PATTA_PN.dbo.packing_detail.rdr1_id and $db_name.dbo.RDR1.LineNum = PATTA_PN.dbo.packing_detail.rdr1_line_num
INNER JOIN $db_name.dbo.ORDR on $db_name.dbo.ORDR.DocEntry = PATTA_PN.dbo.packing_detail.rdr1_id
INNER JOIN $db_name.dbo.OITM on $db_name.dbo.OITM.ItemCode = $db_name.dbo.RDR1.ItemCode
WHERE PATTA_PN.dbo.packing.db = '$db_name' and PATTA_PN.dbo.packing_detail.packing_id = %d", $id);
        return $this->db->query($sql_str)->row();
    }

    public function get_data_for_print($id, $db_name)
    {
        $sql_str = sprintf("SELECT $db_name.dbo.ORDR.U_PINO,$db_name.dbo.ORDR.U_CustomerPO,$db_name.dbo.OITM.U_ForeignName, $db_name.dbo.RDR1.UomCode ,$db_name.dbo.ORDR.Footer,$db_name.dbo.RDR1.U_PcsBox,$db_name.dbo.RDR1.U_BoxCtn,$db_name.dbo.OITM.U_SpecM,$db_name.dbo.OITM.ItemName,$db_name.dbo.OITM.ItemCode,PATTA_PN.dbo.packing_detail.* FROM PATTA_PN.dbo.packing_detail
INNER JOIN PATTA_PN.dbo.packing on PATTA_PN.dbo.packing_detail.packing_id = PATTA_PN.dbo.packing.packing_id
INNER JOIN $db_name.dbo.RDR1 on $db_name.dbo.RDR1.DocEntry = PATTA_PN.dbo.packing_detail.rdr1_id and $db_name.dbo.RDR1.LineNum = PATTA_PN.dbo.packing_detail.rdr1_line_num
INNER JOIN $db_name.dbo.ORDR on $db_name.dbo.ORDR.DocEntry = PATTA_PN.dbo.packing_detail.rdr1_id
INNER JOIN $db_name.dbo.OITM on $db_name.dbo.OITM.ItemCode = $db_name.dbo.RDR1.ItemCode
WHERE PATTA_PN.dbo.packing.db = '$db_name' and PATTA_PN.dbo.packing_detail.packing_id = %s ORDER by $db_name.dbo.ORDR.U_PINO",
            $id);
        return $this->db->query($sql_str)->result();
    }

}