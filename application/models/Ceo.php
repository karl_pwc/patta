<?php

class Ceo extends CI_Model
{

    public function get_ceo_report_detail($packing_id)
    {
        $packing_id = intval($packing_id);
        $db = $this->db->get_where('packing', ['packing_id' => $packing_id])->row()->db;
        $sql_str = "SELECT
  CASE WHEN packing_detail.id IS NULL THEN 0
  ELSE 1 END AS packed,
  CASE WHEN packing.db = 'SAP_KP' THEN N'鋐昇實業股份有限公司'
  WHEN packing.db = 'SAP_PA' THEN N'煒鈞實業股份有限公司'
  WHEN packing.db = 'SAP_JP' THEN N'昆山敬鹏建筑五金有限公司'
  WHEN packing.db = 'SAP_BO' THEN N'保信國際有限公司'
  ELSE N'' END     AS company_name,
  detail.*,
  packing.*,
  packing_detail.*,
  rdr1.*,
  ordr.*,
  oslp.SlpName,
  oitm.ItemName
FROM PATTA_PN.dbo.detail detail INNER JOIN PATTA_PN.dbo.packing packing
    ON packing.packing_id = detail.packing_id
  LEFT JOIN PATTA_PN.dbo.packing_detail packing_detail
    ON detail.sap_id = packing_detail.rdr1_id AND detail.sap_line = packing_detail.rdr1_line_num AND
       packing_detail.packing_id = packing.packing_id
  INNER JOIN $db.dbo.RDR1 rdr1 ON rdr1.DocEntry = detail.sap_id AND rdr1.LineNum = detail.sap_line
  INNER JOIN $db.dbo.OITM oitm ON oitm.ItemCode = rdr1.ItemCode
  INNER JOIN $db.dbo.ORDR ordr ON ordr.DocEntry = rdr1.DocEntry
  INNER JOIN $db.dbo.OSLP oslp ON ordr.SlpCode = OSLP.SlpCode
WHERE packing.packing_id = $packing_id
ORDER BY rdr1.DocEntry, rdr1.LineNum";
        $rows = $this->db->query($sql_str)->result();
        $data = [];
        $data['title'] = reset($rows);
        foreach ($rows as $row) {
            $data['rows'][$row->DocEntry][$row->LineNum][0] = $row;
            $data['detail'][$row->DocEntry][$row->LineNum][] = $row;
        }
        return $data;
    }

    public function get_ceo_report()
    {
        $m = date('Y-m-', mktime(0, 0, 0, date("m") - 9, date("d"), date("Y"))) . '01';
        $this->db->where("date > ", $m);
        $this->db->order_by('date asc');
        $this->db->group_by([
            'packing_id',
            'date',
            'company_name',
            'db',
            'wk',
            'day',
            'CardName',
            'amount',
            'DocCur',
            'state',
            'note',
            'PortName'
        ]);
        $this->db->select('packing_id,company_name,LEFT(date, 7) as date_key,max(U_DocType) as U_DocType,sum(total_ctn) as total_ctn,sum(total_Quantity) as total_Quantity,state,date,db,wk,day,CardName,amount,note,PortName',
            false);
        $result = $this->db->get('ceo')->result();
        $data = [];
        foreach ($result as $row) {
            $data[$row->date_key][$row->db][] = $row;
        }
        return $data;
    }
}