<?php

class CostAnalysis extends CI_Model
{

    CONST STATUS_PROCESSING = 0;
    CONST STATUS_RESPONSE = 1;
    CONST STATUS_CANCEL = 2;

    public static $status = [
        self::STATUS_PROCESSING => '進行中',
        self::STATUS_RESPONSE => '已回覆',
        self::STATUS_CANCEL => '已取消',
    ];
    /**
     * 取得 成本分析單位
     * @param string $db_name
     * @return array
     */
    public function get_plant_list($db_name = DEFAULT_DB)
    {
        return $this->db->select("U_Plant")
                        ->from("$db_name.dbo.QUT1")
                        ->where("U_Plant IS NOT NULL")
                        ->group_by("U_Plant")
                        ->get()
                        ->result_array();
    }
    /**
     * 取得 成本分析單位plant_list
     * @param string $db_name
     * @return array
     */
    public function get_cost_analysis_plant_list($db_name = null)
    {
        $query = $this->db->select("plant")
                        ->from("PATTA_PN.dbo.cost_analysis");
        if($db_name){
            $query->where('db', $db_name);
        }
        return $query->group_by("plant")
                    ->get()
                    ->result_array();
    }

    /**
     * 取得 "新增成本分析單" 所需的報價單資料
     * @param $start
     * @param $length
     * @param $search
     * @param string $db_name
     * @return array
     */
    public function get_list_for_add($start, $length, $search, $db_name = DEFAULT_DB)
    {
        $return = [];

        $db = $this->_get_master_data($db_name, $search)->limit($length, $start);
        $return['data'] = $db->get()->result();

        $db = $this->_get_master_data($db_name, $search);
        $return['count'] = $db->count_all_results();

        return $return;
    }

    /**
     * 取得 "新增成本分析單" 所需的報價單資料 的查詢
     * @param string $db_name
     * @return mixed
     */
    protected function _get_master_data($db_name, $search)
    {
        $query = $this->db->select("$db_name.dbo.OQUT.DocEntry, $db_name.dbo.OQUT.DocNum, $db_name.dbo.OQUT.SlpCode, $db_name.dbo.OSLP.SlpName")
                        ->from("$db_name.dbo.OQUT")
                        ->join("$db_name.dbo.OSLP", "$db_name.dbo.OQUT.SlpCode = $db_name.dbo.OSLP.SlpCode")
                        ->join("$db_name.dbo.QUT1", "$db_name.dbo.OQUT.DocEntry = $db_name.dbo.QUT1.DocEntry")
                        ->where("$db_name.dbo.QUT1.U_Plant = '".$search['U_Plant']."'")
                        ->where("$db_name.dbo.QUT1.U_CstStatus IS NULL")
                        ->distinct();
        if($search['DocEntry_From']){
            $query->where("$db_name.dbo.QUT1.DocEntry >= ".$search['DocEntry_From']);
        }

        if($search['DocEntry_To']){
            $query->where("$db_name.dbo.QUT1.DocEntry <= ".$search['DocEntry_To']);
        }

        return $query;
    }

    /**
     * 取得 報價單 資料總筆數 作為分頁用
     * @param string $db_name
     * @return mixed
     */
    public function get_master_count($db_name, $search)
    {
        return $this->_get_master_data($db_name, $search)->get()->num_rows();
    }

    /**
     * 取得 報價單 的資料 with 細項項目
     * @param string $db_name
     * @param integer $doc_entry
     * @return mixed
     */
    public function get_entry_data($db_name, $doc_entry, $u_plant)
    {
        return $this->db->select("$db_name.dbo.OQUT.DocEntry, $db_name.dbo.OQUT.DocNum, $db_name.dbo.OQUT.SlpCode, $db_name.dbo.OSLP.SlpName, $db_name.dbo.QUT1.LineNum, $db_name.dbo.QUT1.ItemCode, $db_name.dbo.QUT1.Dscription, $db_name.dbo.QUT1.Quantity, $db_name.dbo.QUT1.U_PcsBox, $db_name.dbo.QUT1.U_BoxItem, $db_name.dbo.QUT1.U_BoxCtn, $db_name.dbo.QUT1.U_CntItem, $db_name.dbo.QUT1.U_CtnPLT, $db_name.dbo.QUT1.U_PLTItem, $db_name.dbo.QUT1.U_Plant")
                        ->from("$db_name.dbo.OQUT")
                        ->join("$db_name.dbo.OSLP", "$db_name.dbo.OQUT.SlpCode = $db_name.dbo.OSLP.SlpCode")
                        ->join("$db_name.dbo.QUT1", "$db_name.dbo.OQUT.DocEntry = $db_name.dbo.QUT1.DocEntry")
                        ->where("$db_name.dbo.QUT1.U_Plant = '$u_plant'")
                        ->where("$db_name.dbo.QUT1.U_CstStatus IS NULL")
                        ->where("$db_name.dbo.OQUT.DocEntry", $doc_entry)
                        ->get()->result();
    }

    /**
     * 新增成本分析單後，回寫SAP B1 銷售報價列的狀態為 0-進行中(QUT1.U_CstStatus)及分析單號(QUT1.U_CstNo)
     */
    public function update_sap_status_no($db_name, $cost_analysis_id, $doc_entry, $line_num)
    {
        $this->db->update("$db_name.dbo.QUT1", ['U_CstStatus'=>0, 'U_CstNo'=>$cost_analysis_id], ['DocEntry'=>$doc_entry, 'LineNum'=>$line_num]);
    }

    public function get_list($start, $length, $search)
    {
        $return = [];

        $db = $this->_get_cost_analysis_data($search)->limit($length, $start);
        $return['data'] = $db->get()->result();

        $db = $this->_get_cost_analysis_data($search);
        $return['count'] = $db->count_all_results();

        return $return;
    }

    /**
     * 列出所有成本分析單
     * @param $search
     */
    protected function _get_cost_analysis_data($search)
    {
        $query = $this->db->select("PATTA_PN.dbo.cost_analysis.cost_analysis_id, PATTA_PN.dbo.cost_analysis.db, PATTA_PN.dbo.cost_analysis.doc_entry, PATTA_PN.dbo.cost_analysis.req_name, PATTA_PN.dbo.cost_analysis.date_plan, PATTA_PN.dbo.cost_analysis.status")
                        ->from("PATTA_PN.dbo.cost_analysis")
                        ->where("plant", $search['plant'])
                        ->where("status", $search['status']);

        if(isset($search['db_name']) && $search['db_name']){
            $query->where("db", $search['db_name']);
        }

        if(isset($search['Doc_Entry_From']) && $search['Doc_Entry_From']){
            $query->where("doc_entry >= ".$search['Doc_Entry_From']);
        }

        if(isset($search['Doc_Entry_To']) && $search['Doc_Entry_To']){
            $query->where("doc_entry <= ".$search['Doc_Entry_To']);
        }

        if(isset($search['Cost_Analysis_From']) && $search['Cost_Analysis_From']){
            $query->where("cost_analysis_id >= ".$search['Cost_Analysis_From']);
        }

        if(isset($search['Cost_Analysis_To']) && $search['Cost_Analysis_To']){
            $query->where("cost_analysis_id <= ".$search['Cost_Analysis_To']);
        }

        if((isset($search['Item_Code_From']) && $search['Item_Code_From']) || (isset($search['Item_Code_To']) && $search['Item_Code_To'])){
            $query->join("PATTA_PN.dbo.cost_analysis_detail", "PATTA_PN.dbo.cost_analysis.cost_analysis_id = PATTA_PN.dbo.cost_analysis_detail.cost_analysis_id");
            if(isset($search['Item_Code_From']) && $search['Item_Code_From']){
                $query->where("item_code >= '".$search['Item_Code_From']."'");
            }
            if(isset($search['Item_Code_To']) && $search['Item_Code_To']){
                $query->where("item_code <= '".$search['Item_Code_To']."'");
            }
            $query->distinct();
        }

        return $query;
    }

    /**
     * 取得成本分析單 資料總筆數 作為分頁用
     * @return mixed
     */
    public function get_cost_analysis_count($search)
    {
        return $this->_get_cost_analysis_data($search)->get()->num_rows();
    }

    /**
     * 取得成本分析單 with 明細
     * @param $id
     * @return array
     */
    public function get_cost_analysis($id)
    {
        $mainData = $this->db->from("PATTA_PN.dbo.cost_analysis")
                            ->where("cost_analysis_id", $id)
                            ->get()
                            ->row();
        $mainData->detail = $this->db
                                ->from('PATTA_PN.dbo.cost_analysis_detail')
                                ->where("cost_analysis_id", $id)
                                ->get()
                                ->result();
        return $mainData;
    }

    /**
     * 取得報價計量單位
     * @param $id
     * @return array
     */
    public function get_uom_code($db_name)
    {
        $return = [];
        $result = $this->db->select("$db_name.dbo.OUOM.UOMCode")
                        ->from("$db_name.dbo.OUOM")
                        ->get()
                        ->result();
        foreach ($result as $row) {
            $return[] = $row->UOMCode;
        }
        return $return;
    }

    /**
     * 回覆成本報價
     * 1.並將Web成本分析單的狀態修改為【1-已回覆】
     * 2.系統日期寫至【實際完成日期】
     * 3.回寫各項目至來源【銷售報價】之項次的【內部成本 QUT1.U_InternalCst】、【分析單狀態QUT1.U_CstStatus,1-已回覆】
     */
    public function response($cost_analysis_id)
    {
        $this->db->update("PATTA_PN.dbo.cost_analysis", ['status'=>self::STATUS_RESPONSE,'date_act'=>date('Y-m-d')], ['cost_analysis_id'=>$cost_analysis_id]);
        $cost_analysis = $this->get_cost_analysis($cost_analysis_id);
        foreach ($cost_analysis->detail as $row) {
            $this->db->update("{$cost_analysis->db}.dbo.QUT1", ['U_CstStatus'=>self::STATUS_RESPONSE, 'U_InternalCst'=>$row->total], ['U_CstNo'=>$cost_analysis_id]);
        }
    }

    /**
     * 取得成本分析單 的附件列表
     * @param $id
     * @return array
     */
    public function get_files($cost_analysis_id, $start, $length)
    {
        $return = [];

        $db = $this->_get_cost_analysis_file($cost_analysis_id)->limit($length, $start);
        $return['data'] = $db->get()->result();

        $db = $this->_get_cost_analysis_file($cost_analysis_id);
        $return['count'] = $db->count_all_results();

        return $return;
    }

    /**
     * 列出所有成本分析單的附件
     * @param $cost_analysis_id
     */
    protected function _get_cost_analysis_file($cost_analysis_id)
    {
        return $this->db->from("PATTA_PN.dbo.cost_analysis_file")
                        ->where("cost_analysis_id", $cost_analysis_id);
    }

    /**
     * 取得成本分析單的附件 作為分頁用
     * @return $cost_analysis_id
     */
    public function get_cost_analysis_file_count($cost_analysis_id)
    {
        return $this->_get_cost_analysis_file($cost_analysis_id)->get()->num_rows();
    }

    /**
     * 取得成本分析單 with 明細
     * @param $id
     */
    public function get_cost_analysis_file_data($id)
    {
        return $this->db->from("PATTA_PN.dbo.cost_analysis_file")
                        ->where("id", $id)
                        ->get()
                        ->row();
    }
}