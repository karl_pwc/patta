<?php

class Patta extends CI_Model
{
    public function getCompanyName($db)
    {
        return $this->db->select("$db.dbo.OADM.PrintHdrF")->get("$db.dbo.OADM")->row()->PrintHdrF;
    }

    public function getCompany($db)
    {
        return $this->db->get("$db.dbo.OADM")->row();
    }

    public function getMail($db)
    {
        return $this->db->get("$db.dbo.OADM")->row();
    }
}