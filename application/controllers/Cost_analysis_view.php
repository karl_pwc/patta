<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cost_analysis_view extends MY_Controller
{
    protected $_main_menu = '成本分析單';

    protected $_sub_menu = '查詢';

    public function index($db_name = null)
    {
//        $this->output->enable_profiler(TRUE);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_COST_VIEW);
        $db_name = get_db_name($db_name);
        init_db_list($db_name, 'cost_analysis_view');
        $plant_list = $this->costAnalysis->get_cost_analysis_plant_list($db_name);
        $this->load->view('Cost_analysis_view/list',['plant_list'=>$plant_list]);
    }

    /**
     * 取得查詢的成本分析單
     * @param null $db_name
     */
    public function table()
    {
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $search = [
            'db_name' => trim($this->input->get('db_select', true)),
            'plant' => trim($this->input->get('plant', true)),
            'status' => intval($this->input->get('status', true)),
            'Doc_Entry_From' => intval($this->input->get('Doc_Entry_From', true)),
            'Doc_Entry_To' => intval($this->input->get('Doc_Entry_To', true)),
            'Cost_Analysis_From' => intval($this->input->get('Cost_Analysis_From', true)),
            'Cost_Analysis_To' => intval($this->input->get('Cost_Analysis_To', true)),
            'Item_Code_From' => trim($this->input->get('Item_Code_From', true)),
            'Item_Code_To' => trim($this->input->get('Item_Code_To', true))
        ];

        $list = $this->costAnalysis->get_list($start, $length, $search);
        $data = [];
        foreach ($list['data'] as $row) {
            $data[] = [
                sprintf('<a href="/cost_analysis_view/view/%d" class="btn default btn-xs purple"><i class="fa fa-edit"></i> 查看 </a>', $row->cost_analysis_id),
                $row->cost_analysis_id,
                $row->doc_entry,
                get_db_caption($row->db),
                $row->req_name,
                $row->date_plan,
                CostAnalysis::$status[$row->status]
            ];
        }

        $return = [
            'draw' => $draw,
            'recordsTotal' => $this->costAnalysis->get_cost_analysis_count($search),
            'recordsFiltered' => $list['count'],
            'data' => $data,
            'error' => null
        ];
        echo json_encode($return);
    }

    /**
     * 查詢成本分析單
     * @param null $cost_analysis_id
     */
    public function view($cost_analysis_id)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_COST_MAINTENANCE);
        get_db_name(null);
        $data = $this->costAnalysis->get_cost_analysis($cost_analysis_id);
        $uom_code = $this->costAnalysis->get_uom_code($data->db);
        $this->load->view('Cost_analysis_view/view',['data'=>$data,'uom_code'=>$uom_code]);
    }

    /**
     *　附件列表
     */
    public function file_table($cost_analysis_id)
    {
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $list = $this->costAnalysis->get_files($cost_analysis_id, $start, $length);
        $data = [];
        foreach ($list['data'] as $row) {
            $data[] = [
                sprintf('<a href="/cost_analysis_maintenance/download/%d">'.$row->filename.'</a>', $row->id)
            ];
        }

        $return = [
            'draw' => $draw,
            'recordsTotal' => $this->costAnalysis->get_cost_analysis_file_count($cost_analysis_id),
            'recordsFiltered' => $list['count'],
            'data' => $data,
            'error' => null
        ];
        echo json_encode($return);
    }
}