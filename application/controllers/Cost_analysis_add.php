<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cost_analysis_add extends MY_Controller
{
    protected $_main_menu = '成本分析單';

    protected $_sub_menu = '新增';

    public function index($db_name = null)
    {
//        $this->output->enable_profiler(TRUE);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_COST_ADD);
        $db_name = get_db_name($db_name);
        init_db_list($db_name, 'cost_analysis_add');
        $plant_list = $this->costAnalysis->get_plant_list($db_name);
        $this->load->view('Cost_analysis_add/list',['plant_list'=>$plant_list]);
    }

    /**
     * 取得查詢的報價單
     */
    public function table()
    {
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));
        $db_name = trim($this->input->get('db_select', true));

        $search = [
            'U_Plant' => trim($this->input->get('u_plant_select', true)),
            'DocEntry_From' => intval($this->input->get('DocEntry_From', true)),
            'DocEntry_To' => intval($this->input->get('DocEntry_To', true))
        ];

        $quotation = $this->costAnalysis->get_list_for_add($start, $length, $search, $db_name);
        $data = [];
        foreach ($quotation['data'] as $row) {
            $data[] = [
                sprintf('<input type="checkbox" data-id="%d" value="1" check-type="select">', $row->DocEntry),
                $row->DocNum . sprintf('<input type="hidden" name="id" value="%d" />', $row->DocNum),
                $row->SlpName,
                sprintf('<div class="input-group input-medium date date-picker" data-id="%d" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                    <input type="text" class="form-control date-input" readonly="" data-id="%d">
                    <span class="input-group-btn">
                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                    </span>
                </div>', $row->DocEntry, $row->DocEntry)
            ];
        }

        $return = [
            'draw' => $draw,
            'recordsTotal' => $this->costAnalysis->get_master_count($db_name, $search),
            'recordsFiltered' => $quotation['count'],
            'data' => $data,
            'error' => null
        ];
        echo json_encode($return);
    }

    public function post($db_name, $u_plant)
    {
        $this->form_validation->set_rules('DocEntry[]', '報價單單號', 'required');
        $this->form_validation->set_rules('DatePlan[]', '預計完成日', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header('422');
            echo validation_errors();
            exit;
        }

        try {
            $this->db->trans_begin();

            $doc_entries = $this->input->post('DocEntry[]');
            $date_plan= $this->input->post('DatePlan[]');
            foreach ($doc_entries as $key => $doc_entry) {
                $entryData = $this->costAnalysis->get_entry_data($db_name, $doc_entry, $u_plant);
                $this->db->insert('cost_analysis', [
                    'db' => $db_name,
                    'requester' => $entryData[0]->SlpCode,
                    'req_name' => $entryData[0]->SlpName,
                    'status' => 0,
                    'date_plan' => $date_plan[$key],
                    'doc_entry' => $doc_entry,
                    'plant' => $u_plant
                ]);
                $insert_id = $this->db->insert_id();
                foreach ($entryData as $entryBody) {
                    $this->db->insert('cost_analysis_detail', [
                        'cost_analysis_id' => $insert_id,
                        'base_entry' => $entryBody->DocEntry,
                        'base_line' => $entryBody->LineNum,
                        'item_code' => $entryBody->ItemCode,
                        'item_name' => $entryBody->Dscription,
                        'quantity' => $entryBody->Quantity,
                        'pcs_box' => $entryBody->U_PcsBox,
                        'box_item' => $entryBody->U_BoxItem,
                        'box_ctn' => $entryBody->U_BoxCtn,
                        'ctn_item' => $entryBody->U_CntItem,
                        'ctn_plt' => $entryBody->U_CtnPLT,
                        'plt_item' => $entryBody->U_PLTItem
                    ]);

                    $this->costAnalysis->update_sap_status_no($db_name, $insert_id, $entryBody->DocEntry, $entryBody->LineNum);
                }
            }

            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "新增成功");

        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }
}