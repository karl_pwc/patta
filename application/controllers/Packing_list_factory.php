<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_factory extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = '廠務確認';

    public function index($db_name = null)
    {
//        $this->output->enable_profiler(TRUE);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_FACTORY);
        init_packing_db_list($db_name, 'packing_list_factory');
        $rows = $this->packing->get_list_for_factory(get_packing_db_name($db_name));
        $this->load->view('packing_list_factory/list', ['rows' => $rows]);
    }

    public function note($db_name)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_FACTORY);
        $this->form_validation->set_rules('packing_id[]', '確認', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header('422');
            echo validation_errors();
            exit;
        }

        $packing_ids = $this->input->post('packing_id[]');
        $notes = $this->input->post('note[]');
        foreach ($packing_ids as $key => $packing_id) {
            $this->db->where('packing_id', $packing_id);
            $this->db->update('packing', [
                'note' => $notes[$key]
            ]);
        }
        $this->session->set_flashdata('success_message', "操作成功");
    }

    public function edit($db_name, $id)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_FACTORY);
        $transform_table = $this->packing->get_list_for_view(get_packing_db_name($db_name), $id);
        $rows = $this->packing->get_edit_for_factory($id, get_packing_db_name($db_name));
        $detail = $this->packing->get_detail($id);
        if (empty($rows)) {
            redirect('/packing_list_factory/index/' . $db_name, 'location');
            return;
        }
        $this->load->view('packing_list_factory/edit', [
            'rows' => $rows,
            'id' => $id,
            'db_name' => get_packing_db_name($db_name),
            'detail_json' => json_encode($detail),
            'transform_table' => $transform_table
        ]);
    }

    public function edit_test()
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_FACTORY);
        $transform_table = $this->packing->get_list_for_view(get_packing_db_name($db_name));
        $rows = $this->packing->get_edit_for_factory($id, get_packing_db_name($db_name));
        $detail = $this->packing->get_detail($id);
        if (empty($rows)) {
            redirect('/packing_list_factory/index/' . $db_name, 'location');
            return;
        }
        $this->load->view('packing_list_factory/edit_test', [
            'rows' => $rows,
            'id' => $id,
            'db_name' => get_packing_db_name($db_name),
            'detail_json' => json_encode($detail),
            'transform_table' => $transform_table
        ]);
    }

    public function post($db_name, $packing_id, $pass)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_FACTORY);
        try {
            $db_name = get_packing_db_name($db_name);
            $this->db->trans_begin();
            $doc_num = $this->input->post('doc_num', TRUE);
            $line = $this->input->post('line', TRUE);
            $id = $this->input->post('id', TRUE);
            $batch_no = $this->input->post('batch_no', TRUE);
            $code = $this->input->post('code', TRUE);
            $pallet_start = $this->input->post('pallet_start', TRUE);
            $pallet_end = $this->input->post('pallet_end', TRUE);
            $pallet_amount = $this->input->post('pallet_amount', TRUE);
            $mpcs = $this->input->post('mpcs', TRUE);
            $cartons = $this->input->post('cartons', TRUE);
            $boxes = $this->input->post('boxes', TRUE);
            $nw = $this->input->post('nw', TRUE);
            $gw = $this->input->post('gw', TRUE);
            $total_nw = $this->input->post('total_nw', TRUE);
            $total_gw = $this->input->post('total_gw', TRUE);
            $container_number = $this->input->post('container_number', TRUE);
            $item_code = $this->input->post('item_code', TRUE);
            // $whs_code = $this->input->post('whs_code', TRUE);
            $delete = $this->input->post('delete', TRUE);

            if (empty($doc_num)) {
                $doc_num = array();
            }

            $error = false;
            $request = array();
            // 計算數量
            foreach ($doc_num as $key => $value) {
                $_mpcs = $mpcs[$key];
                $_item_code = $item_code[$key];
                @$request[$_item_code] += $_mpcs;
            }

            /*
             * 庫存判斷
             */
            $out_of_stock_error = array();
            foreach ($request as $_item_code => $amount) {
                // 庫存計算
//                if ($this->packing->out_of_stock($_item_code, $amount, $db_name)) {
//                    $on_hand = $this->packing->get_on_hand($_item_code, $db_name);
//                    $out_of_stock_error[] =
//                        sprintf('<p>%s 庫存不足, 需要 %.2f 單位. 剩餘 %.2f 單位</p>', $_item_code, $amount, $on_hand);
//                    $error = true;
//                }
            }
            $error_stack = array();
            foreach ($doc_num as $key => $value) {
                $_doc_num = $value;
                $_line = intval($line[$key]);
                $_id = intval($id[$key]);
                $_batch_no = $batch_no[$key];
                $_code = $code[$key];
                $_pallet_start = trim($pallet_start[$key]);
                $_pallet_end = trim($pallet_end[$key]);
                $_pallet_amount = floatval($pallet_amount[$key]);
                $_mpcs = floatval($mpcs[$key]);
                $_cartons = floatval($cartons[$key]);
                $_boxes = floatval($boxes[$key]);
                $_nw = floatval($nw[$key]);
                $_gw = floatval($gw[$key]);
                $_total_nw = floatval($total_nw[$key]);
                $_total_gw = floatval($total_gw[$key]);
                $_container_number = $container_number[$key];
                $_delete = intval($delete[$key]);

                if ($_id && $_delete) {
                    $this->db->delete("PATTA_PN.dbo.packing_detail", array('id' => $_id));
                    continue;
                }
//                if (empty($_batch_no)) {
//                    $error_stack[] = sprintf('<p>批號 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
//                    $error = true;
//                }
                if (empty($_code)) {
                    $error_stack[] = sprintf('<p>代號 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_pallet_start)) {
                    $error_stack[] = sprintf('<p>棧板起號 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_pallet_end)) {
                    $error_stack[] = sprintf('<p>棧板迄號 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_pallet_amount)) {
                    $error_stack[] = sprintf('<p>棧板數 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_mpcs)) {
                    $error_stack[] = sprintf('<p>MPCS 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_cartons)) {
                    $error_stack[] = sprintf('<p>箱數 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_boxes)) {
                    $error_stack[] = sprintf('<p>盒數 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_nw)) {
                    $error_stack[] = sprintf('<p>箱淨重 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_gw)) {
                    $error_stack[] = sprintf('<p>箱毛重 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_total_nw)) {
                    $error_stack[] = sprintf('<p>總箱淨重 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
                if (empty($_total_gw)) {
                    $error_stack[] = sprintf('<p>總箱毛重 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
                    $error = true;
                }
//                if (empty($_container_number)) {
//                    $error_stack[] = sprintf('<p>櫃號 填寫不正確 (訂單編號:%d,LineNum:%d)</p>', $_doc_num, $_line);
//                    $error = true;
//                }
                if (!$_id) {
                    $this->db->insert('PATTA_PN.dbo.packing_detail', [
                        'packing_id' => $packing_id,
                        'rdr1_id' => $_doc_num,
                        'rdr1_line_num' => $_line,
                        'batch_no' => $_batch_no,
                        'code' => $_code,
                        'pallet_start' => $_pallet_start,
                        'pallet_end' => $_pallet_end,
                        'pallet_amount' => $_pallet_amount,
                        'mpcs' => $_mpcs,
                        'cartons' => $_cartons,
                        'boxes' => $_boxes,
                        'nw' => $_nw,
                        'gw' => $_gw,
                        'total_nw' => $_total_nw,
                        'total_gw' => $_total_gw,
                        'container_number' => $_container_number
                    ]);
                } else {
                    $this->db->where('id', $_id);
                    $this->db->update('PATTA_PN.dbo.packing_detail', [
                        'rdr1_id' => $_doc_num,
                        'rdr1_line_num' => $_line,
                        'batch_no' => $_batch_no,
                        'code' => $_code,
                        'pallet_start' => $_pallet_start,
                        'pallet_end' => $_pallet_end,
                        'pallet_amount' => $_pallet_amount,
                        'mpcs' => $_mpcs,
                        'cartons' => $_cartons,
                        'boxes' => $_boxes,
                        'nw' => $_nw,
                        'gw' => $_gw,
                        'total_nw' => $_total_nw,
                        'total_gw' => $_total_gw,
                        'container_number' => $_container_number
                    ]);
                }
            }

            if ($error) {
                $this->output->set_status_header('403');
                foreach ($out_of_stock_error as $error_str) echo $error_str;
                foreach (array_unique($error_stack) as $error_str) echo $error_str;
                $this->db->trans_rollback();
                return;
            }
            if ($pass) {
                $this->packing->update_state($packing_id, 'B', 'C');
            }
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('403');
            echo $e->getMessage();
            $this->db->trans_rollback();
        }
    }
}