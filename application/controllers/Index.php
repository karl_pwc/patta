<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends MY_Controller {

    protected $_main_menu = '首頁';

    protected $_sub_menu = '';

    public function __construct(){
        parent::__construct();
    }

    public function index($db_name = null){
        Sakilu_Auth::redirectIfNotLogin('/login');
        get_db_name($db_name);
        $this->load->view('index');
    }

}
