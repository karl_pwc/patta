<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cost_analysis_maintenance extends MY_Controller
{
    protected $_main_menu = '成本分析單';

    protected $_sub_menu = '維護';

    public function index()
    {
//        $this->output->enable_profiler(TRUE);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_COST_MAINTENANCE);
        get_db_name(null);
        $plant_list = $this->costAnalysis->get_cost_analysis_plant_list();
        $this->load->view('Cost_analysis_maintenance/list',['plant_list'=>$plant_list]);
    }

    /**
     * 取得查詢的成本分析單
     */
    public function table()
    {
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $search = [
            'plant' => trim($this->input->get('plant', true)),
            'status' => intval($this->input->get('status', true)),
            'Cost_Analysis_From' => intval($this->input->get('Cost_Analysis_From', true)),
            'Cost_Analysis_To' => intval($this->input->get('Cost_Analysis_To', true))
        ];

        $list = $this->costAnalysis->get_list($start, $length, $search);
        $data = [];
        foreach ($list['data'] as $row) {
            $data[] = [
                sprintf('<a href="/cost_analysis_maintenance/edit/%d" class="btn default btn-xs purple"><i class="fa fa-edit"></i> 維護 </a>', $row->cost_analysis_id),
                $row->cost_analysis_id,
                $row->doc_entry,
                get_db_caption($row->db),
                $row->req_name,
                $row->date_plan,
                CostAnalysis::$status[$row->status]
            ];
        }

        $return = [
            'draw' => $draw,
            'recordsTotal' => $this->costAnalysis->get_cost_analysis_count($search),
            'recordsFiltered' => $list['count'],
            'data' => $data,
            'error' => null
        ];
        echo json_encode($return);
    }

    /**
     * 維護成本分析單
     */
    public function edit($cost_analysis_id)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_COST_MAINTENANCE);
        get_db_name(null);
        $data = $this->costAnalysis->get_cost_analysis($cost_analysis_id);
        $uom_code = $this->costAnalysis->get_uom_code($data->db);
        $this->load->view('Cost_analysis_maintenance/edit',['data'=>$data,'uom_code'=>$uom_code]);
    }

    public function post()
    {
        try {
            $this->db->trans_begin();

            $data = [
                'date_act' => $this->input->post('date_act') ? $this->input->post('date_act'):null,
                'memo' => $this->input->post('memo')
            ];
            $this->db->where('cost_analysis_id', $this->input->post('cost_analysis_id'));
            $this->db->update('cost_analysis', $data);

            $uom = $this->input->post('uom');
            $wire_weight = $this->input->post('wire_weight');
            $wire_up = $this->input->post('wire_up');
            $pkg_cost = $this->input->post('pkg_cost');
            for ($i=1; $i<=5 ;$i++) {
                $item{str_pad($i, 2, "0", STR_PAD_LEFT)} = $this->input->post('item'.str_pad($i, 2, "0", STR_PAD_LEFT));
            }
            for ($i=1; $i<=5 ;$i++) {
                $process{str_pad($i, 2, "0", STR_PAD_LEFT)} = $this->input->post('process'.str_pad($i, 2, "0", STR_PAD_LEFT));
            }
            $sub_exp = $this->input->post('sub_exp');
            $sub_mng = $this->input->post('sub_mng');
            $text = $this->input->post('text');

            foreach ($this->input->post('detail_id') as $id) {
                $data = [
                    'uom'           => $uom[$id],
                    'wire_weight'   => (float)$wire_weight[$id],
                    'wire_up'       => (float)$wire_up[$id],
                    'wire_cst'      => (float)$wire_weight[$id] * $wire_up[$id],
                    'pkg_cost'      => (float)$pkg_cost[$id],
                    'sub_exp'       => (float)$sub_exp[$id],
                    'sub_mng'       => (float)$sub_mng[$id],
                    'text'          => $text[$id],
                ];
                $data['sub_mcst'] = $data['wire_cst'] + $data['pkg_cost'];
                for ($i=1; $i<=5 ;$i++) {
                    $no = str_pad($i, 2, "0", STR_PAD_LEFT);
                    $data['item'.$no] = (float)$item{$no}[$id];
                    $data['sub_mcst'] += $data['item'.$no];
                }
                $data['sub_lcst'] = null;
                for ($i=1; $i<=5 ;$i++) {
                    $no = str_pad($i, 2, "0", STR_PAD_LEFT);
                    $data['process'.$no] = (float)$process{$no}[$id];
                    $data['sub_lcst'] += $data['process'.$no];
                }
                $data['total'] = $data['sub_mcst'] + $data['sub_lcst'] + $data['sub_exp'] + $data['sub_mng'];
                $this->db->where('id', $id);
                $this->db->update('cost_analysis_detail', $data);
            }

            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "更新成功");

        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }

    public function response($cost_analysis_id)
    {
        try {
            $this->db->trans_begin();

            $this->costAnalysis->response($cost_analysis_id);

            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "回覆成功");

        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }

    /**
     *　附件列表
     */
    public function file_table($cost_analysis_id)
    {
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $list = $this->costAnalysis->get_files($cost_analysis_id, $start, $length);
        $data = [];
        foreach ($list['data'] as $row) {
            $data[] = [
                sprintf('<a onclick="deleteFile(%d)" class="btn default btn-xs red"><i class="fa fa fa-trash-o"></i> 刪除 </a>', $row->id),
                sprintf('<a href="/cost_analysis_maintenance/download/%d">'.$row->filename.'</a>', $row->id)
            ];
        }

        $return = [
            'draw' => $draw,
            'recordsTotal' => $this->costAnalysis->get_cost_analysis_file_count($cost_analysis_id),
            'recordsFiltered' => $list['count'],
            'data' => $data,
            'error' => null
        ];
        echo json_encode($return);
    }

    public function upload($cost_analysis_id)
    {
        try {
            $this->db->trans_begin();
            $filePath = "uploads/cost_analysis/$cost_analysis_id";
            if (!is_dir($filePath)) {
                mkdir($filePath, 0777, TRUE);
            }

            $config = [
                'upload_path'=>$filePath,
                'allowed_types'=>'*',
                'file_name'=> time().'.'.pathinfo($_FILES['attachment']['name'], PATHINFO_EXTENSION)
            ];
            $this->load->library('upload', $config);
            if(!$this->upload->do_upload('attachment')){
                throw new Exception($this->upload->display_errors());
            }
            $uploadData = $this->upload->data();
            $this->db->insert('cost_analysis_file', [
                'cost_analysis_id' => $cost_analysis_id,
                'path' => $filePath.'/'.$uploadData['file_name'],
                'filename' => $_FILES['attachment']['name']
            ]);

            $this->db->trans_commit();

        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }

    public function file_delete($id)
    {
        try {
            $this->db->trans_begin();
            $data = $this->costAnalysis->get_cost_analysis_file_data($id);
            $this->load->helper("file");
            unlink(realpath($data->path));
            $this->db->delete('cost_analysis_file', ['id' => $id]);
            $this->db->trans_commit();

        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }

    public function download($id)
    {
        $this->load->helper('download');
        $data = $this->costAnalysis->get_cost_analysis_file_data($id);
        $file = file_get_contents(realpath($data->path)); // Read the file's contents
        force_download($data->filename, $file);
    }
}