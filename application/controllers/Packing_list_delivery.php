<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_delivery extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = '出貨確認';

    public function index($db_name = null)
    {
//        $this->output->enable_profiler(TRUE);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_DELIVERY);
        init_packing_db_list($db_name, 'Packing_list_delivery');
        $rows = $this->packing->get_list_for_delivery(get_packing_db_name($db_name));
        $this->load->view('packing_list_delivery/list', ['rows' => $rows]);
    }

    public function post($db_name)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_DELIVERY);
        try {
            $this->db->trans_begin();

            $packing_id = $this->input->post('packing_id');
            $sap_packing_id = $this->input->post('sap_packing_id');
            $note = $this->input->post('note');

            $error = true;
            foreach ($packing_id as $key => $id) {
                $str = trim($sap_packing_id[$key]);
                if ($str) {
                    $error = false;
                    $this->db->where('packing_id', $id);
                    $this->db->update('packing', [
                        'sap_packing_id' => $str,
                        'note' => $note[$key]
                    ]);
                    $this->packing->update_state($id, 'D', 'E');
                }
                $this->db->where('packing_id', $id);
                $this->db->update('packing', [
                    'note' => $note[$key]
                ]);
                $error = false;
            }

            if ($error) {
                $this->output->set_status_header('403');
                echo '請填寫出貨單號';
                return;
            }
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }
}