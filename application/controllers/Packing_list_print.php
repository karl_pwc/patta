<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_print extends MY_Controller
{
    protected $_main_menu = '首頁';

    protected $_sub_menu = '';

    public function index($packing_id)
    {
        $this->db->where('packing_id', $packing_id);
        $row = $this->db->get('packing')->row();
        $this->load->view('/packing_list_print/list', [
            'row' => $row
        ]);
    }

    public function save($id)
    {
        $this->form_validation->set_rules('print_no', 'NO:', 'trim|required');
        $this->form_validation->set_rules('print_ms', 'per M/S.S:', 'trim|required');
        $this->form_validation->set_rules('print_from', 'From:', 'trim|required');
        $this->form_validation->set_rules('print_to', 'To:', 'trim|required');
        $this->form_validation->set_rules('print_date', 'date:', 'trim|required');
        $this->form_validation->set_rules('print_by', 'Sailing on or about', 'trim|required');
        $this->form_validation->set_rules('print_weight', 'Shipped by:', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header('422');
            echo validation_errors();
            exit;
        }
        $this->db->where('packing_id', $id);
        $this->db->update('packing', [
            'print_no' => $this->input->post('print_no', true),
            'print_ms' => $this->input->post('print_ms', true),
            'print_from' => $this->input->post('print_from', true),
            'print_to' => $this->input->post('print_to', true),
            'print_date' => $this->input->post('print_date', true),
            'print_by' => $this->input->post('print_by', true),
            'print_weight' => $this->input->post('print_weight', true),
        ]);
        $this->session->set_flashdata('success_message', "操作成功");
    }

    public function go($id)
    {
        error_reporting(0);
        $data = [
            'print_no' => $this->input->post('print_no'),
            'print_ms' => $this->input->post('print_ms'),
            'print_from' => $this->input->post('print_from'),
            'print_to' => $this->input->post('print_to'),
            'print_date' => $this->input->post('print_date'),
            'print_by' => $this->input->post('print_by'),
            'print_weight' => $this->input->post('print_weight'),
        ];
        $db = $this->packing->get_db_name($id);
        $data['company'] = $this->patta->getCompany($db);
        $data['rows'] = $this->packing->get_data_for_print($id, $db);
        $data['total'] = $this->packing->get_total_for_print($id, $db);

        $this->load->view('/packing_list_print/print', $data);
    }

    public function test($id){
        $data = [
            'print_no' => $this->input->post('print_no'),
            'print_ms' => $this->input->post('print_ms'),
            'print_from' => $this->input->post('print_from')
        ];
        $db = $this->packing->get_db_name($id);
        $data['rows'] = $this->packing->get_data_for_print($id, $db);
        var_dump($data['rows']);
        exit;
        $data['detail_rows'] = $this->packing->get_detail($id);
        $this->load->view('/packing_list_print/test', $data);

    }
}