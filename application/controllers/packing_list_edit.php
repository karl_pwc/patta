<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_edit extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = '編輯';

    public function index($db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_EDIT);
        init_packing_db_list($db_name, 'packing_list_edit');
        $db_name = get_packing_db_name($db_name);
        $rows = $this->packing->get_list_for_view($db_name);
        $this->load->view('Packing_list_edit/list', [
            'rows' => $rows,
            'db_name' => $db_name
        ]);
    }

    public function post($db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_EDIT);

        init_packing_db_list($db_name, 'packing_list_edit');
        $db_name = get_packing_db_name($db_name);

        try {
            $this->db->trans_begin();

            $packing_id = $this->input->post('packing_id');
            $date = $this->input->post('date');
            $amount = $this->input->post('amount');
            $state = $this->input->post('state');

            $this->form_validation->set_rules('packing_id', '計畫號碼', 'trim|required');
            $this->form_validation->set_rules('date', '出貨排程日', 'trim|required');
            $this->form_validation->set_rules('amount', '總櫃數:', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->output->set_status_header('422');
                echo validation_errors();
                $this->db->trans_rollback();
                exit;
            }
            $this->db->where('packing_id', $packing_id);
            $this->db->update('packing', [
                'date' => $date,
                'amount' => $amount
            ]);
            if ($state != '' && ($state == 'A' || $state == 'B' || $state == 'C')) {
                $this->packing->update_state($packing_id, '', $state);
            }
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }
}