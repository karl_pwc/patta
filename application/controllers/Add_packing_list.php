<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Add_packing_list extends MY_Controller
{

    public function index($db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin("login");
        Sakilu_Auth::denyAccessIfNotRole("新增出貨計畫表");
        init_menu();
        init_select('Add_packing_list', $db_name);

        $this->load->vars(array(
            'menu_select' => '出貨計畫表',
            'sub_menu_select' => '新增出貨計畫表'
        ));
        $db = Zend_Db::factory('Sqlsrv', get_ms_db_config($db_name));
        $select = new Zend_Db_Select($db);
        $rows = $select->from('ORDR', [
            'DocNum',
            'U_PINo',
            'CardCode',
            'CardName',
            'SlpCode',
            'DocCur',
            'DocDueDate',
            'U_DocType',
            'U_ShipPort'
        ])->joinLeft('OSLP', 'OSLP.SlpCode = ORDR.SlpCode', array('SlpName'))
            ->where('ORDR.U_Planned = ?', 'N')->order('DocNum DESC')->query()->fetchAll();
        $this->load->vars('rows', $rows);
        $this->load->view('common/header');
        $this->load->view('Add_packing_list/list');
        $this->load->view('Add_packing_list/footer');
        //$this->output->enable_profiler(TRUE);
    }

    public function post($db_name = null)
    {
        $user = Sakilu_Auth::getUser();
        if (empty($user)) {
            $this->output->set_status_header('401');
            echo "請登入";
            return;
        }
        Sakilu_Auth::denyAccessIfNotRole("新增出貨計畫表");

        if (!$this->input->is_ajax_request()) {
            die();
        }
        $this->load->library('form_validation');
        foreach ($_POST as $k => $v) {
            if (strpos($k, 'id') !== false) {
                $id = intval(substr($k, 3));
                $this->form_validation->set_rules('date_' . $id, "出貨排程日期(訂單編號 $id)", 'required');
                $this->form_validation->set_rules('number_' . $id, "總櫃數(訂單編號 $id)", 'required|numeric|greater_than[0]');
            }
        }
        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header('403');
            echo validation_errors();
            return;
        }
        $user = Sakilu_Auth::getUser();
        $config = get_ms_db_config($db_name);
        $db = Zend_Db::factory('Sqlsrv', $config);
        $db->beginTransaction();
        $this->db->trans_start();
        try {
            $this->db->insert('packing_list', [
                'state' => 'A',
                'admin_id' => $user->admin_id,
                'db_name' => $config['dbname']
            ], true);
            $insert_id = $this->db->insert_id();
            foreach ($_POST as $k => $v) {
                $select = new Zend_Db_Select($db);
                if (strpos($k, 'id') !== false) {
                    $id = intval(substr($k, 3));
                    $date = $this->input->post('date_' . $id);
                    $number = $this->input->post('number_' . $id);
                    $ordr_data = $select->from('ORDR')->where('DocNum = ?', $id)->query()->fetch();
                    $this->db->insert('odrd', [
                        'odrd_id' => $id,
                        'packing_list_id' => $insert_id,
                        'pi' => $ordr_data['U_PINo'],
                        'card_id' => $ordr_data['CardCode'],
                        'card_name' => $ordr_data['CardName'],
                        'SlpCode' => $ordr_data['SlpCode'],
                        'dispatch_date' => $date,
                        'number' => $number,
                        'state' => 'A'
                    ], true);
                    Zend_Db_Table::setDefaultAdapter($db);
                    $table = new Zend_Db_Table('ORDR');
                    $table->update(array('U_Planned' => 'Y'), sprintf('DocNum = %d', $id));
                }
            }
            $db->commit();
            $this->db->trans_complete();
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
        }
    }

}

