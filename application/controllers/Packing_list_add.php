<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_add extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = '新增';

    public function index($db_name = null)
    {
//        $this->output->enable_profiler(TRUE);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_ADD);
        get_packing_db_name($db_name);
        init_packing_db_list($db_name, 'packing_list_add');
        $this->load->view('Packing_list_add/list');
    }

    public function table($db_name = null)
    {
//        $this->output->enable_profiler(TRUE);
        $draw = intval($this->input->get('draw'));
        $start = intval($this->input->get('start'));
        $length = intval($this->input->get('length'));

        $search = [
            'DocEntry' => $this->input->get('columns[1][search][value]', true),
            'U_PINO' => trim($this->input->get('columns[2][search][value]', true)),
            'CardName' => trim($this->input->get('columns[3][search][value]', true)),
            'SlpName' => trim($this->input->get('columns[4][search][value]', true)),
            'DocCur' => trim($this->input->get('columns[5][search][value]', true)),
            'DocDueDate' => trim($this->input->get('columns[6][search][value]', true)),
            'DocType' => trim($this->input->get('columns[7][search][value]', true)),
            'U_Ship' => trim($this->input->get('columns[8][search][value]', true))
        ];
        $packing = $this->packing->get_list_for_add($start, $length, $search, $db_name);
        $data = [];
        foreach ($packing['data'] as $row) {
            $data[] = [
                sprintf('<button type="button" onclick="add(this)" data-id="%d" class="btn btn-primary">新增</button>',
                    $row->DocEntry),
                $row->DocEntry . sprintf('<input type="hidden" name="id" value="%d" />', $row->DocEntry),
                $row->U_PINO,
                $row->CardName,
                $row->SlpName,
                $row->DocCur,
                substr($row->DocDueDate, 0, 10),
                $row->U_DocType,
                $this->packing->get_ship_name($row->U_ShipPort, $db_name)
            ];
        }

        $return = [
            'draw' => $draw,
            'recordsTotal' => $this->packing->get_master_count($db_name),
            'recordsFiltered' => $packing['count'],
            'data' => $data,
            'error' => null
        ];
        echo json_encode($return);
    }

    public function line_data($id, $db_name = null)
    {
        echo json_encode($this->packing->get_line_data_by_id($id, $db_name)->get()->result());
    }

    public function post($db_name)
    {
        $this->form_validation->set_rules('date', '出貨排程日', 'trim|required');
        $this->form_validation->set_rules('amount', '總櫃數', 'trim|required');
        $this->form_validation->set_rules('DocEntry[]', '出貨細項', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header('422');
            echo validation_errors();
            exit;
        }

        try {
            $this->db->trans_begin();
            $this->db->insert('packing', [
                'date' => $this->input->post('date'),
                'amount' => floatval($this->input->post('amount')),
                'db' => $db_name,
                'group_name' => $this->input->post('group_name')
            ]);

            $insert_id = $this->db->insert_id();
            $error = true;
            $doc_entry = $this->input->post('DocEntry[]');
            $line_num = $this->input->post('LineNum[]');
            foreach ($doc_entry as $key => $id) {
                $line = intval($line_num[$key]);
                if ($this->input->post($id . '_' . $line)) {
                    $qty = floatval($this->input->post('qty_' . $id . '_' . $line));
                    $id = intval($id);
                    $error = false;
                    $line = intval($line_num[$key]);
                    $this->db->insert('detail', [
                        'packing_id' => intval($insert_id),
                        'sap_id' => $id,
                        'sap_line' => $line,
                        'planned_amount' => $qty
                    ]);
                    $this->packing->update_planned_qty($insert_id, $db_name, $id, $line);
                }
            }

            if ($error || $this->db->trans_status() === FALSE) {
                $this->output->set_status_header('422');
                $this->db->trans_rollback();
                echo '請勾選出貨計畫表明細';
                return;
            }

            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "新增成功");
        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }
}