<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller
{

    protected $_main_menu = '使用者管理';

    protected $_sub_menu = '權限';

    public function index()
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_ROLE);
        $data = [
            'rows' => $this->db->get('admin')->result()
        ];
        $this->load->view('Role/list', $data);
    }

    public function form($id = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_ROLE);
            $id = intval($id);
        $data = [
            'id' => intval($id)
        ];
        if ($id) {
            $this->db->where('admin_id', $id);
            $row = $this->db->get('admin')->row();
            if (!isset($row->account)) {
                $this->output->set_status_header('500');
                return;
            }
            $data['account'] = $row->account;
            $data['password'] = Sakilu_Encrypt::decode($row->password);
            $data['mail'] = $row->mail;
            $data['role'] = explode(',', $row->role);
            $data['db_role'] = explode(',', $row->db_role);
        }
        $this->load->view('role/form', $data);
    }

    public function post()
    {
        Sakilu_Auth::denyAccessIfNotRole(ROLE_ROLE);
        $update = $id = intval($this->input->post('id', TRUE));
        $update ? $this->form_validation->set_rules('account', '帳號', 'trim|required') :
            $this->form_validation->set_rules('account', '帳號', 'trim|required|is_unique[admin.account]');

        $this->form_validation->set_rules('mail', '信箱', 'trim|valid_email');
        $this->form_validation->set_rules('password', '密碼', 'trim|required');
        $this->form_validation->set_rules('password_2', '密碼確認', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header('422');
            echo validation_errors();
            exit;
        }
        $role = $this->input->post('role[]') ? implode(',', $this->input->post('role[]')) : '';
        $db_role = $this->input->post('db_role[]') ? implode(',', $this->input->post('db_role[]')) : '';

        $data = [
            'account' => $this->input->post('account', TRUE),
            'mail' => $this->input->post('mail', TRUE),
            'password' => Sakilu_Encrypt::encrypt($this->input->post('password')),
            'role' => $role,
            'db_role' => $db_role
        ];
        try {
            if ($update) {
                $this->db->where('admin_id', $id);
                $this->db->update('admin', $data);
            } else {
                $this->db->insert('admin', $data);
            }
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('422');
            echo $e->getMessage();
        }
    }

}