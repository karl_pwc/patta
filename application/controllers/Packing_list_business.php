<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_business extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = '業務確認';

    public function index($db_name = null)
    {
//        $this->output->enable_profiler(TRUE);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_BUSINESS);
        init_packing_db_list($db_name, 'Packing_list_business');
        $rows = $this->packing->get_list_for_business(get_packing_db_name($db_name));
        $this->load->view('packing_list_business/list', ['rows' => $rows]);
    }

    public function post($db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_BUSINESS);
        $this->form_validation->set_rules('packing_id[]', '確認', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->output->set_status_header('422');
            echo validation_errors();
            exit;
        }

        try {
            $this->db->trans_begin();

            $packing_ids = $this->input->post('packing_id[]');
            $notes = $this->input->post('note[]');
            foreach ($packing_ids as $key => $packing_id) {
                $this->packing->update_state($packing_id, 'A', 'B', get_packing_db_name($db_name));
                $this->db->where('packing_id', $packing_id);
                $this->db->update('packing', [
                    'note' => $notes[$key]
                ]);
            }
            if ($this->db->trans_status() === FALSE) {
                $this->output->set_status_header('422');
                $this->db->trans_rollback();
                echo '資料庫錯誤';
                return;
            }
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "確認成功");
        } catch (Exception $e) {
            $this->output->set_status_header('422');
            $this->db->trans_rollback();
            echo $e->getMessage();
        }
    }

    public function pending($id, $db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_BUSINESS);
        $this->packing->update_state($id, 'A', 'P', get_packing_db_name($db_name));
        $this->session->set_flashdata('success_message', "操作完成");
    }
}