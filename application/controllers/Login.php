<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller{

    public function index(){
        $this->load->view("login", array("msg" => $this->session->flashdata('error')));
    }

    public function logout(){
        Sakilu_Auth::logout();
        redirect(site_url("/login"));
    }

    public function post(){
        $this->output->enable_profiler(TRUE);
        $account = $this->input->post("account");
        $password = $this->input->post("password");
        $query = $this->db->get_where('admin', array('account' => $account));

        if($query->num_rows() > 0){
            $row = $query->row();
            if(Sakilu_Encrypt::decode($row->password) != $password){
                $this->session->set_flashdata('error', "密碼錯誤!");
                redirect(site_url("login"));
                return;
            }
            Sakilu_Auth::login($row);
            redirect(site_url("index"));
            return;
        }

        $this->session->set_flashdata('error', "沒有此帳號!");
        redirect(site_url("/login"));
    }

}

