<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ceo_report extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = 'CEO Report';

    public function index($db_name = null)
    {
        set_time_limit(0);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_CEO_REPORT);
        $db_name = get_packing_db_name($db_name);
        init_packing_db_list($db_name, 'ceo_report');

        $rows = $this->ceo->get_ceo_report();
        $this->load->view('ceo_report/list', ['rows' => $rows]);
    }

    public function detail($packing_id)
    {
        set_time_limit(0);
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_CEO_REPORT);
        $db_name = get_packing_db_name('');
        init_packing_db_list($db_name, 'ceo_report');

        $this->load->view('ceo_report/detail', $this->ceo->get_ceo_report_detail($packing_id));
    }

}