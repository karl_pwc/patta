<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_detail extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = '工單裝箱明細';

    public function index($db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_PACKING_DETAIL);
        $db_name = get_packing_db_name($db_name);
        init_packing_db_list($db_name, 'packing_detail');

        $list = $this->packing->get_owor_list($db_name);
        $this->load->view('/Packing_detail/list', [
            'rows' => $list
        ]);
    }

    public function edit($id, $db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_PACKING_DETAIL);
        $db_name = get_packing_db_name($db_name);
        init_packing_db_list($db_name, 'packing_detail');

        $this->load->view('/Packing_detail/edit');
    }

}