<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_ajax_global extends MY_Controller
{

    /**
     * 業務確認 取消pending狀態
     * @param $id
     * @param null $db_name
     */
    public function restore($db_name, $id)
    {
        try {
            $this->db->trans_begin();
            $this->packing->update_state($id, 'P', 'A');
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('403');
            echo $e->getMessage();
            $this->db->trans_rollback();
        }
    }

    /**
     * 業務確認 pending
     * @param $id
     * @param null $db_name
     */
    public function pending($id, $db_name = null)
    {
        $this->packing->update_state($id, 'A', 'P', get_packing_db_name($db_name));
        $this->session->set_flashdata('success_message', "操作完成");
    }

    /**
     * 退回整張出貨計畫表
     * @param $db_name
     * @param $packing_id
     */
    public function cancel_packing($db_name, $packing_id)
    {
        try {
            $this->db->trans_begin();
            $this->packing->cancel_packing($db_name, $packing_id);
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('403');
            echo $e->getMessage();
            $this->db->trans_rollback();
        }
    }

    /**
     * 退回單筆紀錄
     * @param $db_name
     * @param $doc_entry
     * @param $line_num
     */
    public function cancel($packing_id, $db_name, $doc_entry, $line_num)
    {
        try {
            $this->db->trans_begin();
            $this->packing->cancel_detail($packing_id, $db_name, $doc_entry, $line_num);
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('403');
            echo $e->getMessage();
            $this->db->trans_rollback();
        }
    }

    public function update_planned_amount($db_name = null)
    {
        $packing_id = $this->input->post('packing_id');
        $doc_num = $this->input->post('DocEntry', true);
        $line = $this->input->post('LineNum', true);
        $new_amount = $this->input->post('new_amount', true);

        $this->db->trans_begin();
        $this->db->where('sap_id', $doc_num);
        $this->db->where('sap_line', $line);
        $this->db->update('detail', ['planned_amount' =>$new_amount]);
        $this->packing->update_planned_qty($packing_id, $db_name, $doc_num, $line);
        $this->db->trans_commit();

    }

    public function transform($db_name = null, $packing_id)
    {
        $doc_num = $this->input->post('doc_num', true);
        $line = $this->input->post('line', true);
        $target = $this->input->post('target', true);
        if (!$target) {
            $this->output->set_status_header('403');
            echo '請選擇轉單單號';
            return;
        }
        $target = explode('_', $target);

        try {
            $this->db->trans_begin();
            $this->packing->transform(intval($target[0]), $doc_num, $line, $target[1], $packing_id);
            $this->db->trans_commit();
            $this->session->set_flashdata('success_message', "操作成功");
        } catch (Exception $e) {
            $this->output->set_status_header('403');
            echo $e->getMessage();
            $this->db->trans_rollback();
        }
    }
}