<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Packing_list_view extends MY_Controller
{
    protected $_main_menu = '出貨計畫表';

    protected $_sub_menu = '查詢';


    public function index($db_name = null)
    {
        Sakilu_Auth::redirectIfNotLogin('/login');
        Sakilu_Auth::denyAccessIfNotRole(ROLE_VIEW);
        init_packing_db_list($db_name, 'packing_list_view');
        $rows = $this->packing->get_list_for_view(get_packing_db_name($db_name));
        $this->load->view('Packing_list_view/list', ['rows' => $rows]);
    }
}