<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// ------------------------------------------------------------------------
if (!function_exists('init_menu')) {
    function init_menu()
    {
        $ci = &get_instance();
        $ci->config->load('workbench_menu');
        $ci->load->vars(array('workbench_menu' => $ci->config->item('workbench_menu')));
    }
}
if (!function_exists('size_of')) {

    function size_of($var)
    {
        $start_memory = memory_get_usage();
        $tmp = unserialize(serialize($var));
        return intval((memory_get_usage() - $start_memory) / 1024) . 'KB';
    }
}

if (!function_exists('get_db_list')) {

    function get_db_list($check = false)
    {
        $ci = &get_instance();
        $ci->config->load('mssql');
        $return = [];
        $roles = explode(',', Sakilu_Auth::getUser()->db_role);
        foreach ($ci->config->item('db_list') as $v) {
            if (in_array($v['value'], $roles) || $check) {
                $return[] = $v;
            }
        }
        return $return;
    }

    function get_packing_db_list($check = false)
    {
        $ci = &get_instance();
        $ci->config->load('mssql');
        $return = [];
        $roles = explode(',', Sakilu_Auth::getUser()->db_role);
        foreach ($ci->config->item('packing_db_list') as $v) {
            if (in_array($v['value'], $roles) || $check) {
                $return[] = $v;
            }
        }
        return $return;
    }
}

if (!function_exists('init_db_list')) {

    function init_db_list($db_name, $controller)
    {
        $ci = &get_instance();
        $ci->load->vars([
            'db_list' => get_db_list(),
            'db_name' => $db_name,
            'controller' => $controller
        ]);
    }

    function init_packing_db_list($db_name, $controller)
    {
        $ci = &get_instance();
        $ci->load->vars([
            'db_list' => get_packing_db_list(),
            'db_name' => $db_name,
            'controller' => $controller
        ]);
    }
}

if (!function_exists('get_db_name')) {

    function get_packing_db_name($db_name)
    {
        $ci = &get_instance();
        $db_list = get_packing_db_list();
        foreach ($db_list as $db) {
            if ($db['value'] == $db_name) {
                $ci->load->vars([
                    'db' => $db_name
                ]);
                return $db_name;
            }
        }
        $db = reset($db_list);
        $ci->load->vars([
            'db' => $db['value']
        ]);
        return $db['value'];
    }

    function get_db_name($db_name)
    {
        $ci = &get_instance();
        $db_list = get_db_list();
        foreach ($db_list as $db) {
            if ($db['value'] == $db_name) {
                $ci->load->vars([
                    'db' => $db_name
                ]);
                return $db_name;
            }
        }
        $db = reset($db_list);
        $ci->load->vars([
            'db' => $db['value']
        ]);
        return $db['value'];
    }
}

if (!function_exists('get_db_caption')) {

    function get_db_caption($db_name)
    {
        $ci = &get_instance();
        $db_list = get_db_list();
        foreach ($db_list as $db) {
            if ($db['value'] == $db_name) {
                return $db['caption'];
            }
        }
        return null;
    }
}