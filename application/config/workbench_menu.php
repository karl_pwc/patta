<?php
/**
 * Created by PhpStorm.
 * User: sakilu
 * Date: 15/6/24
 * Time: 下午5:12
 */
$config['workbench_menu'] = [
    [
        'icon' => 'icon-home',
        'title' => '首頁',
        'url' => '/index'
    ],
    [
        'icon' => 'icon-wallet',
        'title' => '出貨計畫表',
        'children' => [
            [
                'title' => 'CEO Report',
                'url' => '/ceo_report',
                'role' => ROLE_CEO_REPORT
            ],
            [
                'title' => '查詢',
                'url' => '/packing_list_view',
                'role' => ROLE_VIEW
            ],
            [
                'title' => '編輯',
                'url' => '/packing_list_edit',
                'role' => ROLE_EDIT
            ],
            [
                'title' => '新增',
                'url' => '/packing_list_add',
                'role' => ROLE_ADD
            ],
            [
                'title' => '業務確認',
                'url' => '/packing_list_business',
                'role' => ROLE_BUSINESS
            ],
            [
                'title' => '廠務確認',
                'url' => '/packing_list_factory',
                'role' => ROLE_FACTORY
            ],
            [
                'title' => '船務確認',
                'url' => '/packing_list_shipping',
                'role' => ROLE_SHIPPING
            ],
            [
                'title' => '出貨確認',
                'url' => '/packing_list_delivery',
                'role' => ROLE_DELIVERY
            ]
        ]
    ],
    [
        'icon' => 'icon-docs',
        'title' => '成本分析單',
        'children' => [
            [
                'title' => '新增',
                'url' => '/cost_analysis_add',
                'role' => ROLE_COST_ADD
            ],
            [
                'title' => '維護',
                'url' => '/cost_analysis_maintenance',
                'role' => ROLE_COST_MAINTENANCE
            ],
            [
                'title' => '查詢',
                'url' => '/cost_analysis_view',
                'role' => ROLE_COST_VIEW
            ]
        ]
    ],
    [
        'icon' => 'icon-user',
        'title' => '使用者管理',
        'children' => [
            [
                'title' => '權限',
                'url' => '/role',
                'role' => ROLE_ROLE
            ]
        ]
    ]
];
