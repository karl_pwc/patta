<?php
/**
 * Created by PhpStorm.
 * User: sakilu
 * Date: 15/6/24
 * Time: 下午5:12
 * SAP_KP, SAP_PA, SAP_KF, SAP_BO, SAP_JP, SAP_XW
 */
if (gethostname() == 'DESKTOP-JKUS384') {
    // 此區塊為匡瑋自己主機的資料 改了沒用.  請改 41行後的區塊
    $config['db_list'] = [
        [
            'value' => 'SAP_KP',
            'caption' => 'KP'
        ],
        [
            'value' => 'SAP_PA',
            'caption' => 'PA'
        ],
        [
            'value' => 'SAP_KF',
            'caption' => 'KF'
        ],
        [
            'value' => 'SAP_BO',
            'caption' => 'BO'
        ],
        [
            'value' => 'SAP_XW',
            'caption' => 'XW'
        ],
        [
            'value' => 'SAP_JP',
            'caption' => 'JP'
        ]
    ];
    $config['packing_db_list'] = [
        [
            'value' => 'SAP_KP',
            'caption' => 'KP'
        ],
        [
            'value' => 'SAP_PA',
            'caption' => 'PA'
        ],
        [
            'value' => 'SAP_JP',
            'caption' => 'JP'
        ],
        [
            'value' => 'SAP_BO',
            'caption' => 'BO'
        ]
    ];
} else {
    $config['db_list'] = [
        [
            'value' => 'SAP_KP',
            'caption' => 'KP'
        ],
        [
            'value' => 'SAP_PA',
            'caption' => 'PA'
        ],
        [
            'value' => 'SAP_KF',
            'caption' => 'KF'
        ],
        [
            'value' => 'SAP_BO',
            'caption' => 'BO'
        ],
        [
            'value' => 'SAP_XW',
            'caption' => 'XW'
        ],
        [
            'value' => 'SAP_JP',
            'caption' => 'JP'
        ],
        [
            'value' => 'ERP_Test_00',
            'caption' => 'Test_00'
        ]
    ];
    $config['packing_db_list'] = [
        [
            'value' => 'SAP_KP',
            'caption' => 'KP'
        ],
        [
            'value' => 'SAP_PA',
            'caption' => 'PA'
        ],
        [
            'value' => 'SAP_JP',
            'caption' => 'JP'
        ],
        [
            'value' => 'SAP_BO',
            'caption' => 'BO'
        ]
    ];
}
