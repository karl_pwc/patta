<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<style>
    .page-content, .page-footer, .page-container, #myform > div {
        width: 1800px
    }
</style>
<!-- BEGIN CONTAINER -->
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        船務確認
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="myform" method="POST" class="form-horizontal" action="<?= site_url('packing_list_add/post') ?>">
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            <button id="submit_button" type="button" class="btn green">Submit</button>
                        </p>
                    </div>
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box grey-cascade">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe"></i>船務備註
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <?php $this->load->view('/common/db_select'); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-12">

                                    </div>
                                </div>
                                    <table class="table table-striped table-bordered table-hover" id="table">
                                        <thead>
                                        <tr class="heading">
                                            <th>
                                                備註
                                            </th>
                                            <th style="width: 10px">
                                                操作
                                            </th>
                                            <th style="width: 60px">
                                                計畫號碼
                                            </th>
                                            <th>
                                                訂單號 / PI / 訂單交貨日 / ASAP
                                            </th>
                                            <th>
                                                出貨排程日
                                            </th>
                                            <th>
                                                總櫃數
                                            </th>
                                            <th>
                                                客戶
                                            </th>
                                            <th>
                                                幣別
                                            </th>
                                            <th>
                                                業務人員
                                            </th>
                                            <th>
                                                裝箱數量
                                            </th>
                                            <th>
                                                貨到港
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($rows as $row) { ?>
                                            <?php $data = reset($row); ?>
                                            <tr>
                                                <td>
                                                    <input type="hidden" name="packing_id[]"
                                                           value="<?= $data->packing_id ?>" />
                                                    <textarea type="text" name="note[]"></textarea>
                                                </td>
                                                <td nowrap>
                                                    <i style="cursor:pointer"
                                                       onclick="_open(<?= $data->packing_id ?>, this)"
                                                       class="fa fa-search-plus"></i>

                                                    <i style="cursor:pointer"
                                                       onclick="_print(<?= $data->packing_id ?>, this)"
                                                       class="fa fa-print"></i>
                                                </td>
                                                <td>
                                                    PN-<?= $data->packing_id ?>
                                                </td>
                                                <td>
                                                    <table class="table table-striped table-bordered table-hover dataTable">
                                                        <tbody>
                                                        <?php
                                                        $str = '';
                                                        foreach ($row as $detail) {
                                                            if ($detail->U_PINO != $str) {
                                                                $str = $detail->U_PINO;
                                                                ?>
                                                                <tr>
                                                                    <td style="width: 20px"><?= $detail->DocEntry ?></td>
                                                                    <td style="width: 40px"><?= $detail->U_PINO ?></td>
                                                                    <td style="width: 40px"><?= date('m/d/Y', strtotime(substr($detail->DocDueDate, 0, 10))) ?></td>
                                                                    <td style="width: 70px"><?= $detail->U_DocType ?></td>
                                                                </tr>
                                                            <?php }
                                                        } ?>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <?= date('m/d/Y', strtotime($data->date)) ?>
                                                </td>
                                                <td>
                                                    <?= $data->amount ?>
                                                </td>
                                                <td>
                                                    <?= $data->CardName ?>
                                                </td>
                                                <td>
                                                    <?= $data->DocCur ?>
                                                </td>
                                                <td>
                                                    <?= $data->SlpName ?>
                                                </td>
                                                <td>
                                                    <?= $data->total_mpcs ?>
                                                </td>
                                                <td>
                                                    <?= $data->U_ShipPort ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <tfoot>
                                        <th>
                                            <button type="button" onclick="yadcf.exResetAllFilters(submit_table)"
                                                    class="btn btn-warning">重設
                                            </button>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        </tfoot>
                                    </table>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </form>

        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>

    var submit_table;

    var detail = <?=json_encode($rows)?>;

    var _open = function (id, dom) {
        var data = detail[id];
        var tr = $(dom).closest('tr');
        var row = submit_table.row(tr);
        if (row.child.isShown()) {
            row.child.hide();
        }
        else {
            row.child(format(data)).show();
        }
    }

    var format = function (rows) {
        var t = $('<div><table class="table table-bordered table-hover"><thead><th>訂單編號</th><th>LineNum</th><th>待出貨批數</th><th>已計畫數</th><th>已包裝批數</th><th>品號</th><th>品名</th><th>公制規格</th><th>英制規格</th><th>訂單數量</th><th>未出貨數量</th><th>計量單位</th><th>倉別</th></thead><tbody></tbody></table></div>');
        _.forEach(rows, function (row, key) {
            row['U_SpecM'] = row['U_SpecM'] ? row['U_SpecM'] : '';
            row['U_SpecE'] = row['U_SpecE'] ? row['U_SpecE'] : '';
            row['unitMsr'] = row['unitMsr'] ? row['unitMsr'] : '';

            $(t).find('tbody:last').append("<tr><td>" + row['DocEntry'] + "</td><td><input type='hidden' name='DocEntry[]' value='" + row['DocEntry'] + "' /><input type='hidden' name='LineNum[]' value='" + row['LineNum'] + "' />" + row['LineNum'] + "</td><td>" + Number(row['planned_amount']).toFixed(2) + "</td><td>" + Number(row['U_PlannedQty']).toFixed(2) + "</td><td>" + Number(row['detail_total']).toFixed(2) + "</td><td>" + row['ItemCode'] + "</td><td>" + row['ItemName'] + "</td><td>" + row['U_SpecM'] + "</td><td>" + row['U_SpecE'] + "</td><td>" + Number(row['Quantity']).toFixed(2) + "</td><td>" + Number(row['OpenQty']).toFixed(2) + "</td><td>" + row['unitMsr'] + "</td><td>" + row['WhsCode'] + "</td></tr>");
        });
        return t.html();
    }

    jQuery(document).ready(function () {
        submit_table = $('#table').DataTable({
            "autoWidth": true,
            "dom": 'lrtip', "columnDefs": [
                {
                    "targets": 0,
                    "orderable": false
                },
                {
                    "targets": 1,
                    "orderable": false
                }
            ],
            "order": [[3, 'asc']]
        });

        yadcf.init(submit_table, [
            {column_number: 2, filter_type: "range_number", filter_reset_button_text: false},
            {column_number: 3, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {
                column_number: 4,
                filter_type: "range_date",
                filter_reset_button_text: false,
                style_class: 'form-control'
            },
            {column_number: 5, filter_type: "range_number", filter_reset_button_text: false},
            {
                column_number: 6,
                filter_type: "select",
                filter_reset_button_text: false,
                style_class: 'form-control',
                filter_default_label: 'Select'
            },
            {
                column_number: 7,
                filter_type: "select",
                filter_reset_button_text: false,
                style_class: 'form-control',
                filter_default_label: 'Select'
            },
            {
                column_number: 8,
                filter_type: "select",
                filter_reset_button_text: false,
                style_class: 'form-control',
                filter_default_label: 'Select'
            },
            {
                column_number: 9,
                filter_type: "range_number",
                filter_reset_button_text: false,
                style_class: 'form-control'
            },
            {column_number: 10, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
        ], 'tfoot');

        $('#submit_button').click(function () {
            $.ajax({
                url: '<?=site_url('/packing_list_shipping/post')?>/' + $('#db_select').val(),
                method: 'POST',
                data: $('#myform').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });
    });
</script>
</body>
</html>