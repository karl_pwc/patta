<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>
    .page-content, .page-footer, .page-container {
        width: 1500px
    }
</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        新增出貨計畫表
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="myform" method="POST" class="form-horizontal" action="<?= site_url('/packing_list_add/post') ?>">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box grey-cascade">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe"></i>新增出貨計畫表
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="col-md-12">
                                    <p>
                                        <button id="submit_button" type="button" class="btn green">Submit</button>
                                    </p>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <?php $this->load->view('/common/db_select'); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-12">

                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">出貨排程日</label>

                                            <div class="col-md-4">
                                                <input id="date" name="date" type="text" class="form-control"
                                                       placeholder="Enter text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">總櫃數</label>

                                            <div class="col-md-4">
                                                <input name="amount" type="text" class="form-control"
                                                       placeholder="Enter text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">廠區</label>

                                            <div class="col-md-4">
                                                <select name="group_name" id="group_name" class="form-control">
                                                    <option value="SAP_KP">鋐昇實業股份有限公司</option>
                                                    <option value="SAP_PA">煒鈞實業股份有限公司</option>
                                                    <option value="SAP_JP">昆山敬鹏建筑五金有限公司</option>
                                                    <option value="SAP_BO">保信國際有限公司</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                                <div>
                                    <table class="table table-striped table-bordered table-hover" id="submit_table">
                                        <thead>
                                        <tr class="heading">
                                            <th style="white-space: nowrap;width:140px">

                                            </th>
                                            <th style="width: 80px">
                                                訂單編號
                                            </th>
                                            <th>
                                                PI No
                                            </th>
                                            <th>
                                                客戶
                                            </th>
                                            <th>
                                                業務人員
                                            </th>
                                            <th>
                                                幣別
                                            </th>
                                            <th style="width: 120px">
                                                原始訂單交貨日
                                            </th>
                                            <th>
                                                ASAP
                                            </th>
                                            <th>
                                                貨到港
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <br/>
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </form>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box grey-cascade">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="add_form">
                                <div class="table-scrollable">
                                    <table style="width: 1300px" class="table table-striped table-bordered table-hover"
                                           id="table">
                                        <thead>
                                        <tr class="heading">
                                            <th>

                                            </th>
                                            <th>
                                                訂單編號
                                            </th>
                                            <th>
                                                PI No
                                            </th>
                                            <th>
                                                客戶
                                            </th>
                                            <th>
                                                業務人員
                                            </th>
                                            <th>
                                                幣別
                                            </th>
                                            <th>
                                                原始訂單交貨日
                                            </th>
                                            <th>
                                                ASAP
                                            </th>
                                            <th>
                                                貨到港
                                            </th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <th>
                                            <button type="button" class="btn btn-warning"
                                                    onclick="_reset()">重置
                                            </button>
                                            <script>
                                                var _reset = function () {
                                                    $('#add_form').trigger("reset");
                                                    $('#DocEntry_From').trigger("blur");
                                                    $('#DocEntry_To').trigger("blur");
                                                    $('#U_PINO_From').trigger("blur");
                                                    $('#U_PINO_To').trigger("blur");
                                                    $('#CardName').trigger("blur");
                                                    $('#SlpName').trigger("blur");
                                                    $('#DocCur').trigger("blur");
                                                    $('#DocDueDate_From').trigger("blur");
                                                    $('#DocDueDate_To').trigger("blur");
                                                    $('#U_DocType').trigger("blur");
                                                    $('#U_Ship').trigger("blur");
                                                    submit_table.draw();
                                                }
                                            </script>
                                        </th>
                                        <th>
                                            <input type="text" placeholder="from"
                                                   class="form-control" id="DocEntry_From"/>
                                            <input type="text" placeholder="to" class="form-control"
                                                   id="DocEntry_To"/>

                                        </th>
                                        <th>
                                            <input type="text" placeholder="from"
                                                   class="form-control" id="U_PINO_From"/>
                                            <input type="text" placeholder="to"
                                                   class="form-control"
                                                   id="U_PINO_To"/>
                                        </th>
                                        <th>
                                            <input type="text" class="form-control" id="CardName"/>
                                        </th>
                                        <th>
                                            <input type="text" class="form-control" id="SlpName"/>
                                        </th>
                                        <th>
                                            <input type="text" class="form-control" id="DocCur"/>
                                        </th>
                                        <th>
                                            <input type="text" placeholder="from"
                                                   class="form-control" id="DocDueDate_From"/>
                                            <input type="text" placeholder="to"
                                                   class="form-control"
                                                   id="DocDueDate_To"/>
                                        </th>
                                        <th>
                                            <input type="text" class="form-control" id="U_DocType"/>
                                        </th>
                                        <th>
                                            <input type="text" class="form-control" id="U_Ship"/>
                                        </th>
                                        </tfoot>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <br/>
                                    <br/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>
    jQuery(document).ready(function () {
        $('#date').datepicker({
            format: "yyyy-mm-dd"
        });

        table = $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": '<?=site_url('packing_list_add/table')?>/' + $('#db_select').val(),
            "ordering": false,
            "sDom": "lrtip",
            "autoWidth": true
        });

        submit_table = $('#submit_table').DataTable({
            "ordering": false,
            "sDom": "rti",
            "autoWidth": true
        });

        $('#DocEntry_From, #DocEntry_To').on('blur', function () {
            var _from = _.parseInt($('#DocEntry_From').val());
            var _to = _.parseInt($('#DocEntry_To').val());
            if (isNaN(_from)) _from = '';
            if (isNaN(_to)) _to = '';
            $('#DocEntry_From').val(_from);
            $('#DocEntry_To').val(_to);
            table.columns(1).search(_from + ',' + _to).draw();
        });
        $('#U_PINO_From, #U_PINO_To').on('blur', function () {
            var _from = $('#U_PINO_From').val().trim();
            var _to = $('#U_PINO_To').val().trim();

            table.columns(2).search(_from + ',' + _to).draw();
        });
        $('#CardName').on('blur', function () {
            table.columns(3).search(this.value).draw();
        });
        $('#SlpName').on('blur', function () {
            table.columns(4).search(this.value).draw();
        });
        $('#DocCur').on('blur', function () {
            table.columns(5).search(this.value).draw();
        });
        $('#DocDueDate_From, #DocDueDate_To').on('blur', function () {
            var _from = $('#DocDueDate_From').val().trim();
            var _to = $('#DocDueDate_To').val().trim();

            table.columns(6).search(_from + ',' + _to).draw();
        });
        $('#U_DocType').on('blur', function () {
            table.columns(7).search(this.value).draw();
        });
        $('#U_Ship').on('blur', function () {
            table.columns(8).search(this.value).draw();
        });
        $('#submit_button').click(function () {
            $.ajax({
                url: '<?=site_url('/packing_list_add/post')?>/' + $('#db_select').val(),
                method: 'POST',
                data: $('#myform').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });
    });

    var _delete = function (dom) {
        var _tbody = $(dom).closest('tbody');
        if (_tbody.find('button').size() == 2) {
            $('#CardName').val('').prop('readonly', false);
            $('#DocCur').val('').prop('readonly', false);
            table.columns(3).search('').draw();
            table.columns(5).search('').draw();
        }
        submit_table.row($(dom).closest('tr')).remove().draw();
    }

    var add = function (dom) {
        var tds = $(dom).closest('tr').find('td');
        var id = tds.eq(0).find('button').attr('data-id');
        var input = "<button type='button' class='btn red' onclick='_delete(this)'>刪除</button><button type='button' class='btn btn-default' onclick='_open(this)' id='" + id + "'>細項</button>";
        if ($('#' + id).size()) {
            return;
        }
        $.ajax({
            url: '<?=site_url('/packing_list_add/line_data')?>/' + id + '/' + $('#db_select').val(),
            method: 'POST',
            data: $('#myform').serialize(),
            dataType: 'json',
            beforeSend: function (xhr) {
                Metronic.blockUI();
            }
        }).done(function (data) {
            submit_table.row.add([
                input,
                tds.eq(1).html(),
                tds.eq(2).html(),
                tds.eq(3).html(),
                tds.eq(4).html(),
                tds.eq(5).html(),
                tds.eq(6).html(),
                tds.eq(7).html(),
                tds.eq(8).html()
            ]).child(render_table(data)).draw().show();

            var card_name = tds.eq(3).text().trim();
            var doc_cur = tds.eq(5).text().trim();
            $('#CardName').val(card_name).prop('readonly', true);
            $('#DocCur').val(doc_cur).prop('readonly', true);
            table.columns(3).search(card_name).draw();
            table.columns(5).search(doc_cur).draw();
            Metronic.unblockUI();
            Metronic.initAjax();
        }).fail(function (data) {
            Metronic.unblockUI();
            alert(data.responseText);
        });
    }

    var submit_table;

    var table;

    var _open = function (dom) {
        var tr = $(dom).closest('tr');
        var row = submit_table.row(tr);
        if (row.child.isShown()) {
            row.child.hide();
        }
        else {
            row.child.show();
        }
    }

    var render_table = function (rows) {
        var t = $('<div><table class="table table-bordered table-hover"><thead><th></th><th>計畫數</th><th>LineNum</th><th>品號</th><th>品名</th><th>公制規格</th><th>英制規格</th><th>訂單數量</th><th>未出貨數量</th><th>已計畫數量</th><th>棧板數</th><th>計量單位</th><th>倉別</th></thead><tbody></tbody></table></div>');
        _.forEach(rows, function (row, key) {
            row['U_SpecM'] = row['U_SpecM'] ? row['U_SpecM'] : '';
            row['U_SpecE'] = row['U_SpecE'] ? row['U_SpecE'] : '';
            row['unitMsr'] = row['unitMsr'] ? row['unitMsr'] : '';
            if((Number(row['Quantity']) - Number(row['U_PlannedQty'])).toFixed(2) <= 0){
                return;
            }
            $(t).find('tbody:last').append("<tr><td><input checked value='1' type='checkbox' name='" + row['DocEntry'] + '_' + row['LineNum'] + "' /></td><td><input type='text' class='form-control' name='qty_" + row['DocEntry'] + '_' + row['LineNum'] + "' value='" + (Number(row['Quantity']) - Number(row['U_PlannedQty'])).toFixed(2) + "' /></td><td><input type='hidden' name='DocEntry[]' value='" + row['DocEntry'] + "' /><input type='hidden' name='LineNum[]' value='" + row['LineNum'] + "' />" + row['LineNum'] + "</td><td>" + row['ItemCode'] + "</td><td>" + row['ItemName'] + "</td><td>" + row['U_SpecM'] + "</td><td>" + row['U_SpecE'] + "</td><td>" + Number(row['Quantity']).toFixed(2) + "</td><td>" + Number(row['OpenQty']).toFixed(2) + "</td><td>" + Number(row['U_PlannedQty']).toFixed(2) + "</td><td>" + Number(row['U_PLTQty']).toFixed(2) + "</td><td>" + row['unitMsr'] + "</td><td>" + row['WhsCode'] + "</td></tr>");
        });
        return t.html();
    }
</script>
</body>
</html>