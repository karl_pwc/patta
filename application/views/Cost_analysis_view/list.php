<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>
</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        查詢成本分析單
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="queryForm" method="POST" class="form-horizontal" action="<?= site_url('/cost_analysis_view/post') ?>">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue-hoki">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe"></i>成本分析單條件查詢
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <strong>公司別 :</strong>

                                                <select id="db_select" name="db_select" class="form-control input-inline">
                                                    <?php foreach ($db_list as $v) { ?>
                                                        <option <?=$v['value'] == $db_name ? 'selected' : ''?> value="<?= $v['value'] ?>"><?= $v['caption'] ?></option>
                                                    <?php } ?>
                                                </select>

                                                <script>
                                                    $(function(){
                                                        $('#db_select').change(function(){
                                                            Metronic.blockUI();
                                                            window.location = '<?=site_url("/$controller")?>/index/'+$(this).val();
                                                        });
                                                    });
                                                </script>

                                                <strong>成本分析單位 :</strong>

                                                <select id="plant" name="plant" class="form-control input-inline">
                                                    <<?php foreach ($plant_list as $plant) { ?>
                                                        <option value="<?= $plant['plant'] ?>"><?= $plant['plant'] ?></option>
                                                    <?php } ?>
                                                </select>

                                                <strong>狀態 :</strong>

                                                <select id="status" name="status" class="form-control input-inline">
                                                    <?php foreach(CostAnalysis::$status as $key=>$name){ ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $name; ?></option>
                                                    <?php } ?>
                                                </select>

                                                <strong>報價單單號 :</strong>

                                                起

                                                <input id="Doc_Entry_From" name="Doc_Entry_From" type="text" class="form-control input-inline"
                                                       placeholder="起始單號" value="">

                                                止

                                                <input id="Doc_Entry_To" name="Doc_Entry_To" type="text" class="form-control input-inline"
                                                       placeholder="結束單號" value="">

                                                <button id="query_button" type="button" class="btn green">查詢</button>


                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <strong>品號 :</strong>

                                                起

                                                <input id="Product_From" name="Item_Code_From" type="text" class="form-control input-inline"
                                                       placeholder="起始品號" value="">

                                                止

                                                <input id="Product_To" name="Item_Code_To" type="text" class="form-control input-inline"
                                                       placeholder="結束品號" value="">

                                                <strong>成本分析單號 :</strong>

                                                起

                                                <input id="Cost_Analysis_From" name="Cost_Analysis_From" type="text" class="form-control input-inline"
                                                       placeholder="起始單號" value="">

                                                止

                                                <input id="Cost_Analysis_To" name="Cost_Analysis_To" type="text" class="form-control input-inline"
                                                       placeholder="結束單號" value="">

                                            </div>
                                        </div>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </form>
            <!-- END PAGE HEADER-->
            <div id="result" class="row" style="">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box blue-madison">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>查詢結果
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="add_form">
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover" id="table">
                                        <thead>
                                        <tr class="heading">
                                            <th style="white-space: nowrap;width:40px">
                                                操作
                                            </th>
                                            <th>
                                                成本分析單號
                                            </th>
                                            <th>
                                                報價單號
                                            </th>
                                            <th>
                                                報價來源公司
                                            </th>
                                            <th>
                                                申請人
                                            </th>
                                            <th>
                                                需求日
                                            </th>
                                            <th>
                                                狀態
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <br/>
                                    <br/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>
    var table;
    $(function() {

        $('#query_button').click(function () {
            $('#result').hide();

            if ( $.fn.dataTable.isDataTable('#table')) {
                table.destroy();
            }

            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '<?=site_url('cost_analysis_view/table')?>?' + $('#queryForm').serialize(),
                "ordering": false,
                "sDom": "lrtip",
                "autoWidth": true
            });

            $('#result').show();

        });

    });
</script>
</body>
</html>