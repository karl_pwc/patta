<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>
    .table-input tr.heading > th {
        background-color: #fcf8e3 !important;
    }
</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        維護成本分析單
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="submitForm" method="POST" class="form-horizontal" action="<?= site_url('/cost_analysis_maintenance/post') ?>">
                <div class="row" style="width: 1500px">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue-hoki" style="margin-bottom: 0px">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe"></i>基本資料
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">成本分析單單號</label>
                                            <input type="hidden" id="cost_analysis_id" name="cost_analysis_id" value="<?php echo $data->cost_analysis_id ?>">
                                            <label class="col-md-4 control-label" style="text-align: left;"><?php echo $data->cost_analysis_id ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">來源公司</label>

                                            <label class="col-md-4 control-label" style="text-align: left;"><?php echo get_db_caption($data->db) ?></label>

                                            <label class="col-md-1 control-label">過帳日期</label>

                                            <label class="col-md-4 control-label" style="text-align: left;"><?php echo $data->doc_date ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">申請人</label>

                                            <label class="col-md-4 control-label" style="text-align: left;"><?php echo $data->req_name ?></label>

                                            <label class="col-md-1 control-label">預計完成日</label>

                                            <label class="col-md-4 control-label" style="text-align: left;"><?php echo $data->date_plan ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">狀態</label>

                                            <label class="col-md-4 control-label" style="text-align: left;"><?php echo CostAnalysis::$status[$data->status] ?></label>

                                            <label class="col-md-1 control-label">實際完成日</label>

                                            <label class="col-md-4 control-label" style="text-align: left;"><?php echo $data->date_act ?></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Memo</label>
                                            <label class="col-md-8 control-label" style="text-align: left;">
                                                <?php echo $data->memo ?>
                                            </label>
                                        </div>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                <!-- END PAGE HEADER-->
                <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box">
                        <div class="portlet-body">
                            <div class="portlet-body form">
                                <div class="tabbable-custom ">
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <a href="#tab_detail" data-toggle="tab">
                                                詳細項目 </a>
                                        </li>
                                        <li class="">
                                            <a href="#tab_file" data-toggle="tab">
                                                附件 </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_detail">
                                            <table class="table table-striped table-bordered table-hover" id="table">
                                                <!--
                                                <thead>
                                                <tr class="heading">
                                                    <th style="width:2.5%;">基礎文件號</th>
                                                    <th style="width:2.5%;">基礎列號</th>
                                                    <th>項目代號</th>
                                                    <th>項目說明</th>
                                                    <th style="width:1.5%;">數量</th>
                                                    <th>Pcs/Box</th>
                                                    <th style="width:2.5%;">盒料號</th>
                                                    <th>Box/Ctn</th>
                                                    <th style="width:2.5%;">箱料號</th>
                                                    <th>Ctn/PLT</th>
                                                    <th style="width:2.5%;">棧板料號</th>
                                                    <th>報價計量單位</th>
                                                    <th>線材單重</th>
                                                    <th>線材單價</th>
                                                    <th style="width:2.5%;">線材成本</th>
                                                    <th>包材成本</th>
                                                    <?php for ($i=1; $i<=5 ;$i++) { ?>
                                                        <th>物料<?php echo str_pad($i, 2, "0", STR_PAD_LEFT);?></th>
                                                    <?php } ?>
                                                    <?php for ($i=1; $i<=5 ;$i++) { ?>
                                                        <th>工序<?php echo str_pad($i, 2, "0", STR_PAD_LEFT);?></th>
                                                    <?php } ?>
                                                    <th style="width:3%;">小計_料成本</th>
                                                    <th style="width:3%;">小計_加工成本</th>
                                                    <th>其他費用</th>
                                                    <th>管銷成本</th>
                                                    <th style="width:3%;">總計</th>
                                                    <th>其他說明</th>
                                                </tr>
                                                </thead>
                                                -->
                                                <tbody>
                                                <?php foreach ($data->detail as $row) { ?>
                                                    <tr role="row" class="odd">
                                                        <table class="table table-striped table-bordered table-hover" style="margin:0px;">
                                                            <thead>
                                                            <tr class="heading">
                                                                <th>文件號</th>
                                                                <th>列號</th>
                                                                <th>項目代號</th>
                                                                <th>項目說明</th>
                                                                <th>數量</th>
                                                                <th>Pcs/Box</th>
                                                                <th>盒料號</th>
                                                                <th>Box/Ctn</th>
                                                                <th>箱料號</th>
                                                                <th>Ctn/PLT</th>
                                                                <th>棧板料號</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row" class="odd">
                                                                <input type="hidden" class="detail_id" name="detail_id[]" value="<?php echo $row->id ?>" data-code="<?php echo $row->item_code ?>">
                                                                <td><?php echo $row->base_entry ?></td>
                                                                <td><?php echo $row->base_line ?></td>
                                                                <td><?php echo $row->item_code ?></td>
                                                                <td><?php echo $row->item_name ?></td>
                                                                <td><?php echo floatval($row->quantity) ?></td>
                                                                <td><?php echo floatval($row->pcs_box) ?></td>
                                                                <td><?php echo $row->box_item ?></td>
                                                                <td><?php echo floatval($row->box_ctn) ?></td>
                                                                <td><?php echo $row->ctn_item ?></td>
                                                                <td><?php echo floatval($row->ctn_plt) ?></td>
                                                                <td><?php echo $row->plt_item ?></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </tr>
                                                    <tr role="row" class="odd">
                                                        <table class="table table-striped table-bordered table-hover table-input" style="margin:0px;">
                                                            <thead>
                                                            <tr class="heading">
                                                                <th>報價計量單位</th>
                                                                <th>線材單重</th>
                                                                <th>線材單價</th>
                                                                <th>線材成本</th>
                                                                <th>包材成本</th>
                                                                <?php for ($i=1; $i<=5 ;$i++) { ?>
                                                                    <th>物料<?php echo str_pad($i, 2, "0", STR_PAD_LEFT);?></th>
                                                                <?php } ?>
                                                                <?php for ($i=1; $i<=5 ;$i++) { ?>
                                                                    <th>工序<?php echo str_pad($i, 2, "0", STR_PAD_LEFT);?></th>
                                                                <?php } ?>
                                                                <th>小計料成本</th>
                                                                <th>小計加工成本</th>
                                                                <th>其他費用</th>
                                                                <th>管銷成本</th>
                                                                <th>總計</th>
                                                                <th>其他說明</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr role="row" class="odd">
                                                                <td>
                                                                    <?php echo $row->uom ?>
                                                                </td>
                                                                <td><?php echo floatval($row->wire_weight) ?:'' ?></td>
                                                                <td><?php echo floatval($row->wire_up) ?:'' ?></td>
                                                                <td><?php echo floatval($row->wire_cst) ?:'' ?></td>
                                                                <td><?php echo floatval($row->pkg_cost) ?:'' ?></td>
                                                                <?php for ($i=1; $i<=5 ;$i++) { ?>
                                                                    <td><?php echo floatval($row->{'item'.str_pad($i, 2, "0", STR_PAD_LEFT)}) ?:'' ?></td>
                                                                <?php } ?>
                                                                <?php for ($i=1; $i<=5 ;$i++) { ?>
                                                                    <td><?php echo floatval($row->{'process'.str_pad($i, 2, "0", STR_PAD_LEFT)}) ?:'' ?></td>
                                                                <?php } ?>
                                                                <td><?php echo floatval($row->sub_mcst) ?:'' ?></td>
                                                                <td><?php echo floatval($row->sub_lcst) ?:'' ?></td>
                                                                <td><?php echo floatval($row->sub_exp) ?:'' ?></td>
                                                                <td><?php echo floatval($row->sub_mng) ?:'' ?></td>
                                                                <td><?php echo floatval($row->total) ?:'' ?></td>
                                                                <td><?php echo $row->text ?></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="tab-pane" id="tab_file" style="width: 1470px">
                                            <table class="table table-bordered table-hover" id="file_table">
                                                <thead>
                                                <tr class="heading">
                                                    <th>
                                                        檔案名稱
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
            </form>
            <form id="uploadForm" method="POST" class="form-horizontal" action="<?= site_url('/cost_analysis_maintenance/upload') ?>" enctype="multipart/form-data">
                <input id="upload" type="file" name="attachment" style="display: none">
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>
    var file_table;
    $(function() {
        loadFileTable();
    });

    function loadFileTable(){
        if ( $.fn.dataTable.isDataTable('#file_table')) {
            file_table.destroy();
        }
        file_table = $('#file_table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": '<?=site_url('cost_analysis_view/file_table')?>/'+ $('#cost_analysis_id').val(),
            "ordering": false,
            "sDom": "lrtip",
            "autoWidth": true
        });
    }
</script>
</body>
</html>