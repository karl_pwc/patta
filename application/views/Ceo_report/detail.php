<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>

</style>
<style>
    th {
        text-align: center;
    }
</style>
<?php
$temp = [];
$temp_line = [];
?>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <?php if ($title) { ?>
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <?= $title->company_name ?><br/>
                        計畫表號: PN-<?= $title->packing_id ?>,
                        出貨排程日: <?= $title->date ?>,
                        總櫃數: <?= $title->amount ?>,
                        客戶: <?= $title->CardName ?>,
                        幣別: <?= $title->DocCur ?>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <table rules="all"
                               style="width: 1350px;border:1px solid;border-collapse: collapse;font-size: 14px;word-break:break-all;">
                            <tbody>
                            <?php foreach ($rows as $doc_entry => $rows2) { ?>
                                <?php foreach ($rows2 as $line_num => $rows3) { ?>
                                    <?php foreach ($rows3 as $data) { ?>
                                        <?php if (!isset($temp[$doc_entry])) { ?>
                                            <tr style="background-color: #9BC2E6;">
                                                <td colspan="16">
                                                    訂單編號: <?= $data->DocEntry ?>,
                                                    PINo: <?= $data->U_PINo ?>,
                                                    業務人員: <?= $data->SlpName ?>
                                                </td>
                                            </tr>
                                            <tr style="background-color: #9BC2E6;">
                                                <th>
                                                    Line
                                                </th>
                                                <th>
                                                    待出貨批數
                                                </th>
                                                <th>
                                                    待出貨<br/>盒數
                                                </th>
                                                <th>
                                                    待出貨<br/>棧板數
                                                </th>
                                                <th>
                                                    品號
                                                </th>
                                                <th>
                                                    品名
                                                </th>
                                                <th>
                                                    公制規格
                                                </th>
                                                <th>
                                                    訂單數量
                                                </th>
                                                <th>
                                                    累計出貨量
                                                </th>
                                                <th>
                                                    待出貨量
                                                </th>
                                                <th>
                                                    倉別
                                                </th>
                                                <th>
                                                    PCS/<br/>BOX
                                                </th>
                                                <th>
                                                    BOX/<br/>CTN
                                                </th>
                                                <th>
                                                    CTN/<br/>PLT
                                                </th>
                                                <th>
                                                    MPCS/<br/>PLT
                                                </th>
                                            </tr>
                                        <?php } ?>
                                        <tr style="text-align: center">
                                            <td style="text-align: center">
                                                <?= $data->LineNum ?>
                                            </td>
                                            <td>
                                                <?= $data->planned_amount ?>
                                            </td>
                                            <td>
                                                <?php if (intval($data->U_MPcsPLT) > 0) { ?>
                                                    <?= @number_format($data->planned_amount * $data->U_BoxCtn *
                                                        $data->U_CtnPLT / $data->U_MPcsPLT, 2) ?>
                                                <?php } else { ?>
                                                    0
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php if (intval($data->U_MPcsPLT) > 0) { ?>
                                                    <?= @number_format($data->planned_amount / $data->U_MPcsPLT, 2) ?>
                                                <?php } else { ?>
                                                    0
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?= $data->ItemCode ?>
                                            </td>
                                            <td>
                                                <?= $data->ItemName ?>
                                            </td>
                                            <td>
                                                <?= $data->U_SpecM ?>
                                            </td>
                                            <td>
                                                <?= number_format($data->Quantity, 3) ?>
                                            </td>
                                            <td>
                                                <?= number_format($data->Quantity - $data->OpenQty, 3) ?>
                                            </td>
                                            <td>
                                                <?= number_format($data->OpenQty, 3) ?>
                                            </td>
                                            <td>
                                                <?= $data->WhsCode ?>
                                            </td>
                                            <td>
                                                <?= number_format($data->U_PcsBox, 0) ?>
                                            </td>
                                            <td>
                                                <?= number_format($data->U_BoxCtn, 0) ?>
                                            </td>
                                            <td>
                                                <?= number_format($data->U_CtnPLT, 0) ?>
                                            </td>
                                            <td>
                                                <?= number_format($data->U_MPcsPLT, 1) ?>
                                            </td>
                                        </tr>
                                        <?php if (!$data->packed) { ?>
                                            <tr>
                                                <td colspan="16" style="text-align: center">
                                                    尚未裝箱
                                                </td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="16">
                                                    <center>
                                                        <table rules="all"
                                                               style="margin-top:20px;margin-bottom: 20px;width: 95%;border:1px solid;border-collapse: collapse;font-size: 14px;word-break:break-all;">
                                                            <thead>
                                                            <tr style="background-color: pink">
                                                                <th>批號</th>
                                                                <th>代號</th>
                                                                <th>棧板起號</th>
                                                                <th>棧板迄號</th>
                                                                <th>棧板數</th>
                                                                <th>MPCS</th>
                                                                <th>箱數</th>
                                                                <th>盒數</th>
                                                                <th>箱淨重 (kg)</th>
                                                                <th>箱毛重 (kg)</th>
                                                                <th>總箱淨重 (kg)</th>
                                                                <th>總箱毛重 (kg)</th>
                                                                <th>櫃號</th>
                                                            </tr>
                                                            </thead>

                                                            <?php foreach ($detail[$doc_entry][$line_num] as
                                                                           $detail_data) { ?>
                                                                <tr style="text-align: center;background-color: #C0E0B4;">
                                                                    <td><?= $detail_data->batch_no ?></td>
                                                                    <td><?= $detail_data->code ?></td>
                                                                    <td><?= $detail_data->pallet_start ?></td>
                                                                    <td><?= $detail_data->pallet_end ?></td>
                                                                    <td><?= $detail_data->pallet_amount ?></td>
                                                                    <td><?= $detail_data->mpcs ?></td>
                                                                    <td><?= $detail_data->cartons ?></td>
                                                                    <td><?= $detail_data->boxes ?></td>
                                                                    <td><?= $detail_data->nw ?></td>
                                                                    <td><?= $detail_data->gw ?></td>
                                                                    <td><?= $detail_data->total_nw ?></td>
                                                                    <td><?= $detail_data->total_gw ?></td>
                                                                    <td><?= $detail_data->container_number ?></td>
                                                                </tr>
                                                                <?php
                                                                @$total_nw += $detail_data->total_nw;
                                                                @$total_gw += $detail_data->total_gw;
                                                                @$total_pallet += $detail_data->pallet_amount;
                                                                @$total_boxes += $detail_data->boxes;
                                                                @$total_cartons += $detail_data->cartons;
                                                                @$total_boxes += $detail_data->boxes;
                                                                @$total_mpcs += $detail_data->mpcs;
                                                            } ?>

                                                        </table>
                                                    </center>
                                                </td>
                                            </tr>
                                        <?php } ?>

                                        <?php
                                        $temp[$doc_entry] = true;
                                        ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-12">
                        尚無裝箱資料
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            <?php } ?>
            <?php if ($total_nw) { ?>
                總棧板數: <?= $total_pallet ?> MPCS(UNIT): <?= $total_mpcs ?> 箱數: <?= $total_cartons ?> 盒數: <?= $total_boxes ?> 總淨重: <?= $total_nw ?> 總毛重: <?= $total_gw ?>
            <?php } ?>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
</body>
</html>