<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>

</style>
<?php
function to_word($number)
{
    if ($number == 1) {
        return '一';
    }
    if ($number == 2) {
        return '二';
    }
    if ($number == 3) {
        return '三';
    }
    if ($number == 4) {
        return '四';
    }
    if ($number == 5) {
        return '五';
    }
    if ($number == 6) {
        return '六';
    }
    if ($number == 7) {
        return '日';
    }

}

function state($state)
{
    if ($state == 'A') {
        return "待業務確認";
    } elseif ($state == 'B') {
        return "待廠務確認";
    } elseif ($state == 'C') {
        return "待船務確認";
    } elseif ($state == 'D') {
        return "待出貨確認";
    } elseif ($state == 'E') {
        return "已出貨";
    } elseif ($state == 'P') {
        return "pending";
    } else {
        return "還未排程";
    }
}

?>
<style>
    th {
        text-align: center;
    }
</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <table rules="all"
                           style="width: 1300px;border:1px solid;border-collapse: collapse;font-size: 14px;word-break:break-all;">
                        <thead style="background-color: #9BC2E6;">
                        <th>業</th>
                        <th>廠</th>
                        <th>船</th>
                        <th>#</th>
                        <th>計畫表號</th>
                        <th>出貨日期</th>
                        <th>asap</th>
                        <th>週別</th>
                        <th>客戶</th>
                        <th>櫃數</th>
                        <th>總盒數</th>
                        <th>總基數</th>
                        <th>備註</th>
                        <th>條件備註</th>
                        </thead>
                        <tbody>
                        <?php foreach ($rows as $month => $companies_set) { ?>
                            <?php
                            $total_month['amount'] = 0;
                            $total_month['total_ctn'] = 0;
                            $total_month['total_Quantity'] = 0;
                            $total_month['finish'] = 0;
                            ?>
                            <?php foreach ($companies_set as $name => $orders) { ?>
                                <?php
                                $order_data = reset($orders);
                                ?>
                                <tr>
                                    <td colspan="14"><?= $order_data->company_name ?>
                                        <?php
                                        $company_month['amount'] = 0;
                                        $company_month['total_ctn'] = 0;
                                        $company_month['total_Quantity'] = 0;
                                        $company_month['finish'] = 0;
                                        ?>
                                    </td>
                                </tr>
                                <?php foreach ($orders as $order) { ?>
                                    <tr <?php
                                    if ($order->state == 'E' || $order->state == 'D') {
                                        echo "style='background-color: #C0E0B4'";
                                    }
                                    ?>>
                                        <td style="text-align: center;width: 32px">
                                            <?php
                                            if ($order->state == 'A') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } elseif ($order->state == 'B') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'C') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'D') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'E') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'P') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } else {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center;width: 32px">
                                            <?php
                                            if ($order->state == 'A') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } elseif ($order->state == 'B') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } elseif ($order->state == 'C') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'D') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'E') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'P') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } else {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center;width: 32px">
                                            <?php
                                            if ($order->state == 'A') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } elseif ($order->state == 'B') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } elseif ($order->state == 'C') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } elseif ($order->state == 'D') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'E') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/check.png'));
                                            } elseif ($order->state == 'P') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            } else {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center;width: 32px">
                                            <?php
                                            if ($order->state == 'P') {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/cross.png'));
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center" width="70px">PN-<?= $order->packing_id ?></td>
                                        <td style="text-align: center" width="100px"><?= $order->date ?></td>
                                        <td style="text-align: center" width="44px">
                                            <?php
                                            if (strpos($order->U_DocType, 'ASAP') !== false) {
                                                echo sprintf('<img src="%s" />', base_url('/assets/uploads/asap.ico'));
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: center" width="50px"><?= to_word($order->wk) ?></td>
                                        <td><a target="_blank" href="<?= base_url('/ceo_report/detail/' .
                                                $order->packing_id) ?>"><?= $order->CardName ?></a></td>
                                        <td style="text-align: center;width: 85px">
                                            <?= number_format($order->amount, 1) ?>
                                            <?php
                                            $company_month['amount'] = $company_month['amount'] + $order->amount;
                                            $company_month['total_ctn'] =
                                                $company_month['total_ctn'] + $order->total_ctn;
                                            $company_month['total_Quantity'] =
                                                $company_month['total_Quantity'] + $order->total_Quantity;

                                            $total_month['amount'] = $total_month['amount'] + $order->amount;
                                            $total_month['total_ctn'] = $total_month['total_ctn'] + $order->total_ctn;
                                            $total_month['total_Quantity'] =
                                                $total_month['total_Quantity'] + $order->total_Quantity;
                                            if ($order->state == 'E' || $order->state == 'D') {
                                                $company_month['finish'] =
                                                    $company_month['finish'] + $order->total_Quantity;
                                                $total_month['finish'] =
                                                    $total_month['finish'] + $order->total_Quantity;
                                            }
                                            ?>
                                        </td>
                                        <td style="text-align: right;width: 120px">
                                            <?= number_format($order->total_ctn) ?>
                                        </td>
                                        <td style="text-align: right;width: 150px">
                                            <?= number_format($order->total_Quantity) ?>
                                        </td>
                                        <td width="150px">
                                            <?= $order->note ?>
                                        </td>
                                        <td width="150px">
                                            <?= $order->PortName ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr style="background-color: pink">
                                    <td colspan="9" style="text-align: right">小計</td>
                                    <td style="text-align: center"><?= number_format($company_month['amount'],
                                            1) ?></td>
                                    <td style="text-align: right"><?= number_format($company_month['total_ctn']) ?></td>
                                    <td style="text-align: right"><?= number_format($company_month['total_Quantity']) ?></td>
                                    <td colspan="2" style="text-align: right;text-align: right;">
                                        已出貨基數<?= number_format($company_month['finish']) ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="9" style="text-align: right">
                                    總計(<?= date('m', mktime(0, 0, 0, intval(substr($order->date, 5, 2)),
                                        intval(substr($order->date, 8, 2)), intval(substr($order->date, 0, 4)))) ?>月)
                                </td>
                                <td style="text-align: center;background-color: pink">
                                    <?= number_format($total_month['amount'], 1) ?>
                                </td>
                                <td style="background-color: pink;text-align: right;"><?= number_format($total_month['total_ctn']) ?></td>
                                <td style="background-color: pink;text-align: right;"><?= number_format($total_month['total_Quantity']) ?></td>
                                <td colspan="2" style="background-color: pink;text-align: right;">
                                    已出貨基數<?= number_format($total_month['finish']) ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
</body>
</html>