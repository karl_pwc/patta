<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>
</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        新增成本分析單
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="queryForm" method="POST" class="form-horizontal" action="<?= site_url('/cost_analysis_add/post') ?>">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue-hoki">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe"></i>新增成本分析單條件查詢
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="col-md-8">
                                                <strong>公司別 :</strong>

                                                <select id="db_select" name="db_select" class="form-control input-inline">
                                                    <?php foreach ($db_list as $v) { ?>
                                                        <option <?=$v['value'] == $db_name ? 'selected' : ''?> value="<?= $v['value'] ?>"><?= $v['caption'] ?></option>
                                                    <?php } ?>
                                                </select>
                                                <script>
                                                    $(function(){
                                                        $('#db_select').change(function(){
                                                            Metronic.blockUI();
                                                            window.location = '<?=site_url("/$controller")?>/index/'+$(this).val();
                                                        });
                                                    });
                                                </script>


                                                <strong>成本分析單位 :</strong>

                                                    <select id="u_plant_select" name="u_plant_select" class="form-control input-inline">
                                                        <?php foreach ($plant_list as $plant) { ?>
                                                            <option value="<?= $plant['U_Plant'] ?>"><?= $plant['U_Plant'] ?></option>
                                                        <?php } ?>
                                                    </select>

                                                <strong>報價單單號 :</strong>

                                                起
                                                <input id="DocEntry_From" name="DocEntry_From" type="text" class="form-control input-inline"
                                                       placeholder="起始單號" value="">

                                                止
                                                <input id="DocEntry_To" name="DocEntry_To" type="text" class="form-control input-inline"
                                                       placeholder="結束單號" value="">

                                                <button id="query_button" type="button" class="btn green">查詢</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </form>
            <!-- END PAGE HEADER-->
            <div id="result" class="row" style="">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box blue-madison">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>選擇報價單產生成本分析單
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-scrollable">
                                <table class="table table-bordered table-hover" id="table">
                                    <thead>
                                    <tr class="heading">
                                        <th style="white-space: nowrap;width:20px">
                                            <input type="checkbox" id="check_all" name="check_all" value="1">
                                        </th>
                                        <th style="width: 80px">
                                            報價單單號
                                        </th>
                                        <th>
                                            申請人
                                        </th>
                                        <th>
                                            預計完成日
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                                <br/>
                                <br/>
                            </div>
                            <div class="portlet">
                            <div class="portlet-body">
                                <div class="portlet-body form">
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-4">
                                                    <button id="submit_button" type="button" class="btn red">新增成本分析單</button>
                                                    <button id="cancel_button" type="button" class="btn default" onclick="location.href='/cost_analysis_add/index/SAP_KP'">取消</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="DB" />
                <input type="hidden" id="U_Plant" />
                <!-- END EXAMPLE TABLE PORTLET-->
                <form id="submitForm">

                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>
    var table;
    var rows_selected = [];
    var rows_selected_date = [];
    $(function() {

        $('#query_button').click(function () {

            $('#result').hide();

            $('#DB').val($('#db_select').val());
            $('#U_Plant').val($('#u_plant_select').val());
            rows_selected = [];
            rows_selected_date = [];

            if ( $.fn.dataTable.isDataTable('#table')) {
                table.destroy();
            }

            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '<?=site_url('cost_analysis_add/table')?>?' + $('#queryForm').serialize(),
                "ordering": false,
                "sDom": "lrtip",
                "autoWidth": true,
                "fnDrawCallback": function( oSettings ) {
                    Metronic.initAjax();
                    $('.date').datepicker({
                        format: "yyyy-mm-dd"
                    }).on('changeDate', function(en) {
                        var input = $(this).parent().find('input[type="text"]');
                        var inputDate = input.val();
                        var row = $(this).closest('tr');
                        var index = $.inArray(input.attr('data-id'), rows_selected);
                        if(index !== -1){
                            rows_selected_date[index] = inputDate;
                        }
                    });
                },
                rowCallback: function(row, data, dataIndex){
                    // Get row ID
                    var rowId = $(row).find('input[type=checkbox]').attr('data-id');

                    // If row ID is in the list of selected row IDs
                    var index = $.inArray(rowId, rows_selected);
                    if(index !== -1){
                        $(row).find('input[type="checkbox"][check-type="select"]').prop('checked', true);
                        $(row).find('input[type="text"][data-id="'+rowId+'"]').val(rows_selected_date[index]);
                        $(row).addClass('warning');
                    }
                }
            });

            $('#result').show();

            // Handle click on checkbox
            $('#table tbody').on('click', 'input[type="checkbox"][check-type="select"]', function(e){
                var row = $(this).closest('tr');

                // Get row ID
                var rowId = row.find('input[type=checkbox]').attr('data-id');

                // Get row Date
                var rowDate = row.find('input[type="text"][data-id="'+rowId+'"]').val();

                // Determine whether row ID is in the list of selected row IDs
                var index = $.inArray(rowId, rows_selected);

                // If checkbox is checked and row ID is not in list of selected row IDs
                if(this.checked && index === -1){
                    rows_selected.push(rowId);
                    rows_selected_date.push(rowDate);
                    // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
                } else if (!this.checked && index !== -1){
                    rows_selected.splice(index, 1);
                    rows_selected_date.splice(index, 1);
                }

                if(this.checked){
                    row.addClass('warning');
                } else {
                    row.removeClass('warning');
                }

                // Update state of "Select all" control
                updateDataTableSelectAllCtrl(table);

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });

            // Handle click on table cells with checkboxes
            $('#table').on('click', 'tbody td:first-child, thead th:first-child', function(e){
                $(this).parent().find('input[type="checkbox"]').trigger('click');
            });

            // Handle click on "Select all" control
            $('#check_all').on('click', function(e){
                if(this.checked){
                    $('#table tbody input[type="checkbox"][check-type="select"]:not(:checked)').trigger('click');
                } else {
                    $('#table tbody input[type="checkbox"][check-type="select"]:checked').trigger('click');
                }

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });

            // Handle table draw event
            table.on('draw', function(){
                // Update state of "Select all" control
                updateDataTableSelectAllCtrl(table);
            });
        });


        // Handle form submission event
        $('#submit_button').click(function() {

            if(rows_selected.length==0){
                form_error_message('請選擇至少一筆報價單並填寫預計完成日期');
                return false;
            }

            rows_selected = rows_selected.map(Number);
            array_multisort(rows_selected, rows_selected_date);
            rows_selected = rows_selected.map(String);

            var unFilled = [];
            // Check all row selected dates is filled or not
            $.each(rows_selected, function(index, rowId){
                if(rows_selected_date[index]==''){
                    unFilled.push(rowId);
                }
            });
            if(unFilled.length>0){
                form_error_message('以下選取之報價單單號尚未填寫預計完成日期 : <br/>'+unFilled.join(",<br/>"));
                return false;
            }

            // Iterate over all selected checkboxes
            $.each(rows_selected, function(index, rowId){
                // Create a hidden element
                $('#submitForm').append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'DocEntry[]')
                        .val(rowId)
                );
            });
            $.each(rows_selected_date, function(index, rowDate){
                // Create a hidden element
                $('#submitForm').append(
                    $('<input>')
                        .attr('type', 'hidden')
                        .attr('name', 'DatePlan[]')
                        .val(rowDate)
                );
            });

            $.ajax({
                url: '<?=site_url('/cost_analysis_add/post')?>/' + $('#DB').val() + '/' + $('#U_Plant').val(),
                method: 'POST',
                data: $('#submitForm').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI({ message: 'SAVING...' });
                }
            }).done(function (data) {
                window.location.href='<?=site_url('/cost_analysis_maintenance/index')?>';
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });
    });

    //
    // Updates "Select all" control in a data table
    //
    function updateDataTableSelectAllCtrl(table){
        var $table             = table.table().node();
        var $chkbox_all        = $('tbody input[type="checkbox"][check-type="select"]', $table);
        var $chkbox_checked    = $('tbody input[type="checkbox"][check-type="select"]:checked', $table);
        var chkbox_check_all  = $('#check_all');

        // If none of the checkboxes are checked
        if($chkbox_checked.length === 0){
            chkbox_check_all.parent().removeClass('checked');
            chkbox_check_all.prop('checked', false);
            // If all of the checkboxes are checked
        } else if ($chkbox_checked.length === $chkbox_all.length){
            chkbox_check_all.parent().addClass('checked');
            chkbox_check_all.prop('checked', true);
            // If some of the checkboxes are checked
        } else {
            chkbox_check_all.parent().removeClass('checked');
            chkbox_check_all.prop('checked', false);
        }
    }


</script>
</body>
</html>