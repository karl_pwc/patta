<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>
    .page-content, .page-footer, .page-container {
        width: 1500px
    }
</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        廠務確認
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="myform" method="POST" class="form-horizontal"
                  action="<?= site_url('packing_list_factory/post') ?>">
                <!-- END PAGE HEADER-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>廠務確認
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <div class="form-body">
                            <?php $data = reset($rows); ?>
                            <?php $packing_id = $data->packing_id ?>
                            <h3 class="form-section">訂單資料</h3>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>
                                        出貨計畫表單號: <?= $data->packing_id ?>,
                                        出貨排程日: <?= $data->date ?>,
                                        總櫃數: <?= $data->amount ?>,
                                        客戶: <?= $data->CardName ?>,
                                        幣別: <?= $data->DocCur ?>
                                        <?php
                                        $doc_cur = $data->DocCur;
                                        $card_code = $data->CardCode;
                                        $name = $data->Name;
                                        ?>

                                    </p>
                                </div>
                            </div>
                            <div>
                                <button type="button" id="submit_button" class="btn green">儲存並送至船務確認</button>
                                <br/><br/>
                                <button type="button" id="save_button" class="btn green">儲存</button>
                                <button type="button" id="back_button" class="btn blue">回列表</button>
                            </div>
                            <?php
                            $orders = [];
                            $detail = [];
                            foreach ($rows as $row) {
                                $orders[$row->DocEntry] = $row;
                            }
                            foreach ($rows as $row) {
                                $detail[$row->DocEntry][] = $row;
                            }
                            ?>

                            <?php foreach ($orders as $order) { ?>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h4>裝箱明細</h4>

                                        <p>
                                            訂單編號: <?= $order->DocEntry ?>,
                                            PINo: <?= $order->U_PINO ?>,
                                            業務人員: <?= $order->SlpName ?>,
                                            ASAP: <?= $order->U_DocType ?>
                                        </p>

                                        <p>
                                            總棧板數: <span id="_pallet_amount"></span>
                                            MPCS(UNIT): <span id="_mpcs"></span>
                                            箱數: <span id="_cartons"></span>
                                            盒數: <span id="_boxes"></span>
                                            總淨重: <span id="_nw"></span>
                                            總毛重: <span id="_gw"></span>
                                        </p>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="portlet-body">
                                    <table id="<?= $order->DocEntry ?>"
                                           class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr class="heading">
                                            <th>
                                                裝箱<br/>明細
                                            </th>
                                            <th>
                                                操作
                                            </th>
                                            <th>
                                                Line
                                            </th>
                                            <th>
                                                待出貨批數
                                            </th>
                                            <th>
                                                待出貨盒數
                                            </th>
                                            <th>
                                                待出貨棧板數
                                            </th>
                                            <th>
                                                品號
                                            </th>
                                            <th style="width: 100px">
                                                品名
                                            </th>
                                            <th>
                                                公制規格
                                            </th>
                                            <th>
                                                訂單數量
                                            </th>
                                            <th>
                                                累計出貨量
                                            </th>
                                            <th>
                                                待出貨量
                                            </th>
                                            <th>
                                                倉別
                                            </th>
                                            <th>
                                                庫存量
                                            </th>
                                            <th>
                                                PCS/<br/>BOX
                                            </th>
                                            <th>
                                                BOX/<br/>CTN
                                            </th>
                                            <th>
                                                CTN/<br/>PLT
                                            </th>
                                            <th>
                                                MPCS(UNIT)/<br/>PLT
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($detail[$order->DocEntry] as $row) { ?>
                                            <tr data-itemcode="<?= $row->ItemCode ?>"
                                                data-whscode="<?= $row->WhsCode ?>" data-line="<?= $row->LineNum ?>">
                                                <td>
                                                    <i style="cursor:pointer" class="fa fa-edit open_button"
                                                       data-id="<?= $row->DocEntry ?>"
                                                       onclick="_open(<?= $row->DocEntry ?> ,this)">
                                                    </i>
                                                </td>
                                                <td>
                                                    <a onclick="transform(<?= $row->DocEntry ?>, <?= $row->LineNum ?>)"
                                                       data-toggle="modal" href="#full"> <i style="cursor:pointer"
                                                                                            class="fa fa-trash-o"></i>
                                                    </a>
                                                </td>
                                                <td>
                                                    <?= $row->LineNum ?>
                                                </td>
                                                <td>
                                                    <?= $row->planned_amount ?>
                                                </td>
                                                <td>
                                                    <?php if (intval($row->U_MPcsPLT) > 0) { ?>
                                                        <?= @$row->planned_amount * $row->U_BoxCtn * $row->U_CtnPLT /
                                                        $row->U_MPcsPLT ?>
                                                    <?php } else { ?>
                                                        0
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php if (intval($row->U_MPcsPLT) > 0) { ?>
                                                        <?= @$row->planned_amount / $row->U_MPcsPLT ?>
                                                    <?php } else { ?>
                                                        0
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?= $row->ItemCode ?>
                                                </td>
                                                <td>
                                                    <?= $row->ItemName ?>
                                                </td>
                                                <td>
                                                    <?= $row->U_SpecM ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->Quantity, 3) ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->Quantity - $row->OpenQty, 3) ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->OpenQty, 3) ?>
                                                </td>
                                                <td>
                                                    <?= $row->WhsCode ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->OnHand, 3) ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->U_PcsBox, 0) ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->U_BoxCtn, 0) ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->U_CtnPLT, 0) ?>
                                                </td>
                                                <td>
                                                    <?= number_format($row->U_MPcsPLT, 1) ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bs-modal-lg" id="full" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width: 1200px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">轉單</h4>
            </div>
            <div class="modal-body">
                <form id="modal_form">
                    <input type="hidden" id="doc_num" name="doc_num"/>
                    <input type="hidden" id="line" name="line"/>

                    <table id="modal-table" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr class="heading">
                            <th>
                                選取
                            </th>
                            <th>
                                計畫號碼
                            </th>
                            <th>
                                出貨排程日
                            </th>
                            <th>
                                總櫃數
                            </th>
                            <th>
                                客戶
                            </th>
                            <th>
                                幣別
                            </th>
                            <th>
                                出貨單號
                            </th>
                            <th>
                                船務備註
                            </th>
                            <th>
                                狀態
                            </th>
                            <th>
                                原始訂單交貨日
                            </th>
                            <th>
                                ASAP
                            </th>
                            <th>
                                貨到港
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($transform_table as $row) { ?>
                            <?php
                            $data = reset($row);
                            if ($data->DocCur != $doc_cur || $data->CardCode != $card_code || $data->Name != $name
                            ) continue;
                            ?>
                            <tr>
                                <td>
                                    <label>
                                        <input type="radio" name="target"
                                               value="<?= $data->packing_id ?>_<?= $data->state ?>"/>
                                    </label>
                                </td>
                                <td>
                                    PN-<?= $data->packing_id ?>
                                </td>
                                <td>
                                    <?= $data->date ?>
                                </td>
                                <td>
                                    <?= $data->amount ?>
                                </td>
                                <td>
                                    <?= $data->CardName ?>
                                </td>
                                <td>
                                    <?= $data->DocCur ?>
                                </td>
                                <td>
                                    <?= $data->sap_packing_id ?>
                                </td>
                                <td>
                                    <?= nl2br($data->note) ?>
                                </td>
                                <td>
                                    <?php
                                    if ($data->state == 'A') {
                                        echo "待業務確認";
                                    } elseif ($data->state == 'B') {
                                        echo "待廠務確認";
                                    } elseif ($data->state == 'C') {
                                        echo "待船務確認";
                                    } elseif ($data->state == 'D') {
                                        echo "待出貨確認";
                                    } elseif ($data->state == 'E') {
                                        echo "已出貨";
                                    } elseif ($data->state == 'P') {
                                        echo "pending";
                                    } else {
                                        echo "還未排程";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?= substr($data->DocDueDate, 0, 10) ?>
                                </td>
                                <td>
                                    <?= $data->U_DocType ?>
                                </td>
                                <td>
                                    <?= $data->Name ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <th>

                        </th>
                        <th>
                            <input type="text" placeholder="from"
                                   class="form-control" id="Id_From"/>
                            <input type="text" placeholder="to" class="form-control"
                                   id="Id_To"/>

                        </th>
                        <th>
                            <input type="text" placeholder="from"
                                   class="form-control" id="Date_From"/>
                            <input type="text" placeholder="to"
                                   class="form-control"
                                   id="Date_To"/>
                        </th>
                        <th>
                            <input type="text" placeholder="from"
                                   class="form-control" id="Amount_From"/>
                            <input type="text" placeholder="to"
                                   class="form-control"
                                   id="Amount_To"/>
                        </th>
                        <th>
                            <input type="text" class="form-control" id="CardName"/>
                        </th>
                        <th>
                            <input type="text" class="form-control" id="DocCur"/>
                        </th>
                        <th>
                            <input type="text" class="form-control" id="Sap_id"/>
                        </th>
                        <th>
                            <input type="text" class="form-control" id="Note"/>
                        </th>
                        <th>
                            <input type="text" class="form-control" id="State"/>
                        </th>
                        <th>
                            <input type="text" placeholder="from"
                                   class="form-control" id="DocDueDate_From"/>
                            <input type="text" placeholder="to"
                                   class="form-control"
                                   id="DocDueDate_To"/>
                        </th>
                        <th>
                            <input type="text" class="form-control" id="DocType"/>
                        </th>
                        <th>
                            <input type="text" class="form-control" id="ShipPort"/>
                        </th>
                        </tfoot>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">取消</button>
                <button type="button" id="modal_submit" class="btn blue">送出</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>

    var table_array = [];

    var detail = <?=$detail_json?>;

    var modal_table;


    var _open = function (id, dom) {
        var tr = $(dom).closest('tr');
        var row = table_array[id].row(tr);
        if (row.child.isShown()) {
            row.child.hide();
        }
        else {
            row.child.show();
        }
    }

    $('#Id_From, #Id_To, #Date_From, #Date_To, #Amount_From, #Amount_To, #CardName, #DocCur, #Sap_id, #Note,#State,#DocDueDate_From, #DocDueDate_To, #DocType, #ShipPort').keyup(function () {
        modal_table.draw();
    });

    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {

            var id_from = parseInt($('#Id_From').val(), 10);
            var id_to = parseInt($('#Id_To').val(), 10);
            var date_from = $('#Date_From').val();
            var date_to = $('#Date_To').val();
            var amounot_from = parseFloat($('#Amount_From').val());
            var amounou_to = parseFloat($('#Amount_To').val());
            var cardname = $('#CardName').val();
            var doccur = $('#DocCur').val();
            var sap_id = $('#Sap_id').val();
            var note = $('#Note').val();
            var state = $('#State').val();
            var due_date_from = $('#DocDueDate_From').val();
            var due_date_to = $('#DocDueDate_To').val();
            var doctype = $('#DocType').val();
            var shipport = $('#ShipPort').val();

            if (isNaN(id_from) && isNaN(id_to) && date_from.trim() == '' && date_to.trim() == ''
                && doccur.trim() == '' && cardname.trim() == '' && isNaN(amounot_from) && isNaN(amounou_to) && sap_id.trim() == '' && note.trim() == '' && state.trim() == '' && due_date_from.trim() == '' && due_date_to.trim() == '' && doctype.trim() == '' && shipport.trim() == ''
            ) {
                return true;
            }
            var _id = (parseInt(data[1]) || 0);
            var _date = data[2].trim();
            var _amount = data[3].trim();
            var _cardname = data[4].trim();
            var _doccur = data[5].trim();
            var _sap_id = data[6].trim();
            var _note = data[7].trim();
            var _state = data[8].trim();
            var _due_date = data[9].trim();
            var _doctype = data[10].trim();
            var _shipport = data[11].trim();


            if (!isNaN(id_from) || !isNaN(id_to)) {
                if (
                    !(
                        ( isNaN(id_from) && _id <= id_to ||
                        ( id_from <= _id && isNaN(id_to) ) ||
                        ( id_from <= _id && _id <= id_to ) ))
                ) {
                    return false;
                }
            }
            if (date_from.trim() != '' || date_to.trim() != '') {
                if (
                    !(
                    ( date_from.trim() == '' && _date <= date_to) ||
                    ( date_from <= _date && date_to.trim() == '' ) ||
                    ( date_from <= _date && _date <= date_to ) )) {
                    return false;
                }
            }
            if (!isNaN(amounot_from) || !isNaN(amounou_to)) {
                if (
                    !(
                        ( isNaN(amounot_from) && _amount <= amounou_to ||
                        ( amounot_from <= _amount && isNaN(amounou_to) ) ||
                        ( amounot_from <= _amount && _amount <= amounou_to ) ))
                ) {
                    return false;
                }
            }
            if (cardname.trim() != '') {
                if (_cardname.indexOf(cardname) == -1) {
                    return false;
                }
            }
            if (doccur.trim() != '') {
                if (_doccur.indexOf(doccur) == -1) {
                    return false;
                }
            }
            if (sap_id.trim() != '') {
                if (_sap_id.indexOf(sap_id) == -1) {
                    return false;
                }
            }
            if (note.trim() != '') {
                if (_note.indexOf(note) == -1) {
                    return false;
                }
            }
            if (state.trim() != '') {
                if (_state.indexOf(state) == -1) {
                    return false;
                }
            }
            if (due_date_from.trim() != '' || due_date_to.trim() != '') {
                if (
                    !(
                    ( due_date_from.trim() == '' && _due_date <= due_date_to) ||
                    ( due_date_from <= _due_date && due_date_to.trim() == '' ) ||
                    ( due_date_from <= _due_date && _due_date <= due_date_to ) )) {
                    return false;
                }
            }
            if (doctype.trim() != '') {
                if (_doctype.indexOf(doctype) == -1) {
                    return false;
                }
            }
            if (shipport.trim() != '') {
                if (_shipport.indexOf(shipport) == -1) {
                    return false;
                }
            }
            return true;
        }
    );

    $('#back_button').click(function () {
        window.location = '<?=site_url('/packing_list_factory/index')?>/<?=$db_name?>';
    });

    var format =
        '<table class="table table-bordered table-hover">' +
        '<thead>' +
        '<tr>' +
        '<th></th>' +
        '<th>批號</th>' +
        '<th>代號</th>' +
        '<th>棧板起號</th>' +
        '<th>棧板迄號</th>' +
        '<th>棧板數</th>' +
        '<th>MPCS</th>' +
        '<th>箱數</th>' +
        '<th>盒數</th>' +
        '<th>箱淨重 (kg)</th>' +
        '<th>箱毛重 (kg)</th>' +
        '<th>總箱淨重 (kg)</th>' +
        '<th>總箱毛重 (kg)</th>' +
        '<th>櫃號</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>' +
        '</tbody>' +
        '</table>';

    var _delete = function (dom) {
        var id = $(dom).closest('td').find("[name='id[]']").val();
        if (id > 0) {
            $(dom).closest('td').find("[name='delete[]']").val(1);
            $(dom).closest('tr').hide();
        } else {
            $(dom).closest('tr').remove();
        }
    }
    var _add = function (doc_num, line, item_code, whs_code, dom) {
        var tr = '<tr>' +
            '<td>' +
            '<input type="hidden" name="doc_num[]" value="' + doc_num + '" />' +
            '<input type="hidden" name="line[]" value="' + line + '" />' +
            '<input type="hidden" name="id[]" value="0" />' +
            '<input type="hidden" name="delete[]" value="0" />' +
            '<input type="hidden" name="item_code[]" value="' + item_code + '" />' +
            '<input type="hidden" name="whs_code[]" value="' + whs_code + '" />' +
            '<i style="cursor:pointer" class="fa fa-trash-o"' +
            'onclick="_delete(this)"></i>' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" type="text" name="batch_no[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" type="text" name="code[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 60px" type="text" class="pallet_start" name="pallet_start[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 60px" type="text" class="pallet_end" name="pallet_end[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 60px" onchange="total()" type="text" class="pallet_amount" name="pallet_amount[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" onchange="total()" type="text" class="mpcs" name="mpcs[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" onchange="total()" type="text" class="cartons" name="cartons[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" onchange="total()" type="text" class="boxes" name="boxes[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" onchange="total()" type="text" class="nw" name="nw[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" onchange="total()" type="text" class="gw" name="gw[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" onchange="total()" type="text" class="total_nw"  name="total_nw[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" onchange="total()" type="text" class="total_gw"  name="total_gw[]" value="" />' +
            '</td>' +
            '<td>' +
            '<input style="width: 90px" type="text" name="container_number[]" value="" />' +
            '</td>' +
            '</tr>';
        $(dom).closest('td').find('tbody').append(tr);
    }

    var total = function () {
        var pallet_amount = 0;
        var mpcs = 0;
        var cartons = 0;
        var boxes = 0;
        var total_nw = 0;
        var total_gw = 0;

        $('.pallet_amount').each(function () {
            console.log('pallet_amount');
            pallet_amount = pallet_amount + to_number($(this).val());
        });
        $('.mpcs').each(function () {
            mpcs = mpcs + to_number($(this).val());
        });
        $('.cartons').each(function () {
            cartons = cartons + to_number($(this).val());
        });
        $('.boxes').each(function () {
            boxes = boxes + to_number($(this).val());
        });
        $('.total_nw').each(function () {
            total_nw = total_nw + to_number($(this).val());
        });
        $('.total_gw').each(function () {
            total_gw = total_gw + to_number($(this).val());
            console.log($(this));
        });

        $('#_pallet_amount').html(pallet_amount);
        $('#_mpcs').html(mpcs);
        $('#_cartons').html(cartons);
        $('#_boxes').html(boxes);
        $('#_nw').html(total_nw);
        $('#_gw').html(total_gw);
    }

    var to_number = function (value) {
        var new_value = clearString(value);
        if (new_value == '') return 0;
        return parseFloat(new_value);
    }

    function clearString(s) {
        var pattern = new RegExp("[A-Za-z_]")
        var rs = "";
        for (var i = 0; i < s.length; i++) {
            rs = rs + s.substr(i, 1).replace(pattern, '');
        }
        return rs;
    }
    var _init = function (_html, doc_num, line, item_code, whs_code) {
        var _dom = $(_html);
        _dom = _dom.clone().wrap('<p>').parent();
        if (detail && detail[doc_num] && detail[doc_num][line] && Array.isArray(detail[doc_num][line])) {
            detail[doc_num][line].forEach(function (n) {
                var tr = '<tr>' +
                    '<td>' +
                    '<input type="hidden" name="doc_num[]" value="' + doc_num + '" />' +
                    '<input type="hidden" name="line[]" value="' + line + '" />' +
                    '<input type="hidden" name="id[]" value="' + n.id + '" />' +
                    '<input type="hidden" name="delete[]" value="0" />' +
                    '<input type="hidden" name="item_code[]" value="' + item_code + '" />' +
                    '<input type="hidden" name="whs_code[]" value="' + whs_code + '" />' +
                    '<i style="cursor:pointer" class="fa fa-trash-o"' +
                    'onclick="_delete(this)"></i>' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" name="batch_no[]" value="' + n.batch_no + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" name="code[]" value="' + n.code + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 60px" type="text" class="pallet_start" name="pallet_start[]" value="' + n.pallet_start + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 60px" type="text" class="pallet_end" name="pallet_end[]" value="' + n.pallet_end + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 60px" type="text" onchange="total()" class="pallet_amount" name="pallet_amount[]" value="' + n.pallet_amount + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" onchange="total()" class="mpcs" name="mpcs[]" value="' + n.mpcs + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" onchange="total()" class="cartons" name="cartons[]" value="' + n.cartons + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" onchange="total()" class="boxes" name="boxes[]" value="' + n.boxes + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" onchange="total()" class="nw" name="nw[]" value="' + n.nw + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" onchange="total()" class="gw" name="gw[]" value="' + n.gw + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" onchange="total()" class="total_nw" name="total_nw[]" value="' + n.total_nw + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" onchange="total()" class="total_gw" name="total_gw[]" value="' + n.total_gw + '" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" name="container_number[]" value="' + n.container_number + '" />' +
                    '</td>' +
                    '</tr>';
                _dom.find('tbody').append(tr);
            });
        }
        return _dom.wrap('<p>').html();
    }

    var transform = function (doc_num, line) {
        $('#doc_num').val(doc_num);
        $('#line').val(line);
    }

    jQuery(document).ready(function () {
        $('#modal_submit').click(function () {
            $.ajax({
                url: '<?=site_url('/packing_list_ajax_global/transform')?>/<?=$db_name?>/<?= $packing_id ?>',
                method: 'POST',
                data: $('#modal_form').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    $('#full').modal('hide');
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });

        $("body").delegate(".pallet_start, .pallet_end, .pallet_amount, .cartons, .boxes, .nw, .gw", "change", function () {
            var top_tr = $(this).closest('table').closest('tr').prev();
            var box_plt = $(top_tr).find('td').eq(15).text().trim();
            var ctn_plt = $(top_tr).find('td').eq(16).text().trim();
            var mpcs_plt = $(top_tr).find('td').eq(17).text().trim();

            var tr = $(this).closest('tr');
            var pallet_start = $(tr).find('.pallet_start').val();
            var pallet_end = $(tr).find('.pallet_end').val();
            var pallet_amount = $(tr).find('.pallet_amount').val();
            var mpcs = $(tr).find('.mpcs').val();


            $(tr).find('.pallet_amount').val(to_number(pallet_end) - to_number(pallet_start) + 1);
            $(tr).find('.mpcs').val($(tr).find('.pallet_amount').val() * to_number(mpcs_plt));
            $(tr).find('.cartons').val($(tr).find('.pallet_amount').val() * to_number(ctn_plt));
            $(tr).find('.boxes').val($(tr).find('.cartons').val() * to_number(box_plt));

            var total_nw = to_number($(tr).find('.cartons').val()) * to_number($(tr).find('.nw').val());
            var total_gw = to_number($(tr).find('.cartons').val()) * to_number($(tr).find('.gw').val());
            $(tr).find('.total_nw').val(total_nw.toFixed(2));
            $(tr).find('.total_gw').val(total_gw.toFixed(2));
            total();
        });

        modal_table = $('#modal-table').DataTable({
            "autoWidth": true,
            "pageLength": 5,
            'dom': 'rtip',
            "columnDefs": [
                {
                    "targets": 0,
                    "orderable": false
                }
            ],
            "order": [[1, 'asc']]
        });

        <?php foreach ($orders as $row) { ?>
        table_array[<?=$row->DocEntry?>] = $('#<?=$row->DocEntry?>').DataTable({
            "autoWidth": true,
            "dom": 'rt',
            "ordering": false
        });
        <?php } ?>

        for (id in table_array) {
            var table = $('#' + id);
            table.find('tr').each(function (index, dom) {
                if (!$(dom).attr('data-line')) {
                    return;
                }
                var line = $(dom).attr('data-line');
                var itemcode = $(dom).attr('data-itemcode');
                var whscode = $(dom).attr('data-whscode');
                var row = table_array[id].row(dom);
                var str = '<p><button type="button" class="btn btn-primary" onclick="_add(' + id + ',\'' + line + '\',\'' + itemcode + '\',\'' + whscode + '\', this)">' +
                    '新增明細' +
                    '</button></p>';
                row.child(str + _init(format, id, line, itemcode, whscode));
            });
        }

        show_all();

        $('#submit_button').click(function () {
            show_all();
            $.ajax({
                url: '<?=site_url('/packing_list_factory/post')?>/<?=$db_name?>/<?=$id?>/1',
                method: 'POST',
                data: $('#myform').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });

        $('#save_button').click(function () {
            show_all();
            $.ajax({
                url: '<?=site_url('/packing_list_factory/post')?>/<?=$db_name?>/<?=$id?>/0',
                method: 'POST',
                data: $('#myform').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });

        total();
    });

    var show_all = function () {
        $('.open_button').each(function () {
            var id = $(this).attr('data-id');
            var tr = $(this).closest('tr');
            var row = table_array[id].row(tr);
            row.child.show();
        });
    }
</script>
<style>
    input {
        border: 0px;
    }
</style>
</body>
</html>