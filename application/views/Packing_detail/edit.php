<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        裝箱明細
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="myform" method="POST" class="form-horizontal"
                  action="<?= site_url('packing_list_factory/post') ?>">
                <!-- END PAGE HEADER-->
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>裝箱明細
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form id="add_form">
                            <div class="table-scrollable">
                                <div style="padding-bottom: 20px;">
                                    <button type="button" id="save_button" class="btn green">儲存</button>
                                    <button type="button" onclick="location='<?=site_url('/packing_detail/index')?>'" class="btn blue">回列表</button>
                                </div>
                                <div>
                                    <button type="button" id="add_button" class="btn green">新增</button>
                                </div>
                                <table class="table table-striped table-bordered table-hover" id="table">
                                    <thead>
                                    <tr class="heading">
                                        <th>操作</th>
                                        <th>MPCS(UNIT)</th>
                                        <th>箱數起號</th>
                                        <th>箱數迄號</th>
                                        <th>箱數</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<script>
    $(function(){
        $('#add_button').click(function(){
            var tr = '<tr>' +
                    '<td>' +
                    '<button type="button" class="btn btn-danger"' +
                    'onclick="_delete(this)">刪除</button>' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" name="batch_no[]" value="" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" name="code[]" value="" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" class="pallet_start" name="pallet_start[]" value="" />' +
                    '</td>' +
                    '<td>' +
                    '<input style="width: 90px" type="text" class="pallet_start" name="pallet_start[]" value="" />' +
                    '</td>' +
                '</tr>';
            $('#table').find('tbody').append(tr);
        });
    })
</script>
<!-- END CONTAINER -->
</body>
</html>