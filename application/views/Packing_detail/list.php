<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>
    .page-content, .page-footer, .page-container {
    }
</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        工單裝箱明細
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box grey-cascade">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="add_form">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover" id="table">
                                        <thead>
                                        <tr class="heading">
                                            <th></th>
                                            <th>訂單號</th>
                                            <th>品號</th>
                                            <th>品名</th>
                                            <th>數量</th>
                                            <th>到期日</th>
                                            <th>公制單位</th>
                                            <th>英制單位</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($rows as $row) { ?>
                                            <tr>
                                                <td>
                                                    <button type="button" class="btn btn-default"
                                                            onclick="location='<?=site_url('/packing_detail/edit/1')?>'">編輯
                                                    </button>
                                                </td>
                                                <td><?= $row->DocEntry ?></td>
                                                <td><?= $row->ItemCode ?></td>
                                                <td><?= $row->ItemName ?></td>
                                                <td><?= $row->PlannedQty ?></td>
                                                <td><?= substr($row->DueDate, 0, 10) ?></td>
                                                <td><?= $row->U_SpecM ?></td>
                                                <td><?= $row->U_SpecE ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tfoot>
                                    </table>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
        submit_table = $('#table').DataTable({
            "autoWidth": true,
            "dom": 'lrtip',
            "order": [[1, 'asc']]
        });

        yadcf.init(submit_table, [
            {column_number: 1, filter_type: "range_number", filter_reset_button_text: false},
            {column_number: 2, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {column_number: 3, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {
                column_number: 4,
                filter_type: "range_number",
                filter_reset_button_text: false,
                style_class: 'form-control'
            },
            {
                column_number: 5,
                filter_type: "range_date",
                filter_reset_button_text: false,
                style_class: 'form-control'
            },
            {column_number: 6, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {column_number: 7, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'}
        ], 'tfoot');


        $('#submit_button').click(function () {
            $.ajax({
                url: '<?=site_url('/packing_list_delivery/post')?>/' + $('#db_select').val(),
                method: 'POST',
                data: $('#myform').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });
    });
</script>
<?php $this->load->view('/common/foot') ?>
</body>
</html>