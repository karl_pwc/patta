<!DOCTYPE html >
<html>
<head></head>
<body>
<style>
    table, td, th {
        border: 0px;
        border-collapse: collapse;
        border-top: 1px solid rgb(232, 232, 232);
        font-size: small;
    }

    th, td {
        padding: 5px;
    }

    td {
        text-align: right;
    }

    h1, h2 {
        text-align: center;
        margin: 4px;
    }

    h3, h4 {
        padding: 0px;
        margin: 0px;
    }

    h4 {
        padding: 0px;
        margin: 0px;
    }

    #logo {
        background-image: url("/assets/uploads/logo.jpg");
        background-size: 120px 60px;
        background-repeat: no-repeat;
        background-position: left top;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        position: absolute;
        -webkit-print-color-adjust: exact;
    }

    header {
        z-index: 1;
    }

    table {
        z-index: 1;
    }

    tfoot {
        text-align: right;
    }

    .left {
        width: 70%;
        text-align: left;
    }

    .right {
        width: 30%;
        text-align: left;
    }

    section {
        width: 100%;
        display: flex;
    }

    footer {
        padding: 5px;
        text-align: right;
    }
</style>
<div id="logo"></div>
<header>
    <h2 style="margin-bottom: 3px">
        <?= $company->PrintHdrF ?>
    </h2>
    <h4 style="text-align: center">
        Tel: <?= $company->Phone1 ?> Fax: <?= $company->Fax ?>
    </h4>

    <h2 style="margin-bottom: 10px">PACKING LIST</h2>
    <section style="padding: 5px">
        <div class="left">
            No: <?= $print_no ?> <br/>
            Shipped by: <?= $company->PrintHdrF ?><br/>
            Sailing on or about: <?= $print_by ?>
        </div>
        <div class="right">
            Email: <?= $company->E_Mail ?><br/>
            Date: <?= $print_date ?><br/>
            per M/S S: <?= $print_ms ?><br/>
            From: <?= $print_from ?> / To: <?= $print_to ?>
        </div>
    </section>
</header>
<h4 style="padding: 5px">
    And consigned to messrs:
</h4>
<table style="width:100%;border-top: 0px">
    <thead>
    <th style="text-align: left;border-top: 1px solid #000000;border-bottom: 1px solid #000000">No</th>
    <th style="text-align: center;border-top: 1px solid #000000;border-bottom: 1px solid #000000">Pallet</th>
    <th style="width:40%;border-top: 1px solid #000000;border-bottom: 1px solid #000000">Description</th>
    <th style="text-align: right;border-top: 1px solid #000000;border-bottom: 1px solid #000000">Pcs/Box</th>
    <th style="text-align: right;border-top: 1px solid #000000;border-bottom: 1px solid #000000">Box/Ctn</th>
    <th style="text-align: right;border-top: 1px solid #000000;border-bottom: 1px solid #000000">Ctn</th>
    <th style="text-align: right;border-top: 1px solid #000000;border-bottom: 1px solid #000000">Quantity</th>
    <th style="text-align: right;border-top: 1px solid #000000;border-bottom: 1px solid #000000">UOM</th>
    <th style="text-align: right;border-top: 1px solid #000000;border-bottom: 1px solid #000000">N.W.(KGS)</th>
    <th style="text-align: right;border-top: 1px solid #000000;border-bottom: 1px solid #000000">G.W.(KGS)</th>
    </thead>
    <tbody>
    <?php
    $data = [];
    foreach ($rows as $row) {
        $data[$row->U_PINO][] = $row;
    }
    ?>
    <?php foreach ($data as $pi => $row_set) { ?>
        <?php $count = count($row_set) ?>
        <?php foreach ($row_set as $key => $row) { ?>
            <?php if ($key == 0) { ?>
                <tr>
                    <td colspan="10">
                        <section style="font-weight: bold;display: block;text-align: left">
                            <span style="padding-right: 10px"><?= $pi ?> (<?= $row->U_CustomerPO ?>)</span>
                        </section>
                    </td>
                </tr>
                <tr>
                    <td style="margin-top:0px;border-top: 0px" colspan="10">
                        <section style="font-weight: bold;display: block;text-align: left">
                            <span><?= $row->U_ForeignName ?></span>
                        </section>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td style="text-align: left"><?= @++$i ?></td>
                <td style="text-align: center">
                    <?= $row->pallet_start . '-' . $row->pallet_end ?>
                </td>
                <td>
                    <div style="text-align: center"><?= $row->U_SpecM ?></div>
                </td>
                <td><?= intval($row->U_PcsBox, 0) ? intval($row->U_PcsBox, 0) : '' ?></td>
                <td><?= intval($row->U_BoxCtn, 0) ? intval($row->U_BoxCtn, 0) : '' ?></td>
                <td><?= intval($row->cartons) ?></td>
                <td><?= $row->mpcs ?></td>
                <td><?= $row->UomCode ?></td>
                <td><?= $row->total_nw ?></td>
                <td><?= $row->total_gw ?></td>
                <?php @$max_pallet = max($max_pallet, $row->pallet_start, $row->pallet_end) ?>
                <?php @$Footer = empty($row->Footer) ? $Footer : $row->Footer ?>

            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <th style="border-top: 1px solid #000000;"></th>
        <th style="text-align: center;border-top: 1px solid #000000;"><?= $max_pallet ?></th>
        <th style="border-top: 1px solid #000000;"></th>
        <th style="border-top: 1px solid #000000;" colspan="2"></th>
        <th style="border-top: 1px solid #000000;"><?= intval($total->cartons) ?></th>
        <th style="border-top: 1px solid #000000;"><?= intval($total->mpcs) ?></th>
        <th style="border-top: 1px solid #000000;"><?= $row->UomCode ?></th>
        <th style="border-top: 1px solid #000000;" nowrap><?= $total->total_nw ?></th>
        <th style="border-top: 1px solid #000000;" nowrap><?= $total->total_gw ?></th>
    </tr>
    </tfoot>
</table>
<footer style="text-align: left">
    <h4>The total gross weight = G.W.+Pallet weight = <?= $print_weight ?> KGS</h4>
    <h4 style="padding-top: 10px;width: 50%">Shipping marks:<br > <?= $row->Footer ?></h4>

    <h3 style="text-align: right;margin-top: 15px"><?= $company->PrintHdrF ?></h3>
</footer>
<div style="width: 45%;margin-top:70px;float: right;border:1px groove  rgb(232, 232, 232)"></div>
</body>
</html>