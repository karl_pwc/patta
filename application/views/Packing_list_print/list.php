<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        列印
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- END PAGE HEADER-->
            <div class="portlet box blue ">
                <div class="portlet-title">
                    <div class="caption">
                        列印
                    </div>
                </div>
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form id="myform" method="post" action="<?=site_url('/packing_list_print/go')?>/<?=$row->packing_id?>"
                          class="form-horizontal form-bordered form-row-stripped" accept-charset="utf-8">
                        <div class="form-body">
                            <div class="form-group" id="account_field_box">
                                <label class="control-label col-md-3">
                                    出貨計畫表 單號</label>

                                <div class="col-md-9" id="account_input_box">
                                    <input type="text" class="form-control" readonly name="id" value="<?=$row->packing_id?>" />
                                </div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group" id="password_field_box">
                                <label class="control-label col-md-3">
                                    NO:</label>

                                <div class="col-md-9" id="password_input_box">
                                    <input type="text" class="form-control" name="print_no" value="<?=$row->print_no?>" /></div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group" id="password_field_box">
                                <label class="control-label col-md-3">
                                    per M/S.S:</label>

                                <div class="col-md-9" id="password_input_box">
                                    <input type="text" class="form-control" name="print_ms" value="<?=$row->print_ms?>" /></div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group" id="password_field_box">
                                <label class="control-label col-md-3">
                                    From:</label>

                                <div class="col-md-9" id="password_input_box">
                                    <input type="text" class="form-control" name="print_from" value="<?=$row->print_from?>" /></div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group" id="password_field_box">
                                <label class="control-label col-md-3">
                                    To:</label>

                                <div class="col-md-9" id="password_input_box">
                                    <input type="text" class="form-control" name="print_to" value="<?=$row->print_to?>" /></div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group" id="password_field_box">
                                <label class="control-label col-md-3">
                                    Date:</label>

                                <div class="col-md-9" id="password_input_box">
                                    <input type="text" class="form-control" name="print_date" value="<?=$row->print_date?>" /></div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group" id="password_field_box">
                                <label class="control-label col-md-3">
                                    Sailing on or about:</label>

                                <div class="col-md-9" id="password_input_box">
                                    <input type="text" class="form-control" name="print_by" value="<?=$row->print_by?>" /></div>
                            </div>
                        </div>
                        <div class="form-body">
                            <div class="form-group" id="password_field_box">
                                <label class="control-label col-md-3">
                                    Total Weight:
                                </label>

                                <div class="col-md-9" id="password_input_box">
                                    <input type="text" class="form-control" name="print_weight" value="<?=$row->print_weight?>" /></div>
                            </div>
                        </div>
                        <div class="form-actions fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="submit_button" type="button" class="btn green">
                                            <i class="fa fa-check"></i>
                                            列印
                                        </button>
                                        <button id="save_button" type="button" class="btn green">
                                            <i class="fa fa-check"></i>
                                            儲存設定
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>


        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>

    jQuery(document).ready(function () {
        $('#save_button').click(function () {
            $.ajax({
                url: '<?=site_url('/packing_list_print/save')?>/<?=$row->packing_id?>',
                method: 'POST',
                data: $('#myform').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });

        $('#submit_button').click(function () {
            $('#myform').submit();
        });
    });
</script>
</body>
</html>