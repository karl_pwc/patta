<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
<style>
    .table>thead>tr>th {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 0px solid #ddd;
        border-bottom: 0px solid #ddd;
    }

    .left {
        border-left: 1px solid #ddd;
    }
    .top{
        border-top: 1px solid #ddd;
    }
    .right{
        border-right: 1px solid #ddd;
    }
    .bottom{
        border-bottom: 1px solid #ddd;
    }
    #footer{
        border-left: 1px solid #ddd;
        border-right: 1px solid #ddd;
        border-bottom: 1px solid #ddd;
    }
</style>
<?php
$data = '';
foreach ($rows as $doc_rows) {
    foreach ($doc_rows as $line_rows) {
        foreach ($line_rows as $detail) {
            $data = $detail;
        }
    }
}
?>
<table style="width:1200px" class="table table-hover" align="center">
    <thead>
    <tr>
        <th colspan="8" class="left top right" style="text-align: center;">PACKING LIST</th>
    </tr>
    <tr>
        <th colspan="2" class="left" style="text-align: right;">NO:</th>
        <th colspan="2" style="text-align: left;"><?= $print_no ?></th>
        <th colspan="1" style="text-align: right;">DATA:</th>
        <th colspan="3" class="right" style="text-align: left;"><?= date('Y/m/d') ?></th>
    </tr>
    <tr>
        <th colspan="2" class="left" style="text-align: right;">Shipped by:</th>
        <th colspan="2" style="text-align: left;"></th>
        <th colspan="1" style="text-align: right;">per M/S S:</th>
        <th colspan="3" class="right" style="text-align: left;"><?= $print_ms ?></th>
    </tr>
    <tr>
        <th colspan="2" class="left" style="text-align: right;border-bottom: 2px solid #ddd;">Sailing On or About:</th>
        <th style="text-align: left;border-bottom: 2px solid #ddd;"><?= $print_from ?></th>
        <th style="text-align: left;border-bottom: 2px solid #ddd;">From: <?= $print_ms ?></th>
        <th colspan="1" style="text-align: right;border-bottom: 2px solid #ddd;">To:</th>
        <th colspan="3" class="right" style="text-align: left;border-bottom: 2px solid #ddd;"><?= $data->ship_name ?></th>
    </tr>
    <tr>
        <th class="left">PALLET NO</th>
        <th>STANDARDS</th>
        <th>P/BOX</th>
        <th>BOX/CTN</th>
        <th>TOTAL CTN</th>
        <th>Quantity</th>
        <th>N.W</th>
        <th class="right">G.W</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $pi = [];
    $total_cartons = 0;
    $mpcs = 0;
    $nw = 0;
    $gw = 0;
    $pallet = 0;
    ?>
    <?php foreach ($rows as $doc_rows) { ?>
        <?php foreach ($doc_rows as $line_rows) { ?>
            <?php foreach ($line_rows as $detail) { ?>
                <?php
                $detail_rows_data = @$detail_rows[$detail->DocEntry][$detail->LineNum];
                if (!$detail_rows_data) {
                    continue;
                }
                foreach ($detail_rows_data as $row) {

                    ?>
                    <?php
                    $id = $detail->DocEntry . '_' . $detail->LineNum;
                    if (!in_array($detail->DocEntry . '_' . $detail->LineNum, $pi) && array_push($pi, $id)) { ?>
                        <tr>
                            <td class="left right" colspan="8">DocEntry:<?= $detail->DocEntry ?> ,LineNum:<?= $detail->LineNum ?>,
                                P/I:<?= $detail->U_PINO ?></td>
                        <tr>
                    <?php } ?>
                    <tr>
                        <td class="left"><?= $row->pallet_start ?> ~ <?= $row->pallet_end ?></td>
                        <td><?= $detail->ItemName ?></td>
                        <td><?= number_format($detail->U_PcsBox, 2) ?></td>
                        <td><?= number_format($detail->U_BoxCtn, 2) ?></td>
                        <td><?= $row->cartons ?></td>
                        <td><?= $row->mpcs ?></td>
                        <td><?= $row->nw ?></td>
                        <td class="right"><?= $row->gw ?></td>
                    </tr>
                    <?php
                    $pallet += $row->pallet_amount;
                    $total_cartons += $row->cartons;
                    $mpcs += $row->mpcs;
                    $nw += $row->nw;
                    $gw += $row->gw;
                }
            }
        }
    } ?>
    <tr>
        <td class="left"><?= $pallet?></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?= $total_cartons ?></td>
        <td><?= $mpcs ?></td>
        <td><?= $nw ?></td>
        <td class="right"><?= $gw ?></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td id="footer" colspan="8">
            <?=$detail->Footer?>
        </td>
    </tr>
    </tfoot>
</table>
</body>
</html>
