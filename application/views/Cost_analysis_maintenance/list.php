<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<style>

</style>
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        維護成本分析單
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="queryForm" method="POST" class="form-horizontal" action="<?= site_url('/cost_analysis_add/post') ?>">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box blue-hoki">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe"></i>維護成本分析單條件篩選
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="col-md-8">
                                                <strong>成本分析單位 :</strong>

                                                <select id="plant" name="plant" class="form-control input-inline">
                                                    <<?php foreach ($plant_list as $plant) { ?>
                                                        <option value="<?= $plant['plant'] ?>"><?= $plant['plant'] ?></option>
                                                    <?php } ?>
                                                </select>

                                                <strong>成本分析狀態 :</strong>

                                                <select id="status" name="status" class="form-control input-inline">
                                                    <?php foreach(CostAnalysis::$status as $key=>$name){ ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $name; ?></option>
                                                    <?php } ?>
                                                </select>

                                                <strong>成本分析單號 :</strong>

                                                起

                                                <input id="Cost_Analysis_From" name="Cost_Analysis_From" type="text" class="form-control input-inline"
                                                       placeholder="起始單號" value="">

                                                止

                                                <input id="Cost_Analysis_To" name="Cost_Analysis_To" type="text" class="form-control input-inline"
                                                       placeholder="結束單號" value="">

                                                <button id="query_button" type="button" class="btn green">查詢</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </form>
            <!-- END PAGE HEADER-->
            <div id="result" class="row" style="">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box blue-madison">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-globe"></i>查詢結果
                            </div>
                        </div>
                        <div class="portlet-body">
                            <form id="add_form">
                                <div class="table-scrollable">
                                    <table class="table table-bordered table-hover" id="table">
                                        <thead>
                                        <tr class="heading">
                                            <th style="white-space: nowrap;width:40px">
                                                操作
                                            </th>
                                            <th>
                                                成本分析單號
                                            </th>
                                            <th>
                                                報價單號
                                            </th>
                                            <th>
                                                報價來源公司
                                            </th>
                                            <th>
                                                申請人
                                            </th>
                                            <th>
                                                預計完成日期
                                            </th>
                                            <th>
                                                狀態
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <br/>
                                    <br/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>
    var table;
    $(function() {

        $('#query_button').click(function () {
            $('#result').hide();

            if ( $.fn.dataTable.isDataTable('#table')) {
                table.destroy();
            }

            table = $('#table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '<?=site_url('cost_analysis_maintenance/table')?>?' + $('#queryForm').serialize(),
                "ordering": false,
                "sDom": "lrtip",
                "autoWidth": true
            });

            $('#result').show();

        });

    });

</script>
</body>
</html>