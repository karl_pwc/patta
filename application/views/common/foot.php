<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        2015 &copy;
    </div>
    <div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
    </div>
</div>
<script>
    $(function(){
        <?php if($success_message !== null){ ?>
            $(function(){
                form_success_message("<?php echo $success_message; ?>");
            });
        <?php } ?>
    });

    var _print = function(packing_id, dom){
        window.open('<?=site_url('/packing_list_print/index')?>/'+packing_id);
    }

    var _back = function(packing_id, docEntry, lineNum, dbName){
        if(confirm("確定要將此筆明細取消嗎(將退回至新增程序)？")){
            $.ajax({
                url: '<?=site_url('/packing_list_ajax_global/cancel')?>/'+packing_id+'/'+dbName+'/'+docEntry+'/'+lineNum,
                method: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        }
    }

    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init(); // init quick sidebar
        ComponentsPickers.init();
    });

</script>