<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu page-sidebar-menu-closed" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <?php
            $role = explode(',', Sakilu_Auth::getUser()->role);
            ?>
            <?php foreach ($workbench_menu as $menu) { ?>
                <li class="<?= $menu['title'] == $menu_select ? 'active' : '' ?>">
                    <a href="<?= empty($menu['url']) ? 'javascript:;' : $menu['url']; ?>">
                        <?php if (!empty($menu['icon'])) { ?>
                            <i class="<?= $menu['icon'] ?>"></i>
                        <?php } ?>
                        <span class="title"><?= $menu['title'] ?></span>
                        <?= $menu['title'] == $menu_select ? '<span class="selected"></span>' :
                            '<span class="arrow"></span>' ?>
                    </a>
                    <?php if (!empty($menu['children'])) { ?>
                        <ul class="sub-menu">
                            <?php foreach ($menu['children'] as $sub_menu) { ?>
                                <?php if(in_array($sub_menu['role'], $role)){ ?>
                                <li class="<?= $sub_menu['title'] == $sub_menu_select ? 'active' : '' ?>">
                                    <a href="<?= $sub_menu['url'] ?>/index/<?=isset($db) ? $db : ''?>"><?= $sub_menu['title'] ?></a>
                                </li>
                            <?php }} ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>