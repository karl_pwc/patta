<div>
    <label> 公司選擇
        <select id="db_select" class="form-control input-inline">
            <?php foreach ($db_list as $v) { ?>
                <option <?=$v['value'] == $db_name ? 'selected' : ''?> value="<?= $v['value'] ?>"><?= $v['caption'] ?></option>
            <?php } ?>
        </select>
    </label>
</div>
<script>
    $(function(){
        $('#db_select').change(function(){
            window.location = '<?=site_url("/$controller")?>/index/'+$(this).val();
        });
    });
</script>