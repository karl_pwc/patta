<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<style>
    .page-content, .page-footer, .page-container, #myform > div {
        width: 2000px
    }
</style>
<!-- BEGIN CONTAINER -->
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        編輯
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="myform" method="POST" class="form-horizontal" action="<?= site_url('packing_list_add/post') ?>">
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box grey-cascade">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-globe"></i>查詢
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <?php $this->load->view('/common/db_select'); ?>
                                    </div>
                                    <div class="col-md-6 col-sm-12">

                                    </div>
                                </div>
                                <table class="table table-striped table-bordered table-hover"
                                       id="table">
                                    <thead>
                                    <tr class="heading">
                                        <th style="width: 10px">
                                            操作
                                        </th>
                                        <th style="width: 60px">
                                            計畫號碼
                                        </th>
                                        <th>
                                            訂單號 / PI / 訂單交貨日 / ASAP
                                        </th>
                                        <th>
                                            狀態
                                        </th>
                                        <th>
                                            出貨排程日
                                        </th>
                                        <th>
                                            總櫃數
                                        </th>
                                        <th>
                                            客戶
                                        </th>
                                        <th>
                                            幣別
                                        </th>
                                        <th>
                                            貨到港
                                        </th>
                                        <th>
                                            出貨單號
                                        </th>
                                        <th>
                                            船務備註
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($rows as $row) { ?>
                                        <?php $data = reset($row); ?>
                                        <tr>
                                            <td nowrap>
                                                <i class="fa fa-edit" style="cursor:pointer"
                                                   data-key="<?= $data->packing_id ?>" data-toggle="modal"
                                                   data-amount="<?= $data->amount ?>"
                                                   data-state="<?= $data->state ?>"
                                                   data-date="<?= date('Y-m-d', strtotime($data->date)) ?>"
                                                   data-target="#basic">
                                                </i>
                                                <i style="cursor:pointer"
                                                   onclick="_open(<?= $data->packing_id ?>, this)"
                                                   class="fa fa-search-plus"></i>
                                                <i style="cursor:pointer"
                                                   onclick="cancel_packing(<?= $data->packing_id ?>)"
                                                   class="fa fa-trash-o"></i>
                                            </td>
                                            <td>
                                                PN-<?= $data->packing_id ?>
                                            </td>
                                            <td>
                                                <table class="table table-striped table-bordered table-hover dataTable">
                                                    <tbody>
                                                    <?php
                                                    $str = '';
                                                    foreach ($row as $detail) {
                                                        if ($detail->U_PINO != $str) {
                                                            $str = $detail->U_PINO;
                                                            ?>
                                                            <tr>
                                                                <td style="width: 20px"><?= $detail->DocEntry ?></td>
                                                                <td style="width: 40px"><?= $detail->U_PINO ?></td>
                                                                <td style="width: 40px"><?= date('m/d/Y',
                                                                        strtotime(substr($detail->DocDueDate, 0,
                                                                            10))) ?></td>
                                                                <td style="width: 70px"><?= $detail->U_DocType ?></td>
                                                            </tr>
                                                        <?php }
                                                    } ?>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <?php
                                                if ($data->state == 'A') {
                                                    echo "待業務確認";
                                                } elseif ($data->state == 'B') {
                                                    echo "待廠務確認";
                                                } elseif ($data->state == 'C') {
                                                    echo "待船務確認";
                                                } elseif ($data->state == 'D') {
                                                    echo "待出貨確認";
                                                } elseif ($data->state == 'E') {
                                                    echo "已出貨";
                                                } elseif ($data->state == 'P') {
                                                    ?>
                                                    <button type="button" class="btn btn-success"
                                                            onclick="restore(<?= $data->packing_id ?>)">還原
                                                    </button>
                                                    <?php
                                                    echo "pending";
                                                } else {
                                                    echo "還未排程";
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?= date('m/d/Y', strtotime($data->date)) ?>
                                            </td>
                                            <td>
                                                <?= $data->amount ?>
                                            </td>
                                            <td>
                                                <?= $data->CardName ?>
                                            </td>
                                            <td>
                                                <?= $data->DocCur ?>
                                            </td>
                                            <td>
                                                <?= $data->Name ?>
                                            </td>
                                            <td>
                                                <?= $data->sap_packing_id ?>
                                            </td>
                                            <td>
                                                <?= nl2br($data->note) ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <th>
                                        <button type="button" onclick="yadcf.exResetAllFilters(submit_table)"
                                                class="btn btn-warning">重設
                                        </button>
                                    </th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
            </form>

        </div>
    </div>
</div>
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">修改資料</h4>
            </div>
            <div class="modal-body">
                <div class="portlet-body form">
                    <form id="modal_form" role="form">
                        <div class="form-body">
                            <div class="form-group">
                                <label>計畫號碼</label>
                                <input type="text" id="modal_packing_id" name="packing_id" readonly
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label>出貨排程日</label>
                                <input type="date" id="modal_date" name="date"
                                       class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label>總櫃數</label>
                                <input type="text" id="modal_amount" name="amount" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>狀態</label>
                                <select id="modal_state" name="state" class="form-control">
                                    <option value="A">待業務確認</option>
                                    <option value="B">待廠務確認</option>
                                    <option value="C">待船務確認</option>
                                    <option value="D">待出貨確認</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">關閉</button>
                <button type="button" onclick="modal_submit()" class="btn blue">儲存</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>

    function modal_submit() {
        if ($('#modal_packing_id').val() == '') {
            alert('無計畫號碼, 請重新操作');
            return;
        }
        $.ajax({
            url: '<?=site_url('/packing_list_edit/post')?>/' + $('#db_select').val(),
            method: 'POST',
            data: $('#modal_form').serialize(),
            dataType: 'json',
            beforeSend: function (xhr) {
                Metronic.blockUI();
            }
        }).done(function (data) {
            Metronic.unblockUI();
            window.location.reload();
        }).fail(function (data) {
            Metronic.unblockUI();
            alert(data.responseText);
        });

    }

    $('#basic').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var key = button.data('key');
        var amount = button.data('amount');
        var date = button.data('date');
        var state = button.data('state');

        $('#modal_packing_id').val(key);
        $('#modal_amount').val(amount);
        $('#modal_date').val(date);

        $('#modal_state').empty();
        if (state == 'B') {
            $('#modal_state').append('<option value="">不修改</option>');
            $('#modal_state').append('<option value="A">待業務確認</option>');
        } else if (state == 'C') {
            $('#modal_state').append('<option value="">不修改</option>');
            $('#modal_state').append('<option value="A">待業務確認</option>');
            $('#modal_state').append('<option value="B">待廠務確認</option>');
        } else if (state == 'D') {
            $('#modal_state').append('<option value="">不修改</option>');
            $('#modal_state').append('<option value="A">待業務確認</option>');
            $('#modal_state').append('<option value="B">待廠務確認</option>');
            $('#modal_state').append('<option value="C">待船務確認</option>');
        } else {
            $('#modal_state').append('<option value="">已出貨或僅在業務確認 無法變更</option>');
        }
    });

    var submit_table;

    var transform = function (DocEntry, LineNum, U_Planned, packing_id) {
        var number = prompt("請填寫你要轉的 出貨計畫表單號");
        number = parseInt(number);
        if (number) {
            $.ajax({
                url: '<?=site_url('/packing_list_ajax_global/transform')?>/<?=$db_name?>/' + packing_id,
                method: 'POST',
                data: {
                    doc_num: DocEntry,
                    line: LineNum,
                    target: number + '_' + U_Planned
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        }
    }

    var restore = function (id) {
        if (confirm("確定要還原此筆出貨計畫嗎？")) {
            $.ajax({
                url: '<?=site_url('/packing_list_ajax_global/restore')?>/<?=$db_name?>/' + id,
                method: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        }
    }

    var cancel_packing = function (id) {
        if (confirm("確定要取消此筆出貨計畫嗎？")) {
            $.ajax({
                url: '<?=site_url('/packing_list_ajax_global/cancel_packing')?>/<?=$db_name?>/' + id,
                method: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        }
    }

    var cancel_detail = function (packing_id, docEntry, line) {
        if (confirm("確定要取消此筆訂單明細嗎？")) {
            $.ajax({
                url: '<?=site_url('/packing_list_ajax_global/cancel')?>/' + packing_id + '/<?=$db_name?>/' + docEntry + '/' + line,
                method: 'POST',
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        }
    }

    var detail = <?=json_encode($rows)?>;

    var _open = function (id, dom) {
        var data = detail[id];
        var tr = $(dom).closest('tr');
        var row = submit_table.row(tr);
        if (row.child.isShown()) {
            row.child.hide();
        }
        else {
            row.child(format(data)).show();
        }
    }

    var planned_amount_ajax = function (packing_id, DocEntry, LineNum, planned_amount, U_PlannedQty, Quantity, min) {
        var max = (Quantity - U_PlannedQty) + planned_amount;
        var new_amount = prompt("輸入計畫數(最大值:" + max + ", 最小值:" + min + ") ");

        if (new_amount != null) {
            new_amount = Number(new_amount).toFixed(2)
            if (new_amount <= 0) {
                alert('請輸入大於0數字');
                return;
            }
            if (new_amount < min) {
                alert('請輸入小於' + min + '數字');
                return;
            }
            if (new_amount > max) {
                alert('請輸入小於' + max + '的數字');
                return;
            }
            $.ajax({
                url: '<?=site_url('/packing_list_ajax_global/update_planned_amount')?>/' + $('#db_select').val(),
                method: 'POST',
                data: {
                    new_amount: new_amount,
                    packing_id: packing_id,
                    DocEntry: DocEntry,
                    LineNum: LineNum
                },
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        }
    }

    var format = function (rows) {
        var t = $('<div><table class="table table-bordered table-hover">' +
        '<thead>' +
        '<th>操作</th>' +
        '<th>訂單編號</th>' +
        '<th>LineNum</th>' +
        '<th>待出貨批數</th>' +
        '<th>已計畫數</th>' +
        '<th>已包裝批數</th>' +
        '<th>品號</th>' +
        '<th>品名</th>' +
        '<th>公制規格</th>' +
        '<th>英制規格</th>' +
        '<th>訂單數量</th>' +
        '<th>未出貨數量</th>' +
        '<th>計量單位</th>' +
        '<th>倉別</th>' +
        '</thead>' +
        '<tbody>' +
        '</tbody>' +
        '</table></div>');
        _.forEach(rows, function (row, key) {
            row['U_SpecM'] = row['U_SpecM'] ? row['U_SpecM'] : '';
            row['U_SpecE'] = row['U_SpecE'] ? row['U_SpecE'] : '';
            row['unitMsr'] = row['unitMsr'] ? row['unitMsr'] : '';
            $(t).find('tbody:last').append(
                "<tr>" +
                "<td style='width: 60px'>" +
                "<i style='cursor:pointer' onclick='planned_amount_ajax(" + row['packing_id'] + "," + row['DocEntry'] + "," + row['LineNum'] + "," + Number(row['planned_amount']).toFixed(2) + "," + Number(row['U_PlannedQty']).toFixed(2) + "," + Number(row['Quantity']).toFixed(2) + "," + Number(row['detail_total']).toFixed(2) + ")' class='fa fa-edit'></i> " +
                "<i style='cursor:pointer' class='fa fa-trash-o' onclick='cancel_detail(" + row['packing_id'] + "," + row['DocEntry'] + "," + row['LineNum'] + ")' '></i> " +
                "<i style='cursor:pointer' class='fa fa-sign-out' onclick=\"transform(" + row['DocEntry'] + "," + row['LineNum'] + ",'" + row['state'] + "','" + row['packing_id'] + "')\"></i>" +
                "</td>" +
                "<td>" + row['DocEntry'] + "</td>" +
                "<td>" +
                "<input type='hidden' name='DocEntry[]' value='" + row['DocEntry'] + "' />" +
                "<input type='hidden' name='LineNum[]' value='" + row['LineNum'] + "' />" + row['LineNum'] + "" +
                "</td>" +
                "<td>" + Number(row['planned_amount']).toFixed(2) + "</td>" +
                "<td>" + Number(row['U_PlannedQty']).toFixed(2) + "</td>" +
                "<td>" + Number(row['detail_total']).toFixed(2) + "</td>" +
                "<td>" + row['ItemCode'] + "</td>" +
                "<td>" + row['ItemName'] + "</td>" +
                "<td>" + row['U_SpecM'] + "</td>" +
                "<td>" + row['U_SpecE'] + "</td>" +
                "<td>" + Number(row['Quantity']).toFixed(2) + "</td>" +
                "<td>" + Number(row['OpenQty']).toFixed(2) + "</td>" +
                "<td>" + row['unitMsr'] + "</td>" +
                "<td>" + row['WhsCode'] + "</td>" +
                "</tr>");
        });
        return t.html();
    }

    jQuery(document).ready(function () {
        submit_table = $('#table').DataTable({
            "autoWidth": true,
            "dom": 'lrtip', "columnDefs": [
                {
                    "targets": 0,
                    "orderable": false
                }
            ],
            "order": [[1, 'asc']]
        });

        yadcf.init(submit_table, [
            {column_number: 1, filter_type: "range_number", filter_reset_button_text: false},
            {column_number: 2, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {column_number: 3, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {
                column_number: 4,
                filter_type: "range_date",
                filter_reset_button_text: false,
                style_class: 'form-control'
            },
            {column_number: 5, filter_type: "range_number", filter_reset_button_text: false},
            {
                column_number: 6,
                filter_type: "select",
                filter_reset_button_text: false,
                style_class: 'form-control',
                filter_default_label: 'Select'
            },
            {
                column_number: 7,
                filter_type: "select",
                filter_reset_button_text: false,
                style_class: 'form-control',
                filter_default_label: 'Select'
            },
            {column_number: 8, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {column_number: 9, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
            {column_number: 10, filter_type: "text", filter_reset_button_text: false, style_class: 'form-control'},
        ], 'tfoot');

        $('#submit_button').click(function () {
            $.ajax({
                url: '<?=site_url('/packing_list_delivery/post')?>/' + $('#db_select').val(),
                method: 'POST',
                data: $('#myform').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location.reload();
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });
    });
</script>
</body>
</html>