<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    protected $_main_menu;

    protected $_sub_menu;

    public function __construct()
    {
        parent::__construct();
        init_menu();
        $this->load->vars(array(
            'menu_select' => $this->_main_menu,
            'sub_menu_select' => $this->_sub_menu,
            'success_message' => $this->session->flashdata('success_message')
        ));
    }
}