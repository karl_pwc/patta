<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<?php $this->load->view('/common/head') ?>
<!-- BEGIN CONTAINER -->
<div id="page-container" class="page-container">
    <!-- BEGIN SIDEBAR -->
    <?php $this->load->view('/common/sidebar') ?>
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content"><!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                        <?= $menu_select ?>
                    </h3>
                    <ul style="display: none;" class="page-breadcrumb breadcrumb">
                    </ul>
                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <form id="my_form" method="POST" class="form-horizontal form-bordered"
                  action="<?= site_url('role/post') ?>">
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PORTLET-->
                        <div class="portlet box blue-hoki">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-gift"></i><?= $sub_menu_select ?>
                                </div>
                                <div class="tools">

                                </div>
                            </div>
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">帳號
                                        <span class="required" aria-required="true">
													* </span>
                                        </label>

                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <input type="hidden" name="id" value="<?= $id ?>"/>
                                                <input type="text" class="form-control" name="account"
                                                       value="<?= @$account ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">信箱</label>

                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <input type="text" class="form-control" name="mail"
                                                       value="<?= @$mail ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">密碼
                                        <span class="required" aria-required="true">
													* </span>
                                        </label>

                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <input type="password" class="form-control" name="password"
                                                       value="<?= @$password ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">密碼確認
                                        <span class="required" aria-required="true">
													* </span>
                                        </label>

                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <input type="password" class="form-control" name="password_2"
                                                       value="<?= @$password ?>"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">權限</label>

                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <input <?= @in_array(1, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_ROLE ?>"/> 權限 <br/>
                                                <input <?= @in_array(2, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_VIEW ?>"/> 出貨計畫表 查詢
                                                <br/>
                                                <input <?= @in_array(3, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_EDIT ?>"/> 出貨計畫表 編輯
                                                <br/>
                                                <input <?= @in_array(4, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_ADD ?>"/> 出貨計畫表 新增
                                                <br/>
                                                <input <?= @in_array(5, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_BUSINESS ?>"/> 出貨計畫表
                                                業務確認 <br/>
                                                <input <?= @in_array(6, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_FACTORY ?>"/> 出貨計畫表
                                                廠務確認 <br/>
                                                <input <?= @in_array(7, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_SHIPPING ?>"/> 出貨計畫表
                                                船務確認 <br/>
                                                <input <?= @in_array(8, $role) ? 'checked' : '' ?>
                                                    type="checkbox" name="role[]" value="<?= ROLE_DELIVERY ?>"/> 出貨計畫表
                                                出貨確認 <br/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">公司權限</label>

                                        <div class="col-md-4">
                                            <div class="input-icon right">
                                                <?php foreach (get_db_list(true) as $v) { ?>
                                                    <input <?= @in_array($v['value'], $db_role) ? 'checked' : '' ?>
                                                        type="checkbox" name="db_role[]"
                                                        value="<?= $v['value'] ?>"/> <?= $v['caption'] ?> <br/>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button id="form_submit" type="button" class="btn purple"><i
                                                        class="fa fa-check"></i>
                                                    Submit
                                                </button>
                                                <button type="button"
                                                        onclick="javascript:location.href='<?= site_url('/role') ?>'"
                                                        class="btn default">Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END PORTLET-->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('/common/foot') ?>
<!-- END CONTAINER -->
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        QuickSidebar.init();// init quick sidebar

        $('#form_submit').click(function () {
            $.ajax({
                url: '<?=site_url('/role/post')?>',
                method: 'POST',
                data: $('#my_form').serialize(),
                dataType: 'json',
                beforeSend: function (xhr) {
                    Metronic.blockUI();
                }
            }).done(function (data) {
                Metronic.unblockUI();
                window.location = '<?=site_url('/role')?>';
            }).fail(function (data) {
                Metronic.unblockUI();
                form_error_message(data.responseText);
            });
        });
    });
</script>
</body>
</html>